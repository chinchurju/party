<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/**
 * Admin routes
 */
Route::namespace('Admin')->group(function () {
    Route::get('admin/login', 'LoginController@showLoginForm')->name('admin.login');
    Route::post('admin/login', 'LoginController@login')->name('admin.login');
    Route::get('admin/logout', 'LoginController@logout')->name('admin.logout');
});
Route::group(['prefix' => 'admin', 'middleware' => ['employee'], 'as' => 'admin.' ], function () {
    Route::namespace('Admin')->group(function () {
        Route::group(['middleware' => ['role:admin|superadmin|clerk, guard:employee']], function () {
            Route::get('/dashboard/{reportBase}', 'DashboardController@index')->name('dashboard');
			Route::get('getOrderData','DashboardController@getOrderData');
			Route::post('change-status','DashboardController@changeStatus')->name('changeStatus');
			Route::post('/productSaleReport', 'DashboardController@productSaleReport');
            Route::namespace('Products')->group(function () {
                Route::resource('products', 'ProductController');
                Route::get('remove-image-product', 'ProductController@removeImage')->name('product.remove.image');
                Route::get('remove-image-thumb', 'ProductController@removeThumbnail')->name('product.remove.thumb');
				Route::get('products/{product}/options', 'ProductController@option')->name('products.options');
            });
            Route::namespace('Customers')->group(function () {
                Route::resource('customers', 'CustomerController');
                Route::resource('customers.addresses', 'CustomerAddressController');
            });
            Route::namespace('Categories')->group(function () {
                Route::resource('categories', 'CategoryController');
                Route::get('remove-image-category', 'CategoryController@removeImage')->name('category.remove.image');
            });
            Route::namespace('Orders')->group(function () {
                Route::resource('orders', 'OrderController');
                Route::resource('order-statuses', 'OrderStatusController');
                Route::get('orders/{id}/invoice', 'OrderController@generateInvoice')->name('orders.invoice.generate');
            });
			Route::get('reports/sale', 'Reports\SalesController@index')->name('sale.index');
			Route::get('reports/sale/export', 'Reports\SalesController@export')->name('sale.export');
			
            Route::resource('addresses', 'Addresses\AddressController');
            Route::resource('countries', 'Countries\CountryController');
            Route::resource('countries.provinces', 'Provinces\ProvinceController');
            Route::resource('countries.provinces.cities', 'Cities\CityController');
            Route::resource('couriers', 'Couriers\CourierController');
            Route::resource('attributes', 'Attributes\AttributeController');
            Route::resource('attributes.values', 'Attributes\AttributeValueController');
            Route::resource('brands', 'Brands\BrandController');
			
			Route::post('order/send-location/{id}', 'Orders\OrderController@sendLocation')->name('order.sendlocation');
			//Route::resource('block', 'Blocks\BlockController');
			
			Route::get('block', 'Blocks\BlockController@index')->name('block.index');
			Route::get('block/{id}/edit', 'Blocks\BlockController@edit')->name('block.edit');
			Route::post('block/store', 'Blocks\BlockController@store')->name('block.store');
			Route::get('block/create', 'Blocks\BlockController@create')->name('block.create');;
			Route::post('block/{id}/update', 'Blocks\BlockController@update')->name('block.update');
			Route::post('block/{id}/destroy', 'Blocks\BlockController@destroy')->name('block.destroy');
			
			//reports
            Route::get('/statsCustomers', 'AdminReportsController@statsCustomers');
            Route::get('/statsProductsPurchased', 'AdminReportsController@statsProductsPurchased');
            Route::get('/statsProductsLiked', 'AdminReportsController@statsProductsLiked');
            Route::get('/productsStock', 'AdminReportsController@productsStock');
            Route::post('/productSaleReport', 'AdminReportsController@productSaleReport');
			
			//Banners
			Route::get('/listingBanners', 'AdminBannersController@listingBanners')->name('listingBanners');
			Route::get('/addBanner', 'AdminBannersController@addBanner')->name('addBanner');
			Route::post('/addNewBanner', 'AdminBannersController@addNewBanner');
			Route::get('/editBanner/{id}', 'AdminBannersController@editBanner');
			Route::post('/updateBanner', 'AdminBannersController@updateBanner');
			Route::post('/deleteBanner/', 'AdminBannersController@deleteBanner');


			//Coupon
			Route::get('/listingCoupon', 'AdminCouponController@listingCoupon')->name('listingCoupon');
			Route::get('/addCoupon', 'AdminCouponController@addCoupon')->name('addCoupon');
			Route::post('/addNewCoupon', 'AdminCouponController@addNewCoupon');
			Route::get('/editCoupon/{id}', 'AdminCouponController@editCoupon');
			Route::post('/updateCoupon', 'AdminCouponController@updateCoupon');
			Route::post('/deleteCoupon/', 'AdminCouponController@deleteCoupon');


			 /********* REVIEWS *********/
            Route::get('/reviews', 'AdminReviewsController@index')->name('Reviews');
            Route::post('/review/action', 'AdminReviewsController@action');
			//pages controller
			Route::get('/listingPages', 'AdminPagesController@listingPages')->name('pages');
			Route::get('/addPage', 'AdminPagesController@addPage');
			Route::post('/addNewPage', 'AdminPagesController@addNewPage');
			Route::get('/editPage/{id}', 'AdminPagesController@editPage');
			Route::post('/updatePage', 'AdminPagesController@updatePage');
			Route::get('/pageStatus', 'AdminPagesController@pageStatus');
			//subscribers controller
			Route::get('/subscribers', 'subscribers\AdminSubscribersController@listing')->name('subscribers');
			//zones
			Route::get('/listingZones', 'AdminTaxController@listingZones');
			Route::get('/addZone', 'AdminTaxController@addZone');
			Route::post('/addNewZone', 'AdminTaxController@addNewZone');
			Route::get('/editZone/{id}', 'AdminTaxController@editZone');
			Route::post('/updateZone', 'AdminTaxController@updateZone');
			Route::post('/deleteZone', 'AdminTaxController@deleteZone');

			//tax class
			Route::get('/listingTaxClass', 'AdminTaxController@listingTaxClass');
			Route::get('/addTaxClass', 'AdminTaxController@addTaxClass');
			Route::post('/addNewTaxClass', 'AdminTaxController@addNewTaxClass');
			Route::get('/editTaxClass/{id}', 'AdminTaxController@editTaxClass');
			Route::post('/updateTaxClass', 'AdminTaxController@updateTaxClass');
			Route::post('/deleteTaxClass', 'AdminTaxController@deleteTaxClass');

			//tax rate
			Route::get('/listingTaxRates', 'AdminTaxController@listingTaxRates');
			Route::get('/addTaxRate', 'AdminTaxController@addTaxRate');
			Route::post('/addNewTaxRate', 'AdminTaxController@addNewTaxRate');
			Route::get('/editTaxRate/{id}', 'AdminTaxController@editTaxRate');
			Route::post('/updateTaxRate', 'AdminTaxController@updateTaxRate');
			Route::post('/deleteTaxRate', 'AdminTaxController@deleteTaxRate');

        });
			Route::group(['middleware' => ['role:admin|superadmin, guard:employee']], function () {
            Route::resource('employees', 'EmployeeController');
            Route::get('employees/{id}/profile', 'EmployeeController@getProfile')->name('employee.profile');
            Route::put('employees/{id}/profile', 'EmployeeController@updateProfile')->name('employee.profile.update');
            Route::resource('roles', 'Roles\RoleController');
            Route::resource('permissions', 'Permissions\PermissionController');
        });
    });
});

/**
 * Frontend routes
 */
Auth::routes();
Route::namespace('Auth')->group(function () {
    Route::get('cart/login', 'CartLoginController@showLoginForm')->name('cart.login');
    Route::post('cart/login', 'CartLoginController@login')->name('cart.login');
    Route::get('logout', 'LoginController@logout');
});

Route::namespace('Front')->group(function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::group(['middleware' => ['auth', 'web']], function () {

        Route::namespace('Payments')->group(function () {
            Route::get('bank-transfer', 'BankTransferController@index')->name('bank-transfer.index');
            Route::post('bank-transfer', 'BankTransferController@store')->name('bank-transfer.store');
			Route::get('cash-on-delivery', 'CashOnDeliveryController@index')->name('cash-on-delivery.index');
            Route::post('cash-on-delivery', 'CashOnDeliveryController@store')->name('cash-on-delivery.store');
        });

        Route::namespace('Addresses')->group(function () {
            Route::resource('country.state', 'CountryStateController');
            Route::resource('state.city', 'StateCityController');
        });
		//Route::resource('accounts', 'AccountsController');
        Route::get('accounts', 'AccountsController@index')->name('accounts');
		Route::post('accounts/update', 'AccountsController@update')->name('accounts.update');
		Route::post('accounts/wishlist', 'AccountsController@wishlist')->name('accounts.wishlist');
		Route::get('accounts/order/{orderid}', 'AccountsController@generateInvoice')->name('accounts.invoice');
		Route::post('accounts/order/{orderid}', 'AccountsController@cancelOrder')->name('accounts.cancelorder');
		
        Route::get('checkout', 'CheckoutController@index')->name('checkout.index');
        Route::post('checkout', 'CheckoutController@store')->name('checkout.store');
		
		Route::post('step/{step}', 'CheckoutController@step')->name('checkout.step');
		
        Route::get('checkout/execute', 'CheckoutController@executePayPalPayment')->name('checkout.execute');
        Route::post('checkout/execute', 'CheckoutController@charge')->name('checkout.execute');
        Route::get('checkout/cancel', 'CheckoutController@cancel')->name('checkout.cancel');
        Route::get('checkout/success', 'CheckoutController@success')->name('checkout.success');
        Route::resource('customer.address', 'CustomerAddressController');
		//Route::get('country/zone/{countryid}', 'CustomerAddressController@zone')->name('checkout.zone');
    });
    Route::resource('cart', 'CartController');
	Route::post("cart/ajaxcart", 'CartController@ajaxcart')->name('cart.ajaxcart');
    Route::get("category/{slug}", 'CategoryController@getCategory')->name('front.category.slug');
    Route::get("search", 'ProductController@search')->name('search.product');
	Route::get("list", 'ProductController@shop')->name('product.shop');
	Route::get("contact-us", 'ContactusController@index')->name('contactus.index');
	Route::post("contact-us", 'ContactusController@send')->name('contactus.send');
    Route::get("{product}", 'ProductController@show')->name('front.get.product');
	Route::post("subscribe", 'SubscribersController@add')->name('subscribe');
	
	Route::get("page/{page}", 'PageController@index')->name('page');

});