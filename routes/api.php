<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')->group(function () {
	Route::post('login', 'LoginController@login');
	Route::post('forgetpassword', 'ForgotPasswordController@sendResetPassword');
	Route::post('sign-up', 'RegisterController@register');
	Route::post('sign-up-sm', 'RegisterController@social');
    Route::post('sign-up-google', 'RegisterController@google');
	Route::get("related-products/{productId}", 'ProductController@relativeProducts');
    Route::get("products", 'ProductController@search');
	Route::get("categories", 'CategoryController@index');
	Route::get("category/{categoryId}", 'CategoryController@getProducts');
	Route::get("countries", 'CountryController@index');
	Route::get("country/{countryId}", 'CountryController@getProducts');
	Route::get("banners", 'HomeController@banners');
	Route::get("listingPages", 'HomeController@listingPages');
	Route::get("product/{productid}", 'ProductController@show');
	Route::get('country/zone/{countryid}', 'CustomerAddressController@zone');
	Route::post("subscribe", 'SubscribersController@add');
	Route::post("contact", 'ContactsController@send');
	Route::group(['middleware' => ['api_auth']], function () {
	    Route::post('accounts/registerdevice', 'AccountsController@registerDevices');
		Route::get('accounts/wishlist', 'AccountsController@wishlist');
		Route::post('accounts/wishlist/add', 'AccountsController@wishlistAdd');
		Route::get('accounts/orders', 'AccountsController@my_orders');
		Route::get('accounts/order/{orderId}', 'AccountsController@orders_details');
		Route::get('accounts/address', 'AccountsController@my_address');
		Route::get('accounts/my-account', 'AccountsController@my_details');
		Route::get('accounts/notification', 'AccountsController@notification');
		Route::post('accounts/update', 'AccountsController@update');
		Route::post('accounts/address/create', 'CustomerAddressController@store');
		Route::get('accounts/address/edit/{addressId}', 'CustomerAddressController@edit');
		Route::post('accounts/address/update/{addressId}', 'CustomerAddressController@update');
		Route::get('address/set-default/{id}', 'AccountsController@setDefault');
		Route::get('listcoupon', 'CouponController@listcoupon');
		Route::get('coupon_code_check', 'CouponController@index');
		Route::post('cart/store', 'CartController@store');
		Route::post('cart/update/{cartId}', 'CartController@update');
		Route::get('cart/delete/{cartId}', 'CartController@destroy');
		Route::get('cart/clear', 'CartController@clear');
		Route::get('cart', 'CartController@index');
		Route::post('checkout/step/{step}', 'CheckoutController@step');
		Route::post('checkout', 'Payments\PaymentController@store');
		Route::post('place-order/{id}', 'Payments\PaymentController@storeupdate');
		Route::post('place-order-update/{id}', 'Payments\PaymentController@storeattachment');
		
	});
});


