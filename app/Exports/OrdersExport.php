<?php

namespace App\Exports;


use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Shop\Addresses\Repositories\Interfaces\AddressRepositoryInterface;
use App\Shop\Addresses\Transformations\AddressTransformable;
use App\Shop\Couriers\Courier;
use App\Shop\Couriers\Repositories\CourierRepository;
use App\Shop\Couriers\Repositories\Interfaces\CourierRepositoryInterface;
use App\Shop\Customers\Customer;
use App\Shop\Customers\Repositories\CustomerRepository;
use App\Shop\Customers\Repositories\Interfaces\CustomerRepositoryInterface;
use App\Shop\Orders\Order;
use App\Shop\Orders\Repositories\Interfaces\OrderRepositoryInterface;
use App\Shop\Orders\Repositories\OrderRepository;
use App\Shop\OrderStatuses\OrderStatus;
use App\Shop\OrderStatuses\Repositories\Interfaces\OrderStatusRepositoryInterface;
use App\Shop\OrderStatuses\Repositories\OrderStatusRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use \Datetime;

class OrdersExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
	public function view(): View
    {
		
		$order =   Order::where('id','>','0');
		
		if (request()->has('dateRange')) {
			if(!empty(request()->input('dateRange'))){
				$reqst = explode("-",request()->input('dateRange'));
				
				
				$date = date_create_from_format('m/d/Y',trim($reqst[0]));
				$frmdt = date_format($date, 'Y-m-d');
				
				/*$fdate = new DateTime();
				$fdate->createFromFormat('m/d/Y',trim($reqst[0]));
				echo $frmdt = $fdate->format('Y-m-d');
				
				
				$tdate = new DateTime();
				$fdate->createFromFormat('m/d/Y',trim($reqst[1]));
				echo $todt = $fdate->format('Y-m-d');*/
				
				$date1 = date_create_from_format('m/d/Y',trim($reqst[1]));
				$todt = date_format($date1, 'Y-m-d');
				
				$order->where('created_at' ,'>=',$frmdt);	
				$order->where('created_at' ,'<=',$todt);
			}
		}
		if(request()->has('status')){
			if(!empty(request()->input('status'))){
				$order->where('order_status_id' ,'=',request()->input('status'));	
			}
		}
		//$order->->join('customers', 'customers.id', '=', 'orders.customer_id')
				
		$orders = $order->with(array('customer'=>function($query){
			$query->select('*');
		}))->with(array('orderStatus'=>function($query){
			$query->select('*');
		}))->get();
		$total = 0;
		if($orders){
			foreach($orders as $order){
				$total+=$order->total_paid;
			}
		}
        return view('exports.sale', [
            'orders' => $orders,'total'=>$total,
        ]);
    }
}
