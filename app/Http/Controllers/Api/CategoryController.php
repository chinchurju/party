<?php

namespace App\Http\Controllers\Api;

use App\Shop\Products\Product;
use App\Shop\Categories\Category;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Shop\Products\Transformations\ProductTransformable;
use App\Shop\Categories\Repositories\CategoryRepository;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Shop\Brands\Repositories\BrandRepository;
use App\Shop\Brands\Repositories\BrandRepositoryInterface;
use DB;
use Illuminate\Http\Request;
class CategoryController extends Controller
{
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepo;
	private $brandRepo;

    /**
     * CategoryController constructor.
     *
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(
		CategoryRepositoryInterface $categoryRepository,
		BrandRepositoryInterface $brandRepository,
		ProductRepositoryInterface $productRepository
	)
    {
        $this->categoryRepo = $categoryRepository;
		$this->brandRepo    = $brandRepository;
		$this->productRepo  = $productRepository;
    }
	
	public function index()
	{
		$categories = Category::where('parent_id','=','1')->where('status','1')->with('children')->get();
		foreach($categories as $key=>$row){
			if($row->cover){
				$categories[$key]->cover = env("APP_URL")."/storage/".$row->cover;
			}
		}
		return response()->json(["status" => "200", "categories" =>$categories]);
	}

    /**
     * Find the category via the slug
     *
     * @param string $slug
     * @return \App\Shop\Categories\Category
     */
    public function getCategory(string $slug)
    {
        $category 	= $this->categoryRepo->findCategoryBySlug(['slug' => $slug]);
        $repo 		= new CategoryRepository($category);
        $products 	= $repo->findProducts()->where('status', 1)->all();
		$recommends_list  = $this->productRepo->listProducts('id','ASC');
		$brands = $this->brandRepo->listBrands();
		foreach($brands as $key=>$row){
			$count = DB::table('products')->where('brand_id','=',$row->id)->count(DB::raw('DISTINCT id'));
			if($count > 0){
				$brands[$key]->pdr_count = $count;
			}else{
				unset($brands[$key]);
			}
		}
		$categories = DB::table('categories')->where('status','=',1)->where('parent_id','>',1)->get();
		foreach($categories as $key=>$row){
			$count = DB::table('category_product')->where('category_id','=',$row->id)->count(DB::raw('DISTINCT id'));
			if($count > 0){
				$categories[$key]->pdr_count = $count;
			}else{
				unset($categories[$key]);
			}
		}
        return view('front.categories.category', [
            'products'   => $repo->paginateArrayResults($products, 20),
			'links'	     => $repo->paginateArrayResults($products, 20)->links(),
			'brands'     => $brands,
			'category'   => $category,
			'categories1'=> $categories,
			'recommends' => $repo->paginateArrayResults($products, 3)
        ]);
    }
	/**
     * Find the category via the slug
     *
     * @param string $slug
     * @return \App\Shop\Categories\Category
     */
    public function getProducts(int $id,Request $request)
        {
		
		$categories_arr =[];
		$brands_arr =[];
		$order_by_fl = 'id';
		$order_by = 'DESC';
		
        $category 	= $this->categoryRepo->findCategoryBySlug(['id' => $id]);

        $repo = new CategoryRepository($category);
        $products =  $repo->findProductsContry($request->all());
        
		$categories = Category::where('id','=',$category->id)->where('status','=',1)->with('children')->get();
		foreach($categories as $key=>$row){
			$count = DB::table('category_product')->where('category_id','=',$row->id)->count(DB::raw('DISTINCT id'));
			if($count > 0){
				$categories[$key]->pdr_count = $count;
			}else{
				unset($categories[$key]);
				//$categories[$key]->pdr_count = 0;
			}
		}

        if(request()->has("brands")){
			$brands_arr = explode(',', request()->input("brands"));
			if($order_by == "ASC"){
				$products =  $repo->findProducts()->where('status', 1)->whereIn('brand_id', $brands_arr)->sortBy($order_by_fl)->all();
			}
			else{
				$products =  $repo->findProducts()->where('status', 1)->whereIn('brand_id', $brands_arr)->sortByDesc($order_by_fl)->all();
			}

        }
      //  $brands_arr_s = [];
		$brands = $this->brandRepo->listBrands();
		foreach($brands as $key=>$row){
			$count = $repo->findProducts()->where('status', 1)->where('brand_id','=',$row->id)->count(DB::raw('DISTINCT id'));
			//$count =  DB::table('products')->where('brand_id','=',$row->id)->count(DB::raw('DISTINCT id'));
			if($count > 0){
				$brands[$key]->pdr_count = $count;
			}else{
				unset($brands[$key]);
			}
			
		}
		/*if($brands->count() <= 1){
			unset($brands[0]);
		}*/
		
		$categories = Category::where('id','=',$category->id)->where('status','=',1)->with('children')->get();
		foreach($categories as $key=>$row){
			$count = DB::table('category_product')->where('category_id','=',$row->id)->count(DB::raw('DISTINCT id'));
			if($count > 0){
				$categories[$key]->pdr_count = $count;
			}else{
				unset($categories[$key]);
				//$categories[$key]->pdr_count = 0;
			}
		}
		
        $dd = [];
		foreach($products as $key=>$row){
			$products[$key]->cover = env("APP_URL")."/storage/products/".$row->cover;
			if((int)$row->brand_id > 0){
				$brand = DB::table('brands')->where('id','=',$row->brand_id)->first();
				//print_r($brand);
				$products[$key]->brand_name = $brand->name;
			}
			if(!is_array($products[$key]))
				$dd[] = $products[$key];
		}
		$d = $repo->paginateArrayResults($products->toArray(), 12);
		$ss = (json_decode(json_encode($d)));
		$ss->data = (object)$ss->data;
		$br = (json_decode(json_encode($brands)));
		return response()->json(["status" => "200", "products" =>$ss,'brands' =>(object)$br,'brans_arr'=>$brands_arr]);
    }
}
