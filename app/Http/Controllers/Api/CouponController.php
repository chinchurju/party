<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Shop\Coupons;
use App\Http\Controllers\Controller;
use DB;
use Validator;
class CouponController extends Controller
{
	public function index(Request $request){
		$validator = Validator::make($request->all(), [
	        'code' =>'required'
	    ]);

	    if ($validator->fails()) {
	        return response()->json(array('status'=>'fail','message'=>$validator->errors()->all()), 200);
	    }

      // print_r($request->all());
      // exit();
	    $date = date('Y-m-d');
      $coupon_rs = DB::table('coupon')

      ->join('orders','orders.customer_id','=','coupon.userid')
      ->where('coupon.code','=',$request->code)->where('coupon.status','=',1)
      ->where('orders.coupon_code','=',$request->code)
      ->where('coupon.valid_from','<=',$date)
      ->where('coupon.expires_date','>=',$date)
      ->where('coupon.userid','=',$request->userid)
      // ->select('id','amount','code','name',DB::row('(SELECT id FROM orders WHERE customer_id = '.$request->userid.' AND coupon_code = coupon.code ) as orders'))
      ->get();
       // print_r($coupon_rs);
       //  exit();


      if(!empty($coupon_rs)){
        if(count($coupon_rs) > 0){
          return response()->json(['status' => 'fail', 'message' => 'coupon already used'], 200);
        }
        else{
          return response()->json(['status' => 'success', 'message' =>'coupon code apply'], 200);
        }
      }
      else{
        return response()->json(['status' =>'fail', 'message' => 'invalid coupon code'], 200);
      }

        // $coupn = DB::select("SELECT * FROM coupon c ,customers t ,orders o WHERE c.code=o.coupon_code and t.id=o.customer_id and c.userid = t.id" and 'valid_form','<=',$date and 'valid_to','>=',$date);
    }
    public function arrayConvert($arr){ 
        $new_ar=[];
        foreach($arr as $key=>$value){
           $new_ar[strtolower($key)]= $value;
        }
        return $new_ar;
    }

	public function listcoupon(Request $request){
 	   $input=$request->all();
       $header_values = getallheaders();
       $header_values=$this->arrayConvert($header_values);
       $date = date('Y-m-d');
	   $coupon = DB::select("SELECT * FROM coupon c ,customers t WHERE c.userid=t.id and t.login_token ='".$header_values['usertoken']."'");
       return response()->json(["status" => "200", "success" =>1,'coupon' => $coupon ]);
         
	}


}