<?php

namespace App\Http\Controllers\Api;

use App\Shop\Customers\Customer;
use App\Http\Controllers\Controller;
use App\Shop\Customers\Repositories\CustomerRepository;
use App\Shop\Customers\Repositories\Interfaces\CustomerRepositoryInterface;
use App\Shop\Customers\Requests\CreateCustomerRequest;
use App\Shop\Customers\Requests\RegisterCustomerRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Mail;
use Socialite;
use Google_Client;
use Google_Service_People;
use Log;
use DB;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/accounts';

    private $customerRepo;

    /**
     * Create a new controller instance.
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->middleware('guest');
        $this->customerRepo = $customerRepository;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Customer
     */
    protected function create(array $data)
    {
        return $this->customerRepo->createCustomer($data);
    }

    /**
     * @param RegisterCustomerRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function register(Request $request)
    {
       
		$v = Validator::make($request->all(), [
			'name' => 'required',
            'email' => 'required|email|unique:customers',
            'password' => 'required|min:8',
		]);
		if ($v->fails())
		{
			return response()->json(["status" => "200",'success'=>0, "error" =>$v->errors()]);
		}
        $customer = $this->create($request->except('_method', '_token'));
        Auth::login($customer);
		Mail::send('emails.customer.welcome', ['customer'=>$customer], function ($message) use ($customer) {
			$message
			   ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
			  
			  ->to($customer->email, $customer->name)
			  ->subject('Welcome - '.$customer->name);
		});
		return response()->json(["status" => "200",'success'=>1, "user" =>$customer]);
       // return redirect()->route('accounts');
    }
    
    public function social(Request $request)
    {
      
       Log::debug('ter',$request->all());
        $user = Socialite::driver('facebook')->userFromToken($request->token);
         Log::info(json_encode($user));
       // exit();
       if($user){
            $users = DB::table('customers')->where('fb_id',$user['id'])->first();
            if($users){
                
                 return response()->json(["status" => "200",'success'=>1, "user" =>$users]);
            }
            // else{
                // if (!$request->email || !$request->customertype)
                // {
                //     return response()->json(["status" => "200",'success'=>0, "msg" =>"-1","user" =>$request->all()]);
                // }
                else{
                     $v = Validator::make($request->all(), [
                        'name' => 'required',
                        'email' => 'email:customers',
                        'fb_id' => 'unique:customers',
                        // 'customertype' => 'required',
                    ]);
                    if ($v->fails())
                    {
                        return response()->json(["status" => "200",'success'=>0, "error" =>$v->errors()]);
                    }
                
                    $customer = $this->customerRepo->createCustomerSocial($request->except('_method', '_token'));
                    
    				$data['fb_id'] = $user['id'];

    			    DB::table('customers')->where('id', $customer->id)->update($data);
    			
                    Auth::login($customer);
                    Mail::send('emails.customer.welcome', ['customer'=>$customer], function ($message) use ($customer) {
                        $message
                           ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
                          
                          ->to($customer->email, $customer->name)
                          ->subject('Welcome - '.$customer->name);
                    });
                    return response()->json(["status" => "200",'success'=>1, "user" =>$customer]);
                }
            // }

        }else{
            return response()->json(["status" => "200",'success'=>0, "msg" =>"Something went wrong"]);
        }

    }
    
    
    public function google(Request $request)
    {
       //Log::debug('ter',$request->all());
       //$googleAuthCode = $request->input( 'token' );
       //$accessTokenResponse= Socialite::driver('google')->getAccessTokenResponse($googleAuthCode);
       //$accessToken=$accessTokenResponse["access_token"];
       //$expiresIn=$accessTokenResponse["expires_in"];
       //$idToken=$accessTokenResponse["id_token"];
       //$refreshToken=isset($accessTokenResponse["refresh_token"])?$accessTokenResponse["refresh_token"]:"";
       //$tokenType=$accessTokenResponse["token_type"];
       // $user = Socialite::driver('google')->userFromToken($accessToken);
       //Log::info(json_encode($user));
       //exit();
       Log::debug('ter',$request->all());
          $user = $request->all();
       Log::info(json_encode($user));
       // exit();
       if($user){
            $users = DB::table('customers')->where('gmail_id',$user['gmail_id'])->first();

            if($users){
                 return response()->json(["status" => "200",'success'=>1, "user" =>$users]);
            }

             // else{
                // if (!$request->customertype)
                // {
                //     return response()->json(["status" => "200",'success'=>0, "msg" =>"-1","user" =>$request->all()]);
                // }
            
                else{
                $v = Validator::make($request->all(), [
    			'name' => 'required',
    			// 'customertype' => 'required',
                'email' => 'required|email',
                'gmail_id' => 'unique:customers',
    	    	]);

        		if ($v->fails())
        		{
        			return response()->json(["status" => "200",'success'=>0, "error" =>$v->errors()]);
        		}
                $customer = $this->customerRepo->createCustomerSocial($request->except('_method', '_token'));
                    
    			$data['gmail_id'] = $user['gmail_id'];

    			DB::table('customers')->where('id', $customer->id)->update($data);
    			
                Auth::login($customer);
        		Mail::send('emails.customer.welcome', ['customer'=>$customer], function ($message) use ($customer) {
        			$message
        			  ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
        			  
        			  ->to($customer->email, $customer->name)
        			  ->subject('Welcome - '.$customer->name);
        		});
        		return response()->json(["status" => "200",'success'=>1, "user" =>$customer]);
               // return redirect()->route('accounts');
            }
          // }
       }
    }
}
