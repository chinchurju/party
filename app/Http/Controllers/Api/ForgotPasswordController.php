<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use Validator;
use DB;
use Illuminate\Http\Response;

class ForgotPasswordController extends Controller
{
	
	use SendsPasswordResetEmails;
	
    public function sendResetPassword(Request $request)
    {
       $res = $this->sendResetLinkEmail($request);
	   if(isset($res['email'])){
		    return response()->json(["status" => "200",'success'=>0, "error" =>$res['email']]);
	   }else{
		   return response()->json(["status" => "200",'success'=>1, "msg" =>'Reset Password Link send to email']); 
	   }
    }
	
}
