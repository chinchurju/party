<?php

namespace App\Http\Controllers\Api;

use App\Shop\Products\Product;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Shop\Products\Transformations\ProductTransformable;
use DB;
use Illuminate\Http\Request;
class ProductController extends Controller
{
    use ProductTransformable;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepo;

    /**
     * ProductController constructor.
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepo = $productRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search()
    {
        if (request()->has('q') && request()->input('q') != '') {
            $list = $this->productRepo->searchProduct(request()->input('q'));
        } else {
            $list = $this->productRepo->listProductsCountryWise();
        }

        $products = $list->where('status', 1)->map(function (Product $item) {
            return $this->transformProduct($item,true);
        });
	$products_ar =$this->productRepo->paginateArrayResults($products->all(),10);

       // print_r($products_ar);exit;
        $d = $products_ar->toArray();
        $d['data'] = array_values($d['data']);

		return response()->json(["status" => "200", 'products'=>$d]);
    }
	
    public function relativeProducts($id)
    {
        $products = DB::table('related_products')->join('products', 'products.id', '=', 'related_products.related_product_id')->where('product_id',$id)->get();
		foreach($products as $key=>$row){
			$products[$key]->cover = env("APP_URL")."/storage/products/".$row->cover;
		}
		return response()->json(["status" => "200", "related_products" =>$products]);
    }
    /**
     * Get the product
     *
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $product = $this->productRepo->findProductById($id);
		$product->cover = env("APP_URL")."/storage/products/".$product->cover;
        $images = $product->images()->get();
		foreach($images as $key=>$row){
			$images[$key]->src = env("APP_URL")."/storage/products/".$row->src;
		}
        $category = $product->categories()->first();
        $productAttributes = $product->attributes;
		if((int)$product->brand_id > 0){
				$brand = DB::table('brands')->where('id','=',$product->brand_id)->first();
				//print_r($brand);
				$product->payment = $brand->name;
		}
		$ss = [];
		foreach($productAttributes as $key=>$productAttribute){
			foreach($productAttribute->attributesValues as $key1=>$value){
			//	print_r($value);
				//$value['attribut'] = $value->attribute;
				$value->attribute = $value->attribute;
			///	$ss[$value->attribute->name][$value->id] =["value"=>$value->value,"id"=> $value->id];
				$productAttribute['attributesValues'][$key1] = $value;
			}
			$productAttributes[$key] = $productAttribute;
		}

        return response()->json(["status" => "200",  'product'=>$product,'images'=>$images,'productAttributes'=>$productAttributes,'category'=>$category]);
    }
}
