<?php

namespace App\Http\Controllers\Api;

use App\Shop\Carts\Requests\AddToCartRequest;
use App\Shop\Carts\Cart;
use App\Shop\Carts\Repositories\Interfaces\CartRepositoryInterface;
use App\Shop\Couriers\Repositories\Interfaces\CourierRepositoryInterface;
use App\Shop\ProductAttributes\Repositories\ProductAttributeRepositoryInterface;
use App\Shop\Products\Product;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Shop\Products\Repositories\ProductRepository;
use App\Shop\Products\Transformations\ProductTransformable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class CartController extends Controller
{
    use ProductTransformable;

    /**
     * @var CartRepositoryInterface
     */
    private $cartRepo;
	
	private $cart;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepo;

    /**
     * @var CourierRepositoryInterface
     */
    private $courierRepo;

    /**
     * @var ProductAttributeRepositoryInterface
     */
    private $productAttributeRepo;

    /**
     * CartController constructor.
     * @param CartRepositoryInterface $cartRepository
     * @param ProductRepositoryInterface $productRepository
     * @param CourierRepositoryInterface $courierRepository
     * @param ProductAttributeRepositoryInterface $productAttributeRepository
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        CourierRepositoryInterface $courierRepository,
        ProductAttributeRepositoryInterface $productAttributeRepository
	
    ) {
        $this->productRepo = $productRepository;
        $this->courierRepo = $courierRepository;
        $this->productAttributeRepo = $productAttributeRepository;
		
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$this->cart = new Cart();
		//print_r($this->cart->getCart());
		$c = $this->cart->getCart();
		if($c){
			foreach($c as $key=>$prd){
				$product = $prd->products;
				$product->cover = env("APP_URL")."/storage/products/".$product->cover;
				///$product = $this->productRepo->findProductById($product->id);
				$attribute = $product->attributes ;
				if($prd->product_attr_id){
					foreach($attribute as $attr){
						if($attr->id == $prd->product_attr_id){
							foreach($attr->attributesValues as $key1=>$value){
								//	print_r($value);
									//$value['attribut'] = $value->attribute;
									$value->attribute = $value->attribute;
								///	$ss[$value->attribute->name][$value->id] =["value"=>$value->value,"id"=> $value->id];
									$attr['attributesValues'][$key1] = $value;
							}
							$product->my_attr = $attr;
						}
					}
				}
				$c[$key]->products = $product;
			}
		}else{
			return response()->json(["status" => "200",'success'=>1,'msg'=>"No product found" ]);
		}
		return response()->json(["status" => "200",  'cartItems'=>$c,'subtotal' => $this->cart->getCartTotal(),'total' => $this->cart->getCartTotal()]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  AddToCartRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['quantity'] = 1;
	    $product = $this->productRepo->findProductById($request->input('product'));
        if ($product->attributes()->count() > 0) {
            $productAttr = $product->attributes()->where('default', 1)->first();
            if (isset($productAttr->sale_price)) {
                $product->price = $productAttr->price;
                if (!is_null($productAttr->sale_price)) {
                    $product->price = $productAttr->sale_price;
                }
            }
        }
		$option = null;
    //    if ($request->has('productAttribute')) {
         //   $attr = $this->productAttributeRepo->findProductAttributeById($request->input('productAttribute'));
          //  $product->price = $attr->price;
			//$option  = $request->input('productAttribute');
      //  }
		$this->cart = new Cart();
        $this->cart->addToCart($product, $request->input('quantity'),$option);
		return response()->json(["status" => "200",'success'=>1,'msg'=>"Item added successfuly" ]);
    }

	/**
     * Store a newly created resource in storage.
     *
     * @param  AddToCartRequest $request
     * @return \Illuminate\Http\Response
     */
    public function ajaxcart(AddToCartRequest $request)
    {
        $product = $this->productRepo->findProductById($request->input('product'));
        if ($product->attributes()->count() > 0) {
            $productAttr = $product->attributes()->where('default', 1)->first();
            if (isset($productAttr->sale_price)) {
                $product->price = $productAttr->price;

                if (!is_null($productAttr->sale_price)) {
                    $product->price = $productAttr->sale_price;
                }
            }
        }
        if ($request->has('productAttribute')) {
            $attr = $this->productAttributeRepo->findProductAttributeById($request->input('productAttribute'));
            $product->price = $attr->price;
        }
        $this->cartRepo->addToCart($product, $request->input('quantity'));
		$data['cart_item_cound'] = $this->cartRepo->countItems();
		$data['cart_item']	=	'<div class="Growler-notice" style="display: block; opacity: 1; background-color: rgb(220, 216, 214); color: rgb(0, 0, 0); position: relative; left: 0px; top: -3px;padding: 0 8px;">
									<div class="Growler-notice-head">Item Added</div>
									<div class="Growler-notice-body" style="padding: 4px 1px;">
										<h6><b>'.$product->name.' was added into cart.</b></h6>
										<img width="100%" src="'.url("storage/products/$product->cover").'">
									</div>
								</div>';
		return response()->json($data);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$this->cart = Cart::findOrFail($id);
		if($this->cart->customer_id == auth()->user()->id){
			$this->cart->updateQuantityInCart($id,$request->input('quantity'));
		}else{
			return response()->json(["status" => "200",'success'=>0,'msg'=>"An error occured while updating your cart" ]);
		}
		return response()->json(["status" => "200",'success'=>1,'msg'=>"Cart updated successfuly" ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$this->cart = Cart::findOrFail($id);
		if($this->cart->customer_id == auth()->user()->id){
			$this->cart->delete();
		}else{
			return response()->json(["status" => "200",'success'=>0,'msg'=>"An error occured while deleting your cart" ]);
		}
        return response()->json(["status" => "200",'success'=>1,'msg'=>"Removed to cart successful" ]);
    }
	public function clear()
    {
		$this->cart = new Cart();
		$this->cart->clear();
        return response()->json(["status" => "200",'success'=>1,'msg'=>"Cart is empty" ]);
    }
	
}
