<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Contacts\Contacts;
//validator is builtin class in laravel
use Validator;
use App;
use Lang;
use DB;
//for password encryption or hash protected
use Hash;
use App\Administrator;

//for authenitcate login data
use Auth;
use Mail;

//use Illuminate\Foundation\Auth\ThrottlesLogins;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

//for requesting a value 

class ContactsController extends Controller
{
	
	public function add(Request $request){
		
		$Contacts = new Contacts();
		if(count($Contacts->getContacts($request->input('email_address')) && ($request->input('phone'))) <= 0){
			$data = $request->except('_method', '_token');
			$Contacts->createContacts($data);
			$res = array();
			if($Contacts){
				$res['status'] = 200;
				$res['res_code'] = 1;
				$res['res_msg'] =  "Thank you! We will get in touch you soon.";
			}else{
				$res['status'] = 200;
				$res['res_code'] = 0;
				$res['res_msg'] =  "Error occured while inserting data";
			}
			return response()->json($res);
		}else{
			$res['status'] = 200;
			$res['res_code'] = 0;
			$res['res_msg'] =  "You are already Contacted!";
			return response()->json($res);
		}
				
	}
	public function send(Request $request){
		
		$data = $request->all();
		Mail::send('emails.contactus', [
            'contact' => $data,
        ], function ($message){
			$message
			 ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
			  ->to('rohith@whytecreations.in', "Admin")
			  ->subject('Contact Request');
		});
		
		return response()->json(["status" => "200", "success" =>1,'mag'=>'successfuly !.' ]);
	}
}
