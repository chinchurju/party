<?php

namespace App\Http\Controllers\Api;

use App\Shop\Products\Product;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;

use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Shop\Products\Transformations\ProductTransformable;
use DB;
use URL;
use	App\Libraries\Simpleimage;
use Illuminate\Http\Request;
use Lang;
class HomeController extends Controller
{
    use ProductTransformable;

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepo;

	/**
     * @var ProductRepositoryInterface
     */
    private $productRepo;
	
    /**
     * HomeController constructor.
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(CategoryRepositoryInterface $categoryRepository,ProductRepositoryInterface $productRepository)
    {
        $this->categoryRepo = $categoryRepository;
		$this->productRepo = $productRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $cat1 = $this->categoryRepo->findCategoryById(1);
        $cat2 = $this->categoryRepo->findCategoryById(2);
		$banners = DB::table('banners')->where('status', '=', 1)->where('expires_date', '>=', date('Y-m-d 23:59:59'))->where('type', '=','home1' )->get();
		$banners2 = DB::table('banners')->where('status', '=', 1)->where('expires_date', '>=', date('Y-m-d 23:59:59'))->where('type', '=','home2' )->get();
		$blocks = DB::table('blocks')->where('status', '=', '1')->orderBy('display_order', 'ASC')->get();
		 
		$list = $this->productRepo->listProducts();
       

        $products = $list->where('status', 1)->map(function (Product $item) {
            return $this->transformProduct($item);
        });

       
		$products = $this->productRepo->paginateArrayResults($products->all(), 6);
		
        return view('front.index', compact('cat1', 'cat2','banners','banners2','products','blocks'));
    }
	public function banners()
    {
		$banners = DB::table('banners')->where('status', '=', 1)->where('expires_date', '>=', date('Y-m-d 23:59:59'))->where('type', '=','home1' )->get();
		foreach($banners as $key=>$row){
			$banners[$key]->banners_image = env("APP_URL").'/'.$row->banners_image;
		}
		return response()->json(["status" => "200", "banners" =>$banners]);
    }
    
     public function listingPages(Request $request){
        $title          = array('pageTitle' => Lang::get("labels.Pages"));
        $language_id    =   '1';        
        
        $pages = DB::table('pages_description')
        ->select('slug','description','name')
        ->where('pages_description.language_id','=', $language_id)->get();
        
        $result["pages"] = $pages;
        
        return response()->json(["status" => "200","success" =>1, "pages" =>$pages]);




    }	
	
}
