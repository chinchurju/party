<?php

namespace App\Http\Controllers\Front;

use App\Shop\Products\Product;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;

use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Shop\Products\Transformations\ProductTransformable;
use DB;
use	App\Libraries\Simpleimage;
use App\Shop\Wishlist\Wishlist;
class HomeController extends Controller
{
    use ProductTransformable;

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepo;

	/**
     * @var ProductRepositoryInterface
     */
    private $productRepo;
	
    /**
     * HomeController constructor.
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(CategoryRepositoryInterface $categoryRepository,ProductRepositoryInterface $productRepository)
    {
        $this->categoryRepo = $categoryRepository;
		$this->productRepo = $productRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $cat1 = $this->categoryRepo->findCategoryById(1);
        $cat2 = $this->categoryRepo->findCategoryById(2);
		$banners = DB::table('banners')->where('status', '=', 1)->where('expires_date', '>=', date('Y-m-d 23:59:59'))->get();
		$blocks = DB::table('blocks')->where('status', '=', '1')->orderBy('display_order', 'ASC')->get();
		$list = $this->productRepo->listProducts();
        $products = $list->where('status', 1)->map(function (Product $item) {
            return $this->transformProduct($item);
        });
		$products = $this->productRepo->paginateArrayResults($products->all(), 10);
		$Wishlist = new Wishlist();
		$mywishlist =  $Wishlist->getWishlist();
        return view('front.index', compact('cat1', 'cat2','banners','products','blocks','mywishlist'));
    }
}
