<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Subscribers\Subscribers;
//validator is builtin class in laravel
use Validator;
use App;
use Lang;
use DB;
//for password encryption or hash protected
use Hash;
use App\Administrator;

//for authenitcate login data
use Auth;

//use Illuminate\Foundation\Auth\ThrottlesLogins;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

//for requesting a value 

class PageController extends Controller
{
	public function index(string $slug){
		
		$pages = DB::table('pages')
			->leftJoin('pages_description', 'pages_description.page_id', '=', 'pages.page_id')
			->select('pages.*','pages_description.description','pages_description.language_id','pages_description.name' ,'pages_description.page_description_id')
			->where('pages.slug','=', $slug)
			->where('pages.status','=', 1)
			->get();
		return view('front.page', ['page'=>$pages]);
	}
}
