
<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//validator is builtin class in laravel
use Validator;
use App;
use Lang;
use DB;
//for password encryption or hash protected
use Hash;
use App\Administrator;

//for authenitcate login data
use Auth;
use Mail;

//use Illuminate\Foundation\Auth\ThrottlesLogins;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

//for requesting a value 

class ContactsController extends Controller
{
	
public function send(Request $request){
		
		$data = $request->all();
		Mail::send('emails.contactus', [
            'contact' => $data,
        ], function ($message){
			$message
			   ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
			  ->to('info@uni.com', "Rohith")
			  ->subject('Address');
		});
		
	}
}