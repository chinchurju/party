<?php

namespace App\Http\Controllers\Admin\Orders;

use App\Shop\Addresses\Repositories\Interfaces\AddressRepositoryInterface;
use App\Shop\Addresses\Transformations\AddressTransformable;
use App\Shop\Couriers\Courier;
use App\Shop\Couriers\Repositories\CourierRepository;
use App\Shop\Couriers\Repositories\Interfaces\CourierRepositoryInterface;
use App\Shop\Customers\Customer;
use App\Shop\Customers\Repositories\CustomerRepository;
use App\Shop\Customers\Repositories\Interfaces\CustomerRepositoryInterface;
use App\Shop\Orders\Order;
use App\Shop\Orders\Repositories\Interfaces\OrderRepositoryInterface;
use App\Shop\Orders\Repositories\OrderRepository;
use App\Shop\OrderStatuses\OrderStatus;
use App\Shop\OrderStatuses\Repositories\Interfaces\OrderStatusRepositoryInterface;
use App\Shop\OrderStatuses\Repositories\OrderStatusRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Mail;
use App\Libraries\Fcm;
use DB;
use App\Shop\Devices\Device;
class OrderController extends Controller
{
    use AddressTransformable;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepo;

    /**
     * @var CourierRepositoryInterface
     */
    private $courierRepo;

    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepo;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepo;

    /**
     * @var OrderStatusRepositoryInterface
     */
    private $orderStatusRepo;

    public function __construct(
        OrderRepositoryInterface $orderRepository,
        CourierRepositoryInterface $courierRepository,
        AddressRepositoryInterface $addressRepository,
        CustomerRepositoryInterface $customerRepository,
        OrderStatusRepositoryInterface $orderStatusRepository
    ) {
        $this->orderRepo = $orderRepository;
        $this->courierRepo = $courierRepository;
        $this->addressRepo = $addressRepository;
        $this->customerRepo = $customerRepository;
        $this->orderStatusRepo = $orderStatusRepository;

        $this->middleware(['permission:update-order, guard:employee'], ['only' => ['edit', 'update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->orderRepo->listOrders('id', 'desc');

        if (request()->has('q')) {
            $list = $this->orderRepo->searchOrder(request()->input('q') ?? '');
        }
        $orders = $this->orderRepo->paginateArrayResults($this->transFormOrder($list), 10);
        return view('admin.orders.list', ['orders' => $orders]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $orderId
     * @return \Illuminate\Http\Response
     */
    public function show($orderId)
    {
        $order = $this->orderRepo->findOrderById($orderId);
        //$order->courier = $this->courierRepo->findCourierById($order->courier_id);
       // $order->address = $this->addressRepo->findAddressById($order->address_id);

        $orderRepo = new OrderRepository($order);

        $items = $orderRepo->listOrderedProducts();

        return view('admin.orders.show', [
            'order' => $order,
            'items' => $items,
            'customer' => $this->customerRepo->findCustomerById($order->customer_id),
            'currentStatus' => $this->orderStatusRepo->findOrderStatusById($order->order_status_id),
            'payment' => $order->payment,
            'user' => auth()->guard('employee')->user()
        ]);
    }

    /**
     * @param $orderId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($orderId)
    {
        $order = $this->orderRepo->findOrderById($orderId);
        //$order->courier = $this->courierRepo->findCourierById($order->courier_id);
       // $order->address = $this->addressRepo->findAddressById($order->address_id);

        $orderRepo = new OrderRepository($order);

        $items = $orderRepo->listOrderedProducts();

        return view('admin.orders.edit', [
            'statuses' => $this->orderStatusRepo->listOrderStatuses(),
            'order' => $order,
            'items' => $items,
            'customer' => $this->customerRepo->findCustomerById($order->customer_id),
            'currentStatus' => $this->orderStatusRepo->findOrderStatusById($order->order_status_id),
            'payment' => $order->payment,
            'user' => auth()->guard('employee')->user()
        ]);
    }

    /**
     * @param Request $request
     * @param $orderId
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $orderId)
    {
        $order = $this->orderRepo->findOrderById($orderId);
        $orderRepo = new OrderRepository($order);

        if ($request->has('total_paid') && $request->input('total_paid') != null) {
            $orderData = $request->except('_method', '_token');
        } else {
            $orderData = $request->except('_method', '_token', 'total_paid');
        }

        $orderRepo->updateOrder($orderData);
		//$order->courier = $this->courierRepo->findCourierById($order->courier_id);
		//$order->address = $this->addressRepo->findAddressById($order->address_id);

		$orderRepo = new OrderRepository($order);

		$items = $orderRepo->listOrderedProducts();
		$shippingFee = 'Free';
		/*if($order->courier->is_free == 0){
			$shippingFee = $order->courier->cost;
		}*/
		$customer = $this->customerRepo->findCustomerById($order->customer_id);
		$currentStatus = $this->orderStatusRepo->findOrderStatusById($order->order_status_id);
		$customer->order_id = $order->reference;
		$customer->order_status = $currentStatus->name;
		
        $devices =Device::where('customers_id',$customer->id)->get();
       
        $device_ids_adroid = [];
        $device_ids_ios = [];
            if($devices->count()>0){
            foreach($devices as $row){
                if($row->device_type==1)
                    $device_ids_ios[] = $row->device_id;
                else    
                $device_ids_adroid[] = $row->device_id;
            }
            $msg = "Request ID #".$order->reference." has been ".$currentStatus->name;
            if($device_ids_adroid){
                // Fcm::sendPushNotificationToFCMSever($device_ids,'Request '.$customer->order_status,$msg,array('order_id'=>$orderId));
                 
                Fcm::sendPushNotificationToFCMSeverAndroid($device_ids_adroid,'Request '.$customer->order_status,$msg,array('order_id'=>$orderId));
            }
            if($device_ids_ios){
                Fcm::sendPushNotificationToFCMSever($device_ids_ios,'Request '.$customer->order_status,$msg,array('order_id'=>$orderId));
                 
            }
            DB::table('notification')->insert([
                'customer_id' =>  $order->customer_id,
                'order_id'    =>  $orderId,
                'title'       =>  'Request '.$customer->order_status,
                'message'     =>  $msg,
                'date'        =>  date('Y-m-d H:i:s')
            ]);
        }
		Mail::send('emails.order.order_status', [
			'order' => $order,
			'items' => $items,
			'shippingfee'=>$shippingFee,
			'currentStatus'=>$currentStatus,
			'customer' => $customer,
			'payment' => $order->payment,
			'user' => auth()->guard('employee')->user()
		], function ($message) use ($customer) {
			$message
			  ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
			  ->to($customer->email, $customer->name)
			  ->subject('Request '.$customer->order_status.' #'.$customer->order_id);
		});
        return redirect()->route('admin.orders.edit', $orderId);
    }

    /**
     * Generate order invoice
     *
     * @param int $id
     * @return mixed
     */
    public function generateInvoice(int $id)
    {
        $order = $this->orderRepo->findOrderById($id);
        $data = [
            'order' => $order,
            'products' => $order->products,
            'customer' => $order->customer,
            'courier' => $order->courier,
           // 'address' => $this->transformAddress($order->address),
            			//'shipping_address' => $this->transformAddress($order->shippingAddress),
            'status' => $order->orderStatus,
            'payment' => $order->paymentMethod
        ];

        $pdf = app()->make('dompdf.wrapper');
        $pdf->loadView('invoices.orders', $data)->stream();
        return $pdf->stream();
    }
    public function sendLocation(int $id,Request $request)
    {
        $order = $this->orderRepo->findOrderById($id);
        $orderRepo = new OrderRepository($order);
        $orderData['order_status_id'] = 3;
        $orderRepo->updateOrder($orderData);
        
        $items = $orderRepo->listOrderedProducts();
        $customer = $this->customerRepo->findCustomerById($order->customer_id);
        $data = $request->all();
        $customer->order_id = $order->reference;
        $data['order_id'] = $order->reference;
        $img_location = 'https://maps.googleapis.com/maps/api/staticmap?&zoom=13&size=600x300&&markers=color:blue%7Clabel:S%7C'.$order->location_latitude.','.$order->location_longitude.'&key=AIzaSyBXqOs4tDhl2cuTeIS63KRS78RGhNQYMy0';
        Mail::send('emails.order.service_request', [
          'order' => $order,
            'items' => $items,
            'location_url' => 'https://maps.google.com/maps?q='.$order->location_latitude.','.$order->location_longitude.'&hl',
            'img_location' => $img_location,
            'customer' => $customer,
            'currentStatus' => $this->orderStatusRepo->findOrderStatusById($order->order_status_id),
            'payment' => $order->payment,
            'user' => auth()->guard('employee')->user()
        ], function ($message) use ( $data) {
            $message
              ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
              ->to($data['email'])
              ->subject('Order  #'.$data['order_id']);
        });

        return redirect()->route('admin.orders.show', $id);



     
    }
    /**
     * @param Collection $list
     * @return array
     */
    private function transFormOrder(Collection $list)
    {
        $courierRepo = new CourierRepository(new Courier());
        $customerRepo = new CustomerRepository(new Customer());
        $orderStatusRepo = new OrderStatusRepository(new OrderStatus());

        return $list->transform(function (Order $order) use ($courierRepo, $customerRepo, $orderStatusRepo) {
           // $order->courier = $courierRepo->findCourierById($order->courier_id);
            $order->customer = $customerRepo->findCustomerById($order->customer_id);
            $order->status = $orderStatusRepo->findOrderStatusById($order->order_status_id);
            //print_r($order->products->toArray());exit;
          
            $ser_name="";
            foreach( $order->products as $item){
               $ser_name .= $item->name.","; 
                
            }
           // echo  $ser_name;exit;
            $order->service_name = rtrim($ser_name,',');
            return $order;
        })->all();
    }
}
