<?php
/*
Project Name: IonicEcommerce
Project URI: http://ionicecommerce.com
Author: VectorCoder Team
Author URI: http://vectorcoder.com/
Version: 2.1
*/
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

//validator is builtin class in laravel
use Validator;

use App;
use Lang;

use DB;
//for password encryption or hash protected
use Hash;
use App\Administrator;

//for authenitcate login data
use Auth;

//use Illuminate\Foundation\Auth\ThrottlesLogins;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

//for requesting a value 
use Illuminate\Http\Request;
//use Illuminate\Routing\Controller;


class AdminCouponController extends Controller
{
	
	//listingTaxClass
	public function listingCoupon(Request $request){
		$title = array('pageTitle' => Lang::get("labels.ListingBanners"));		
		
		$result = array();
		$message = array();
			
		$coupon = DB::table('coupon')->paginate(20);
		
		$result['message'] = $message;
		$result['coupon'] = $coupon;
		
		return view("admin.listingCoupon", $title)->with('result', $result);
	}
	
	//addTaxClass
	public function addCoupon(Request $request){
		$title = array('pageTitle' => Lang::get("labels.AddBanner"));
		$tbl = "customers";
		$customers = DB::table($tbl)->where('status', '=', 1)->get();
		$tbll = "categories";
		$categories = DB::table($tbll)->where('status', '=', 1)->get();
		$result = array();
		$message = array();
		$result['message'] = $message;
		$result['customers'] = $customers;
		$result['categories'] = $categories;
		return view("admin.addCoupon", $title)->with('result', $result);


	}
	
	//addNewZone	
	public function addNewCoupon(Request $request){
		
		$expiryDate = str_replace('/', '-', $request->expires_date);
		$expiryDateFormate = date('Y-m-d H:i:s', strtotime($expiryDate));
		$vali = str_replace('/', '-', $request->valid_from);
		$valid = date('Y-m-d H:i:s', strtotime($vali));
		
		
		DB::table('coupon')->insert([
				'name'  		 =>   $request->name,
				'code'  		 =>   $request->code,
				'userid' 	     =>   $request->userid,
				'catid'			 =>   $request->catid,
				'amount'         =>   $request->amount,
				'status'	 	 =>   $request->status,
				'valid_from'	 =>   $valid,
				'expires_date'	 =>	  $expiryDateFormate,
				]);
										
		$message = "Coupon has been added successfully!";
		return redirect()->back()->withErrors([$message]);
	}
	
	//editTaxClass
	public function editCoupon(Request $request){		
		$title = array('pageTitle' => Lang::get("labels.EditBanner"));
		$result = array();		
		$result['message'] = array();
		$customers = DB::table('customers')->where('status', '=', 1)->get();
		$categories = DB::table('categories')->where('status', '=', 1)->get();
		$coupon = DB::table('coupon')->where('id', $request->id)->get();
		$result['coupon'] = $coupon;
		$result['customers'] = $customers;
		$result['categories'] = $categories;
	    return view("admin.editCoupon",$title)->with('result', $result);
	}
	
	//updateTaxClass
	public function updateCoupon(Request $request){
		
		$title = array('pageTitle' => Lang::get("labels.EditBanner"));
		
		$expiryDate = str_replace('/', '-', $request->expires_date);
		$expiryDateFormate = date('Y-m-d H:i:s', strtotime($expiryDate));
		$vali = str_replace('/', '-', $request->valid_from);
		$valid = date('Y-m-d H:i:s', strtotime($vali));
		
		$message = "coupon code has been updated";
				
		$countryUpdate = DB::table('coupon')->where('id', $request->id)->update([
			'name'  		 =>   $request->name,
			'code'  		 =>   $request->code,
			'userid' 	     =>   $request->userid,
			'catid'			 =>   $request->catid,
			'amount'         =>   $request->amount,
			'status'	 	 =>   $request->status,
			'valid_from'	 =>   $valid,
			'expires_date'	 =>	  $expiryDateFormate,
		]);
				
		return redirect()->back()->withErrors([$message ]);
	}
	
	//deleteCountry
	public function deleteCoupon(Request $request){
		DB::table('coupon')->where('id', $request->id)->delete();
		return redirect()->back()->withErrors("Coupon has been Deleted");
	}
}                                                                                     
