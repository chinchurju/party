<?php

namespace App\Http\Controllers\Admin;
use App\Shop\Orders\Order;
use App\Shop\Products\Product;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Log;
use Carbon\Carbon;
use App\Shop\Categories\Category;
class DashboardController extends Controller
{
    public function index(Request $request)
    {
		$language_id      = 	'1';
		$result 		  =		array();
		
		$reportBase		  = 	$request->reportBase;
		

		//recently order placed
		$orders = DB::table('orders')
			//->LeftJoin('currencies', 'currencies.code', '=', 'orders.currency')
			->orderBy('created_at','DESC')
			->get();

		//print_r($orders);
		
		$index = 0;
		$total_price = array();
		foreach($orders as $key=>$orders_data){
			$orders_products = DB::table('order_product')
				->select('product_price', DB::raw('SUM(product_price) as total_price'))
				->where('order_id', '=' ,$orders_data->id)
				->groupBy('product_price')
				->get();
				
			if($orders_products->count()>0)	
				$orders[$key]->total_price = $orders_products[0]->total_price;
			
			/*$orders_status_history = DB::table('order_statuses')
				->select('order_statuses.name', 'order_statuses.id')
				->where('id', '=', $orders_data->order_status_id);
				
			$orders[$index]->orders_status_id = $orders_status_history[0]->order_status_id;
			$orders[$index]->orders_status = $orders_status_history[0]->name;*/
			
			$index++;				
		}
		
		$compeleted_orders = 0;
		$pending_orders = 0;
		/*foreach($orders as $orders_data){
			
			if($orders_data->orders_status_id=='2')
			{
				$compeleted_orders++;
			}
			if($orders_data->orders_status_id=='1')
			{
				$pending_orders++;
			}
		}*/
		//print_r($orders);
		//$result['orders'] = array_slice($orders, '0', '10');
		$result['orders'] = $orders->chunk(10);
		$result['pending_orders'] = $pending_orders;
		$result['compeleted_orders'] = $compeleted_orders;
		$result['total_orders'] = count($orders);
		$result['inprocess'] = count($orders)-$pending_orders-$compeleted_orders;
		//add to cart orders
		//$cart = DB::table('customers_basket')->get();
		
		$result['cart'] = 150;//count($cart);
		
		//Rencently added products
		$recentProducts = DB::table('products')
			->orderBy('products.id', 'DESC')
			->paginate(8);

        $result['outOfStock']=Product::where('quantity',0)->count();
			
		$result['recentProducts'] = array();//$recentProducts;
		
		//products
		$products = DB::table('products')

			->orderBy('products.id', 'DESC')
			->get();

		$result['totalProducts'] = count($products);
		
		$customers = DB::table('customers')
			//->LeftJoin('customers_info','customers_info.customers_info_id','=', 'customers.customers_id')
			->orderBy('created_at','DESC')
			->get();
			
		//$result['recentCustomers'] = array_slice($customers, '0', '21');
		$result['recentCustomers'] = $customers->chunk(21);
		//print_r($result['recentCustomers']);
		//print '<br><br><br>';
//		foreach ($result['recentCustomers']  as $recentCustomers){
//			print_r($recentCustomers[0]->customers_id);	
//		}
		$result['totalCustomers'] = count($customers);
		$result['reportBase'] = $reportBase;	
		
		//get function from other controller
		//$myVar = new AdminSiteSettingController();
		//$currency = $myVar->getSetting();
		$result['currency'] = "QAR";//$currency;
        return view('admin.dashboard',compact('result'));//->with('result', $result);;
    }


    public function getOrderData(){

        $fullList=array();$daysArray=array();

        $day= Carbon::today();
		//echo Carbon::today()->toDateString()." 00:00:00";exit;
        $visitData=Order::whereBetween('created_at', [Carbon::today()->toDateString()." 00:00:00", Carbon::today()->toDateString()." 23:59:59"])->count();
        $fullList[0]=$visitData;
        $daysArray[0]=Carbon::today()->format('Y-m-d');
        for ($i=1;$i<28;$i++){
			
			$d = $day->subDay()->toDateString();
			$visitData=Order::whereBetween('created_at', [$d." 00:00:00",$d." 23:59:59"])->count();
            //$visitData=Order::where('created_at', '=', $day->subDay()->toDateString()." 23:59:59")->count();
            $fullList[$i]=$visitData;
            $daysArray[$i]=$day->format('Y-m-d');

        }
        return response()->json([
            'data' => $fullList,
            'dates' => $daysArray
        ]);
    }


	
	//public function productSaleReport($reportBase){
	public function productSaleReport(Request $request){
		
		$saleData = array();
		$date = time();
		$reportBase = $request->reportBase;
		//$reportBase = 'last_year';
		
		if($reportBase=='this_month'){
			
			$dateLimit 	= date('d',  $date);
			
			//for current month		
			for($j = 1; $j <= $dateLimit; $j++){
				
				$dateFrom   = date('Y-m-'.$j.' 00:00:00', time());
				$dateTo 	= date('Y-m-'.$j.' 23:59:59', time());
				
				//sold products
				$orders = DB::table('orders')
					->whereBetween('date_purchased', [$dateFrom, $dateTo])
					->get();
			
				$totalSale = 0;
				foreach($orders as $orders_data){
					
					$orders_status = DB::table('orders_status_history')
						->where('orders_id', '=', $orders_data->orders_id)
						->orderby('date_added', 'DESC')->limit(1)->get();
						
					if($orders_status[0]->orders_status_id != 3){
						$totalSale++;
					}
				}
				
				//purchase products
				$products = DB::table('products')
					->select('products_quantity', DB::raw('SUM(products_quantity) as products_quantity'))
					->whereBetween('products_date_added', [$dateFrom, $dateTo])
					->get();
									
				$saleData[$j-1]['date'] = date('d M',strtotime($dateFrom));
				$saleData[$j-1]['totalSale'] = $totalSale;
				
				if(empty($products[0]->products_quantity)){
					$producQuantity = 0;
				}else{
					$producQuantity = $products[0]->products_quantity;
				}
				
				$saleData[$j-1]['productQuantity'] = $producQuantity;
			}
			
		}else if($reportBase=='last_month'){
			$datePrevStart =  date("Y-n-j", strtotime("first day of previous month"));
			$datePrevEnd   =  date("Y-n-j", strtotime("last day of previous month"));
			
			$dateLimit 	= date('d',  strtotime($datePrevEnd));
			
			//for last month		
			for($j = 1; $j <= $dateLimit; $j++){
				
				$dateFrom   = date('Y-m-'.$j.' 00:00:00', strtotime($datePrevStart));
				$dateTo 	= date('Y-m-'.$j.' 23:59:59', strtotime($datePrevEnd));
				
				//sold products
				$orders = DB::table('orders')
					->whereBetween('date_purchased', [$dateFrom, $dateTo])
					->get();
			
				$totalSale = 0;
				foreach($orders as $orders_data){
					
					$orders_status = DB::table('orders_status_history')
						->where('orders_id', '=', $orders_data->orders_id)
						->orderby('date_added', 'DESC')->limit(1)->get();
						
					if($orders_status[0]->orders_status_id != 3){
						$totalSale++;
					}
				}
				
				//purchase products
				$products = DB::table('products')
					->select('products_quantity', DB::raw('SUM(products_quantity) as products_quantity'))
					->whereBetween('products_date_added', [$dateFrom, $dateTo])
					->get();
									
				$saleData[$j-1]['date'] = date('d M',strtotime($dateFrom));
				$saleData[$j-1]['totalSale'] = $totalSale;
				
				if(empty($products[0]->products_quantity)){
					$producQuantity = 0;
				}else{
					$producQuantity = $products[0]->products_quantity;
				}
				
				$saleData[$j-1]['productQuantity'] = $producQuantity;
			}
			
		}else if($reportBase=='last_year'){
			
			$dateLimit 	=   date("Y", strtotime("-1 year"));
			
			$datePrevStart =  date("Y-n-j", strtotime("first day of previous month"));
			$datePrevEnd   =  date("Y-n-j", strtotime("last day of previous month"));
			
			//for last year		
			for($j = 1; $j <= 12; $j++){
				$dateFrom   = date( $dateLimit.'-'.$j.'-1 00:00:00', strtotime($datePrevStart));
				$dateTo 	= date( $dateLimit.'-'.$j.'-31 23:59:59', strtotime($datePrevEnd));
				
				//sold products
				$orders = DB::table('orders')
					->whereBetween('date_purchased', [$dateFrom, $dateTo])
					->get();
			
				$totalSale = 0;
				foreach($orders as $orders_data){
					
					$orders_status = DB::table('orders_status_history')
						->where('orders_id', '=', $orders_data->orders_id)
						->orderby('date_added', 'DESC')->limit(1)->get();
						
					if($orders_status[0]->orders_status_id != 3){
						$totalSale++;
					}
				}
				
				//purchase products
				$products = DB::table('products')
					->select('products_quantity', DB::raw('SUM(products_quantity) as products_quantity'))
					->whereBetween('products_date_added', [$dateFrom, $dateTo])
					->get();
									
				$saleData[$j-1]['date'] = date('M Y',strtotime($dateFrom));
				$saleData[$j-1]['totalSale'] = $totalSale;
				
				if(empty($products[0]->products_quantity)){
					$producQuantity = 0;
				}else{
					$producQuantity = $products[0]->products_quantity;
				}
				
				$saleData[$j-1]['productQuantity'] = $producQuantity;
			}
		}else{
			$reportBase = str_replace('dateRange','', $reportBase);
		$reportBase = str_replace('=','', $reportBase);
		$reportBase = str_replace('-','/', $reportBase);
		
		$dateFrom = substr($reportBase,0,10);
		$dateTo = substr($reportBase,11,21);
		
		$diff = abs(strtotime($dateFrom) - strtotime($dateTo));
		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
		$totalDays = floor($diff / (60 * 60 * 24));
	//	print ('day: '.$days.' months: '.$months.' years: '.$years.'<br>');
		$totalMonths = floor($diff / 60 / 60 / 24 / 30);
		
		if($diff == 0 && $days == 0 && $years == 0 && $months == 0){
			//print 'asdsad';
			
			$dateLimitFrom 	= date('G',  strtotime($dateFrom));
			$dateLimitTo 	= date('d',  strtotime($dateTo));
			$selecteddate 	= date('m',  strtotime($dateFrom));
			$selecteddate	= date('Y',  strtotime($dateFrom));
			
			//for current month		
			for($j = 1; $j <= 24; $j++){
								
				$dateFrom   = date('Y-m-d'.' '.$j.':00:00', strtotime($dateFrom));
				$dateTo   = date('Y-m-d'.' '.$j.':59:59', strtotime($dateFrom));
				
				//sold products
				$orders = DB::table('orders')
					->whereBetween('date_purchased', [$dateFrom, $dateTo])
					->get();
			
				$totalSale = 0;
				foreach($orders as $orders_data){
					
					$orders_status = DB::table('orders_status_history')
						->where('orders_id', '=', $orders_data->orders_id)
						->orderby('date_added', 'DESC')->limit(1)->get();
						
					if($orders_status[0]->orders_status_id != 3){
						$totalSale++;
					}
				}
				
				//purchase products
				$products = DB::table('products')
					->select('products_quantity', DB::raw('SUM(products_quantity) as products_quantity'))
					->whereBetween('products_date_added', [$dateFrom, $dateTo])
					->get();
									
				$saleData[$j-1]['date'] = date('h a',strtotime($dateFrom));
				$saleData[$j-1]['totalSale'] = $totalSale;
				
				if(empty($products[0]->products_quantity)){
					$producQuantity = 0;
				}else{
					$producQuantity = $products[0]->products_quantity;
				}
				
				$saleData[$j-1]['productQuantity'] = $producQuantity;
				//print $dateLimitFrom.'<br>';
				
			}
			
		}else if($days > 1 && $years == 0 && $months == 0){
			
			//print 'daily';
			
			$dateLimitFrom 	= date('d',  strtotime($dateFrom));
			$dateLimitTo 	= date('d',  strtotime($dateTo));
			$selectedMonth 	= date('m',  strtotime($dateFrom));
			$selectedYear	= date('Y',  strtotime($dateFrom));
			//print $selectedYear;
			
			//for current month		
			for($j = 1; $j <= $totalDays; $j++){
				
				//print 'dateFrom: '.date('Y-m-'.$j.' 00:00:00', time()).'dateTo: '.date('Y-m-'.$j.' 23:59:59', time());
				//print '<br>';
				
				$dateFrom   = date($selectedYear.'-'.$selectedMonth.'-'.$dateLimitFrom, strtotime($dateFrom));
				//$dateTo 	= date('Y-m-'.$j.' 23:59:59', time());
				//print $dateFrom .'<br>';
				$lastday   =  date('t',strtotime($dateFrom));
				//print 'lastday: '.$lastday .' <br>';
				
				
				//sold products
				$orders = DB::table('orders')
					->whereBetween('date_purchased', [$dateFrom, $dateTo])
					->get();
			
				$totalSale = 0;
				foreach($orders as $orders_data){
					
					$orders_status = DB::table('orders_status_history')
						->where('orders_id', '=', $orders_data->orders_id)
						->orderby('date_added', 'DESC')->limit(1)->get();
						
					if($orders_status[0]->orders_status_id != 3){
						$totalSale++;
					}
				}
				
				//purchase products
				$products = DB::table('products')
					->select('products_quantity', DB::raw('SUM(products_quantity) as products_quantity'))
					->whereBetween('products_date_added', [$dateFrom, $dateTo])
					->get();
									
				$saleData[$j-1]['date'] = date('d M',strtotime($dateFrom));
				$saleData[$j-1]['totalSale'] = $totalSale;
				
				if(empty($products[0]->products_quantity)){
					$producQuantity = 0;
				}else{
					$producQuantity = $products[0]->products_quantity;
				}
				
				$saleData[$j-1]['productQuantity'] = $producQuantity;
				//print $dateLimitFrom.'<br>';
				if($dateLimitFrom == $lastday ){
					$dateLimitFrom = '1';
					$selectedMonth++;
					
				}else{
					$dateLimitFrom++;
				}
				
				if($selectedMonth > 12){
					$selectedMonth = '1';
					$selectedYear++;
				}
			}
		}else if($months >= 1 && $years == 0){
			
			//for check if date range enter into another month
			if($days>0){
				$months+=1;	
			}
						
			$dateLimitFrom 	= date('d',  strtotime($dateFrom));
			$dateLimitTo 	= date('d',  strtotime($dateTo));
			$selectedMonth 	= date('m',  strtotime($dateFrom));
			$selectedYear	= date('Y',  strtotime($dateFrom));
			//print $selectedMonth;
			
			$i = 0;
			//for current month		
			for($j = 1; $j <= $months; $j++){
				if($j==$months){
					$lastday = $dateLimitTo;
				}else{
					$lastday  =  date('t',strtotime($dateLimitFrom.'-'.$selectedMonth.'-'.$selectedYear));
				}
				
				$dateFrom   = date($selectedYear.'-'.$selectedMonth.'-'.$dateLimitFrom, strtotime($dateFrom));
				$dateTo   = date($selectedYear.'-'.$selectedMonth.'-'.$lastday, strtotime($dateTo));
				//print $dateFrom.' '.$dateTo.'<br>';
				
				
				//sold products
				$orders = DB::table('orders')
					->whereBetween('date_purchased', [$dateFrom, $dateTo])
					->get();
			
				$totalSale = 0;
				foreach($orders as $orders_data){
					
					$orders_status = DB::table('orders_status_history')
						->where('orders_id', '=', $orders_data->orders_id)
						->orderby('date_added', 'DESC')->limit(1)->get();
						
					if($orders_status[0]->orders_status_id != 3){
						$totalSale++;
					}
				}
				
				//purchase products
				$products = DB::table('products')
					->select('products_quantity', DB::raw('SUM(products_quantity) as products_quantity'))
					->whereBetween('products_date_added', [$dateFrom, $dateTo])
					->get();
									
				$saleData[$i]['date'] = date('M Y',strtotime($dateFrom));
				$saleData[$i]['totalSale'] = $totalSale;
				
				if(empty($products[0]->products_quantity)){
					$producQuantity = 0;
				}else{
					$producQuantity = $products[0]->products_quantity;
				}
				
				$saleData[$i]['productQuantity'] = $producQuantity;
				
				$selectedMonth++;
				if($selectedMonth > 12){
					$selectedMonth = '1';
					$selectedYear++;
				}
				$i++;
			}
		
		} else if($years >= 1){
			
			//print $years.'sadsa';
			if($months>0){
				$years+=1;	
			}
			
			//print $years;
			
			$dateLimitFrom 	= date('d',  strtotime($dateFrom));
			$dateLimitTo 	= date('d',  strtotime($dateTo));
			
			$selectedMonthFrom 	= date('m',  strtotime($dateFrom));
			$selectedMonthTo 	= date('m',  strtotime($dateTo));
			
			$selectedYearFrom	= date('Y',  strtotime($dateFrom));
			$selectedYearTo	= date('Y',  strtotime($dateTo));
			//print $selectedYearFrom.' '.$selectedYearTo;
			
			$i = 0;
			//for current month		
			for($j = $selectedYearFrom; $j <= $selectedYearTo; $j++){
				
				if($j==$selectedYearTo){
					$selectedYearTo = $selectedYearTo;	
					$dateLimitTo = $dateLimitTo;
				}else{
					$selectedMonthTo = 12;	
					$dateLimitTo = 31;
				}
				
				if( $selectedYearFrom == $j ){
					$selectedMonthFrom = $selectedMonthFrom;
				}else{
					$selectedMonthFrom = 1;
				}
				
			//	print $j.'-'.$selectedMonthFrom.'-'.$dateLimitFrom.'<br>';
				//print $j.'-'.$selectedMonthTo.'-'.$dateLimitTo.'<br>';
				//$lastday  =  date('t',strtotime($dateLimitFrom.'-'.$selectedMonth.'-'.$selectedYear));
				
				
				$dateFrom   = date($j.'-'.$selectedMonthFrom.'-'.$dateLimitFrom, strtotime($dateFrom));
				$dateTo   	= date($j.'-'.$selectedMonthTo.'-'.$dateLimitTo, strtotime($dateTo));
			//	print $dateFrom.' '.$dateTo.'<br>';
				//print $dateFrom.'<br>';
				
				
				//sold products
				$orders = DB::table('orders')
					->whereBetween('date_purchased', [$dateFrom, $dateTo])
					->get();
			
				$totalSale = 0;
				foreach($orders as $orders_data){
					
					$orders_status = DB::table('orders_status_history')
						->where('orders_id', '=', $orders_data->orders_id)
						->orderby('date_added', 'DESC')->limit(1)->get();
						
					if($orders_status[0]->orders_status_id != 3){
						$totalSale++;
					}
				}
				
				//purchase products
				$products = DB::table('products')
					->select('products_quantity', DB::raw('SUM(products_quantity) as products_quantity'))
					->whereBetween('products_date_added', [$dateFrom, $dateTo])
					->get();
									
				$saleData[$i]['date'] = date('Y',strtotime($dateFrom));
				$saleData[$i]['totalSale'] = $totalSale;
				
				if(empty($products[0]->products_quantity)){
					$producQuantity = 0;
				}else{
					$producQuantity = $products[0]->products_quantity;
				}
				
				$saleData[$i]['productQuantity'] = $producQuantity;
				//$selectedYear++;
				//$selectedMonth++;
				$i++;
			}
			
		
		}
			//print_r($saleData);
		}
		
		 //$reportBase = str_replace('dateRange','', $reportBase);
		 
		// return $reportBase;
		 return $saleData;	
	}
	public function changeStatus(Request $request){
	    $type = $request->type;
	   // $value = 
	    switch ($type) {
            case "category":
                
                
                Category::where('id',$request->id)->update(['status'=>$request->value]);
               // $cat = Category::where('id',$request->id)->first();
                //print_r($cat);exit;
               // $car->status = '0';
               // $car->save();
                break;
            case "product":
                Product::where('id',$request->id)->update(['status'=>$request->value]);
                break;
          
          
        }
        
	}
}
