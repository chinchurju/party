<?php

namespace App\Http\Controllers\Admin\Couriers;

use App\Shop\Couriers\Repositories\CourierRepository;
use App\Shop\Couriers\Repositories\Interfaces\CourierRepositoryInterface;
use App\Shop\Couriers\Requests\CreateCourierRequest;
use App\Shop\Couriers\Requests\UpdateCourierRequest;
use App\Http\Controllers\Controller;
use DB;
class CourierController extends Controller
{
    /**
     * @var CourierRepositoryInterface
     */
    private $courierRepo;

    /**
     * CourierController constructor.
     * @param CourierRepositoryInterface $courierRepository
     */
    public function __construct(CourierRepositoryInterface $courierRepository)
    {
        $this->courierRepo = $courierRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.couriers.list', ['couriers' => $this->courierRepo->listCouriers('name', 'asc')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arr=DB::table('zones')->pluck('zone_name')->toArray();
        $data = array_combine(range(1, count($arr)), array_values($arr));

        return view('admin.couriers.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCourierRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCourierRequest $request)
    {
        $courior=$this->courierRepo->createCourier($request->all());
        foreach ($request->tags as $tag){
            DB::table('zone_courier_mapping')->insert([
                ['zone_id' => $tag, 'courier_id' => $courior->id]
            ]);
        }

        $request->session()->flash('message', 'Create successful');
        return redirect()->route('admin.couriers.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $zoneList=DB::table('zone_courier_mapping')
            ->leftJoin('zones','zones.zone_id','=','zone_courier_mapping.zone_id')
            ->where('zone_courier_mapping.courier_id',$id)->pluck('zone_courier_mapping.zone_id')->toArray();
        $arr=DB::table('zones')->pluck('zone_name')->toArray();
        $data = array_combine(range(1, count($arr)), array_values($arr));
        return view('admin.couriers.edit', ['courier' => $this->courierRepo->findCourierById($id)])->with('list',$zoneList)->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCourierRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCourierRequest $request, $id)
    {
        $courier = $this->courierRepo->findCourierById($id);

        $update = new CourierRepository($courier);
        $update->updateCourier($request->all());
        DB::table('zone_courier_mapping')->where('courier_id',  $courier->id)->delete();

        foreach ($request->tags as $tag){
            DB::table('zone_courier_mapping')->insert([
                ['zone_id' => $tag, 'courier_id' => $courier->id]
            ]);
        }
        $request->session()->flash('message', 'Update successful');
        return redirect()->route('admin.couriers.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $courier = $this->courierRepo->findCourierById($id);

        $courierRepo = new CourierRepository($courier);
        $courierRepo->delete();

        request()->session()->flash('message', 'Delete successful');
        return redirect()->route('admin.couriers.index');
    }
}
