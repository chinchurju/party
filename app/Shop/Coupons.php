<?php

namespace App\Shop;

use Illuminate\Database\Eloquent\Model;

class Coupons extends Model
{
    protected $fillable = [
        'userid', 'name','code','valid_form','valid_from','expires_date','status','amount','description'
    ];

    protected $table = 'coupon';

    public $timestamps = true;

    public function Orders(){
        return $this->hasOne('App\Shop\Order');
    }
}
