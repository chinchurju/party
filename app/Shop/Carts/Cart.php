<?php

namespace App\Shop\Carts;

use Illuminate\Database\Eloquent\Model;
use App\Shop\Products\Product;
class Cart extends Model
{
	
	protected $table = 'carts';
	protected $fillable = [
        'customer_id',
        'product_id',
        'product_attr_id',
        'qty',
		'price',
        'sub_total'
    ];
	/**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function products()
    {
		//$this->belongsTo('App\Post', 'foreign_key')
        return $this->belongsTo(Product::class,'product_id', 'id');
    }
	public function getCart()
    {
        return $this->where('customer_id',auth()->user()->id)->get();
    }
	public function addToCart($product,$qty,$option=null)
    {
		$cartItem = $this->where('customer_id', auth()->user()->id)->where('product_id', $product->id)->first();
		if($cartItem){
			return $this->updateQuantityInCart($cartItem->id, ($qty+$cartItem->qty));
		}else{	
			return $this->create(['customer_id'=>auth()->user()->id,'product_id'=>$product->id,'product_attr_id'=>$option,'qty'=>$qty,'price'=>$product->price,'sub_total'=>($qty*$product->price)] );
		}
    }
	public function getCartTotal($shippingCharge=0)
    {
        return ($this->where('customer_id',auth()->user()->id)->sum('sub_total'))+$shippingCharge;
    }
	public function updateQuantityInCart($id, $qty)
    {
		$cartItem = $this->where('id', $id)->firstOrFail();
		return $cartItem->update(['qty'=>$qty,'sub_total'=>($qty*$cartItem->price)]);
    }
    static function clear()
    {
        return static::where('customer_id',auth()->user()->id)->delete();
    }
	
}
