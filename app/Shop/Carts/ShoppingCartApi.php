<?php

namespace App\Shop\Carts;

use App\Shop\Carts\Cart
use Gloudemans\Shoppingcart\Facades\Cart;

class ShoppingCartApi extends Cart
{
    

    /**
     * Get the total price of the items in the cart.
     *
     * @param int $decimals
     * @param string $decimalPoint
     * @param string $thousandSeparator
     * @param float $shipping
     * @return string
     */
    public function total($decimals = null, $decimalPoint = null, $thousandSeparator = null, $shipping = 0.00)
    {
        $content = $this->getContent();

        $total = $content->reduce(function ($total, CartItem $cartItem) {
            return $total + ($cartItem->qty * $cartItem->priceTax);
        }, 0);

        $grandTotal = $total + $shipping;

        return number_format($grandTotal, $decimals, $decimalPoint, $thousandSeparator);
    }
	/**0
     * @param Product $product
     * @param int $int
     * @param array $options
     * @return CartItem
     */
    public function addToCart($product, int $int, $options = [])
    {
        return Cart::add($product->id,$product->name, $int,$product->price);
    }
}
