<?php

namespace App\Shop\Orders;

use App\Shop\Addresses\Address;
use App\Shop\Couriers\Courier;
use App\Shop\Customers\Customer;
use App\Shop\OrderStatuses\OrderStatus;
use App\Shop\Products\Product;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Http\Request;
use DB;
class OrderAttachment extends Model
{
    protected $table ="request_attachment";
	 protected $fillable = [
        'images',
        'type',
        'order_id'
    ];
    public function order()
    {
        return $this->belongsTo(Order::class,'order_id');
    }
}
