<?php

namespace App\Shop\Wishlist;

use Illuminate\Database\Eloquent\Model;
use DB;
class Wishlist extends Model
{
    public function AddToWishlist($data){
		$data['created_date'] = date("Y-m-d H:i:s");
		$data['customer_id'] = auth()->user()->id;
		return DB::table('wishlist')->insertGetId(
			$data
		);
	}
	public function getWishlist(){
		
		$id = 0;
		if(auth()->check()){
			$id = auth()->user()->id;
		}
		$list = array();		
		$res  =  DB::table('wishlist')->where('customer_id',$id)->get();
		if($res){
			foreach($res as $row){
				$list[] = $row->product_id;
			}
		}
		return $list;
		
	}
	public function getWishlistByCustomer(){
		return DB::table('wishlist')->where('customer_id',auth()->user()->id)->join('products', 'products.id', '=', 'wishlist.product_id')->get();
	}
	public function deleteWishlist($product_id){
		return DB::table('wishlist')->where('customer_id',auth()->user()->id)->where('product_id',$product_id)->delete();
	}
	
}
