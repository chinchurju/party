<?php

namespace App\Shop\Categories;

use App\Shop\Products\Product;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'name_ar',
        'slug',
        'description',
        'description_ar',
        'cover',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function countryProducts(array $request)
    {
        $country_id = '';
        if(isset($request['country_id']))
             $country_id = $request['country_id'];

        if(auth()->check()){

             $country_id = auth()->user()->country_id;
        }
              
        if($country_id)
            return $this->belongsToMany(Product::class)->join('country_product', 'country_product.product_id', '=', 'products.id')->where('country_product.country_id',$country_id)->get();
        else
            return $this->belongsToMany(Product::class)->get();
    }

    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(static::class, 'parent_id');
    }
}
