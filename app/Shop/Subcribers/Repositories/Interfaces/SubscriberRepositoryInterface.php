<?php

namespace App\Shop\Subscribers\Repositories\Interfaces;

use Jsdecena\Baserepo\BaseRepositoryInterface;
use App\Shop\Subscribers\Subscriber;
use Illuminate\Support\Collection;

interface SubscriberRepositoryInterface extends BaseRepositoryInterface
{
    public function updateSubscriber(array $params) : Country;

    public function listSubscribers(string $order = 'id', string $sort = 'desc') : Collection;

    public function createSubscriber(array $params) : Country;

    public function findSubscriberById(int $id) : Country;

    public function findProvinces();

    public function listStates() : Collection;
}
