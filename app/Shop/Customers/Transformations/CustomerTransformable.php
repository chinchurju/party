<?php

namespace App\Shop\Customers\Transformations;

use App\Shop\Customers\Customer;

trait CustomerTransformable
{
    protected function transformCustomer(Customer $customer)
    {
        $prop = new Customer;
        $prop->id = (int) $customer->id;
        $prop->name = $customer->name;
        $prop->email = $customer->email;
        $prop->fb_id = $customer->fb_id;
        $prop->gmail_id = $customer->gmail_id;
        $prop->phone = $customer->phone;
        $prop->status = (int) $customer->status;

        return $prop;
    }
}
