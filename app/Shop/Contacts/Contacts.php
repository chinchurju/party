<?php

namespace App\Shop\Contacts;

use Illuminate\Database\Eloquent\Model;
use DB;
class Contacts extends Model
{
    public function createContacts($data){
		$data['created_date'] = date("Y-m-d H:i:s");
		$dd =  DB::table('contacts')->insertGetId(
			$data
		);
	}
	public function getContacts($data){
		return DB::table('contacts')->where('email_address','phone',$data)->get();
	}
}
