<?php

namespace App\Shop\Products\Transformations;

use App\Shop\Products\Product;
use Illuminate\Support\Facades\Storage;

trait ProductTransformable
{
    /**
     * Transform the product
     *
     * @param Product $product
     * @return Product
     */
   protected function transformProduct(Product $product,$flag = false)
    {
        $prod = new Product;
        $prod->id = (int) $product->id;
        $prod->name = $product->name;
        $prod->name_ar = $product->name_ar;
        $prod->type = $product->type;
        $prod->sku = $product->sku;
        $prod->slug = $product->slug;
        $prod->short_description = $product->short_description;
        $prod->short_description_ar = $product->short_description_ar;
        $prod->description = $product->description;
        $prod->description_ar = $product->description_ar;
        if($flag)
            $prod->cover = env("APP_URL")."/storage/products/".rawurlencode($product->cover);//asset("storage/$product->cover");
        else
            $prod->cover = $product->cover;//asset("storage/$product->cover");
        $prod->duration = $product->duration;
       // $prod->cover = $product->cover;//asset("storage/$product->cover");
        $prod->quantity = $product->quantity;
        $prod->price = $product->price;
        $prod->status = $product->status;
        $prod->weight = (float) $product->weight;
        $prod->mass_unit = $product->mass_unit;
        $prod->sale_price = $product->sale_price;
        $prod->brand_id = (int) $product->brand_id;

        return $prod;
    }
}
