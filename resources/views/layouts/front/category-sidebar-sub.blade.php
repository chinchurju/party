<div>
	<input type="checkbox" class="read-more-state" id="post-{{ $key1+1 }}" />
	<ul class="filt read-more-wrap">
		<?php $i = 0; ?>
		@foreach($subs as $key=>$sub)
			@if($i == 4)
				<div class="read-more-target">
			@endif
				<li class="filt__item">
					<a href="{{ route('front.category.slug', $sub->slug) }}"><label class="label--checkbox">{{ $sub->name }}</label></a>
				</li>
		   <?php $i++; ?>	
		@endforeach
		@if(($i) >=5  )
			</div>
		@endif
	</ul>
	@if(($i) >=5  )
		<label for="post-{{ $key1+1 }}" class="read-more-trigger"></label>  
	@endif
</div>