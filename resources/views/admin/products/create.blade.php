@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <form action="{{ route('admin.products.store') }}" method="post" class="form" enctype="multipart/form-data">
                <div class="box-body">
                    {{ csrf_field() }}
                    <div class="col-md-8">
                        <h2>Service</h2>
                     <!--   <div class="form-group">
                            <label for="sku">SKU </label>
                            <input type="text" name="sku" id="sku" placeholder="xxxxx" class="form-control" value="{{ old('sku') }}">
                        </div>-->
                        <div class="form-group">
                            <label for="name">Name <span class="text-danger">*</span></label>
                            <input type="text" name="name" id="name" placeholder="Name" class="form-control" value="{{ old('name') }}">
                        </div>
                        
                         <div class="form-group">
                            <label for="name">Name Arabic</label>
                            <input type="text" name="name_ar" id="name_ar" placeholder="Name Arabic" class="form-control" value="{{ old('name_ar') }}">
                        </div>
                        
                        <div class="form-group">
                            <label for="type">Type</label>
                            <input type="text" class="form-control" name="type" id="type" placeholder="Type"  value="{{ old('type') }}">
                        </div>
                        
                        
                        <div class="form-group">
                            <label for="short_description">Short Description </label>
                            <input type="text" class="form-control" name="short_description" id="short_description" rows="5" placeholder="Short Description"  value="{{ old('short_description') }}">
                        </div>
                        
                        
                        <div class="form-group">
                            <label for="short_description">Short Description Arabic</label>
                            <input type="text" class="form-control" name="short_description_ar" id="short_description_ar" rows="5" placeholder="Short Description Arabic"  value="{{ old('short_description_ar') }}">
                        </div>

                        
                        <div class="form-group">
                            <label for="description">Description </label>
                            <textarea class="form-control" name="description" id="description" rows="5" placeholder="Description">{{ old('description') }}</textarea>
                        </div>
                        
                        <div class="form-group">
                            <label for="description">Description Arabic</label>
                            <textarea class="form-control" name="description_ar" id="description" rows="5" placeholder="Description">{{ old('description_ar') }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="duration">Duration </label>
                            <input type="text" class="form-control" name="duration" id="duration" rows="5" placeholder="Duration">
                        </div>
                       <div class="form-group">
                            <label for="cover">Image</label>
                            <input type="file" name="cover" id="cover" class="form-control">
                        </div>
                       <div class="form-group">
                            <label for="image">Gallery Images</label>
                            <input type="file" name="image[]" id="image" class="form-control" multiple>
                            <small class="text-warning">You can use ctr (cmd) to select multiple images</small>
                        </div>
                       <!-- <div class="form-group">
                            <label for="quantity">Quantity <span class="text-danger">*</span></label>
                            <input type="text" name="quantity" id="quantity" placeholder="Quantity" class="form-control" value="{{ old('quantity') }}">
                        </div>-->
					<!--	<div class="form-group">
                            <label for="payment">Payment </label>
								<input type="text" name="payment" id="payment" placeholder="Payment" class="form-control" value="{{ old('payment') }}">
                        </div>-->
					<!-- 	 @if(!$brands->isEmpty())
                        <div class="form-group">
                            <label for="brand_id">Payment </label>
                            <select name="brand_id" id="brand_id" class="form-control select2">
                                <option value=""></option>
                                @foreach($brands as $brand)
                                    <option selected="selected"  value="{{ $brand->id }}">{{ $brand->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif -->
                       <!--  <div class="form-group">
                        @include('admin.shared.country-select', ['countries' => $countries, 'selectedIds' => []])
                    </div> -->
                        
                        <div class="form-group">
                            <label for="price">Price <span class="text-danger">*</span></label>
                            <div class="input-group">
                                <span class="input-group-addon">QAR</span>
                                <input type="text" name="price" id="price" placeholder="Price" class="form-control" value="{{ old('price') }}">
                            </div>
                        </div>
                        
                         <!-- <div class="form-group">
                            <label for="price">Emergency Charge <span class="text-danger">*</span></label>
                            <div class="input-group">
                                <span class="input-group-addon">QAR</span>
                                <input type="text" name="sale_price" id="sale_price" placeholder="Price" class="form-control" value="{{ old('sale_price') }}">
                            </div>
                        </div>
                        
                         <div class="form-group">
                            <label for="sku">Meaage for Emergency Service<span class="text-danger">*</span></label>
                                <input type="text" name="sku" id="sku" placeholder="Message" class="form-control" value="{{ old('sku') }}">
                        </div> -->
                        
                      
                        
                       
                        @include('admin.shared.status-select', ['status' => 0])
                      <!--  @include('admin.shared.attribute-select', [compact('default_weight')])-->
						<!--<div class="form-group">
                            <label for="price">Is New ?</label>
                            <div class="input-group">
                                <input type="checkbox" name="new_product_flag" id="new_product_flag"   value="1">
                            </div>
                        </div>-->
                    </div>
                    <div class="col-md-4">
                        <h2>Categories</h2>
                        @include('admin.shared.categories', ['categories' => $categories, 'selectedIds' => []])
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.products.index') }}" class="btn btn-default">Back</a>
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection

@section('js')
<script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.min.js"></script>
   
   <script>
    $(function () { 
    $("textarea").summernote({height: "200px",});
     });
</script>
@endsection
