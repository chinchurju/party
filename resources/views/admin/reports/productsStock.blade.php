@extends('layouts.admin.app')

@section('content')

  <!-- Main content -->
  <section class="content"> 
    <!-- Info boxes --> 
    
    <!-- /.row -->
    <div class="row">
	<div class="col-md-12">
		   <h2>Products Stock</h2>
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="@if(!isset($_REQUEST['action'])) active  @endif"><a href="#low-in-stock" data-toggle="tab"> {{ trans('labels.LowInStock') }} </a></li>
              <li class="@if(isset($_REQUEST['action']) and $_REQUEST['action']=='outofstock') active  @endif"><a href="#out-of-stock" data-toggle="tab"> {{ trans('labels.OutofStock') }} </a></li>
            </ul>
            <div class="tab-content">
              <div class=" @if(!isset($_REQUEST['action'])) active  @endif tab-pane" id="low-in-stock">
               <div class="box-body">
                    <div class="row">
                      <div class="col-xs-12">
                        <table id="t1" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>{{ trans('labels.ID') }}</th>
                              <th>{{ trans('labels.Image') }}</th>
                              <th>{{ trans('labels.Products') }}</th>
                              <th>{{ trans('labels.Quantity') }}</th>
                              <th>{{ trans('labels.View') }}</th>
                            </tr>
                          </thead>
                          <tbody>
                         @if(count($result['lowQunatity']) > 0)
                            @foreach ($result['lowQunatity'] as  $key=>$lowQunatityProducts)
                                <tr>
                                    <td>{{ $lowQunatityProducts->id }}</td>
                                    <td><img src="{{asset('').'storage/products/'.$lowQunatityProducts->cover}}" alt="" width=" 100px" height="100px"></td>
                                    <td width="45%">
                                        <strong>{{ $lowQunatityProducts->name }} ( {{ $lowQunatityProducts->sku }} )</strong><br>
                                    </td>
                                    <td>
                                        {{ $lowQunatityProducts->quantity }}
                                    </td>
                                    <td>
                                        <a data-toggle="tooltip" data-placement="bottom" title="Edit" href="products/editProduct/{{ $lowQunatityProducts->id }}/edit" class="badge bg-light-blue"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                         @endif
                         </tbody>
                        </table>
                        <div class="col-xs-12 text-right">
                            {{$result['lowQunatity']->links()}}
                        </div>
                      </div>
                      
                    </div>
         		 </div>
              </div>
              <!-- /.tab-pane -->
              <div class="@if(isset($_REQUEST['action']) and $_REQUEST['action']=='outofstock') active  @endif tab-pane" id="out-of-stock">
                <!-- The timeline -->
                <div class="box-body">
                    <div class="row">
                      <div class="col-xs-12">
                        <table id="example1" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>{{ trans('labels.ID') }}</th>
                              <th>{{ trans('labels.Image') }}</th>
                              <th>{{ trans('labels.Products') }}</th>
                              <th>{{ trans('labels.Quantity') }}</th>
                              <th>{{ trans('labels.View') }}</th>
                            </tr>
                          </thead>
                          <tbody>
                          @if(count($result['outOfStock']) > 0)
                            @foreach ($result['outOfStock'] as  $key=>$outOfStockData)
                                
                                <tr>
                                    <td>{{ $outOfStockData->id }}</td>
                                    <td><img src="{{asset('').'storage/products/'.$lowQunatityProducts->cover}}" alt="" width=" 100px" height="100px"></td>
                                    <td width="45%">
                                        <strong>{{ $outOfStockData->name }} ( {{ $outOfStockData->sku }} )</strong><br>
                                    </td>
                                    <td>
                                        {{ $outOfStockData->quantity }}
                                    </td>

                                    <td>
                                        <a data-toggle="tooltip" data-placement="bottom" title="Edit" href="products/editProduct/{{ $lowQunatityProducts->id }}/edit" class="badge bg-light-blue"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                             @endforeach
                            @endif
                          </tbody>
                        </table>
                        <div class="col-xs-12 text-right">
                            {{$result['outOfStock']->links()}}
                        </div>
                           
                      </div>
                      
                    </div>
         		 </div>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
       </div>
    <!-- Main row --> 
    
    <!-- /.row --> 
  </section>
  <!-- /.content --> 
@endsection 