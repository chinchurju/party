@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">

    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if($orders)
            <div class="box">
                <div class="box-body">
                    <h2>Request</h2>
                    @include('layouts.date_search', ['route' => route('admin.sale.index'),'statuses'=>$statuses])
                    <table class="table table-striped table-bordered dataTable">
                        <thead>
                            <tr>
							
                                <th class="col-md-3 @if(request()->has('order_by')) @if(request()->input('order_by') == 'id') @if(request()->input('order') == 'asc') sorting_asc @else sorting_desc @endif @else sorting @endif @else sorting @endif" data-orderby="id">ID</th>
                                <th class="col-md-3 @if(request()->has('order_by')) @if(request()->input('order_by') == 'created_at') @if(request()->input('order') == 'asc') sorting_asc @else sorting_desc @endif @else sorting @endif @else sorting @endif" data-orderby="created_at">Date</th>
                                <td class="col-md-3">Customer</td>
                                <th class="col-md-2 @if(request()->has('order_by')) @if(request()->input('order_by') == 'order_status_id') @if(request()->input('order') == 'asc') sorting_asc @else sorting_desc @endif @else sorting @endif @else sorting @endif" data-orderby="order_status_id">Status</th>
                           	    <th class="col-md-2 @if(request()->has('order_by')) @if(request()->input('order_by') == 'total') @if(request()->input('order') == 'asc') sorting_asc @else sorting_desc @endif @else sorting @endif @else sorting @endif" data-orderby="total">Total</th>


                            </tr>
                        </thead>
                        <tbody>
						@if(count($orders)>0)
							@foreach ($orders as $order)
								<tr>
									<td>{{$order->reference}}</td>
									<td><a title="Show order" href="{{ route('admin.orders.show', $order->id) }}">{{ date('M d, Y h:i a', strtotime($order->created_at)) }}</a></td>
									<td>{{$order->customer->name}}</td>
									<td style="text-align:center;color: #ffffff; background-color: {{ $order->orderStatus->color }}">{{$order->orderStatus->name}}</td>
									<td>
										<span class="label @if($order->total != $order->total_paid) label-danger @else label-success @endif">{{ config('cart.currency') }} {{ $order->total }}</span>
									</td>
								</tr>
							@endforeach
						@else
							<tr>
								<td colspan="5" align="center">No records found</td>
								
							</tr>
						@endif
                        </tbody>
                    </table>
                </div>
               
            </div>
            <!-- /.box -->
        @endif

    </section>
    
    <!-- /.content -->
@endsection
@section('js')
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('.dataTable th').click(function(){
        var sort = 'asc';
        if($(this).hasClass('sorting_asc')){
            var sort = 'desc';
        }
        window.location="{{route('admin.sale.index')}}?order_by="+$(this).data('orderby')+"&order="+sort+'{!! $extraparam !!}';
    } );
} );   
</script>

@endsection