@extends('layouts.admin.app')
@section('content')
<!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Banners</h2>
            <div class="row">
              <div class="col-xs-12">
			     <div class="btn-group pull-right" >
                        <a class="btn btn-sm btn-primary" href="{{ route('admin.addBanner') }}"><i class="fa fa-plus"></i> Create Banner</a>
                 </div>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Title</th>
                      <th>Description</th>
                      <th>Image</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  @if(count($result['banners'])>0)
                    @foreach ($result['banners'] as $key=>$banners)
                        <tr>
                            <td>{{ $banners->banners_id }}</td>
                            <td>{{ $banners->banners_title }}</td>
							<td>{{ $banners->banners_desc }}</td>
                            <td><img src="{{asset('').'/'.$banners->banners_image}}" alt="" width=" 100px"></td>
                            <td><a data-toggle="tooltip" data-placement="bottom" title="Edit" href="editBanner/{{ $banners->banners_id }}" class="badge bg-light-blue"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                            
                            <a data-toggle="tooltip" data-placement="bottom" title="Delete" id="deleteBannerId" banners_id ="{{ $banners->banners_id }}" class="badge bg-red"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </tr>
                    @endforeach
                    @else
                       <tr>
                            <td colspan="5">NoRecordFound</td>
                       </tr>
                    @endif
                  </tbody>
                </table>
                <div class="col-xs-12 text-right">
                	{{$result['banners']->links()}}
                </div>
              </div>
            </div>
          </div>
     </section>  

    <!-- deleteBannerModal -->
	<div class="modal fade" id="deleteBannerModal" tabindex="-1" role="dialog" aria-labelledby="deleteBannerModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="deleteBannerModalLabel">Delete Banner</h4>
		  </div>
		  {!! Form::open(array('url' =>'admin/deleteBanner', 'name'=>'deleteBanner', 'id'=>'deleteBanner', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
				  {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
				  {!! Form::hidden('banners_id',  '', array('class'=>'form-control', 'id'=>'banners_id')) !!}
		  <div class="modal-body">						
			  <p>Are you sure you want to delete this banner?</p>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-primary" id="deleteBanner">Delete</button>
		  </div>
		  {!! Form::close() !!}
		</div>
	  </div>
	</div>
    	 
@endsection 