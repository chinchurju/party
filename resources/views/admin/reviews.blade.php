@extends('layouts.admin.app')
@section('content')
 <section class="content">
   
   
    <style>
        .cls-pending {
            border-left: 3px solid red !important;
        }
        .checked {
            color: orange;
        }
        .accept_review {
            color: green;
            cursor: pointer;
            font-size: 18px;
        }.reject_review {
             color: red;
             cursor: pointer;
             font-size: 18px;
         }
        
    </style>
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Add Banner</h2>
          <!-- /.box-header -->
        <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-push-6 col-sm-6">
                            <div class="dataTables_filter">
                                <form action="{{url('admin/reviews')}}" method="get">
                                    @if(@$search!='')
                                        <div class="input-group input-group-sm col-xs-1 pull-right">
                                            <a href="{{url('admin/reviews')}}" class="btn btn-primary btn-sm pull-right">ALL</a>
                                        </div>
                                    @endif
                                    <div class="input-group input-group-sm col-xs-6 pull-right">
                                            <span class="input-group-addon">
                                                Search
											</span>
                                        <input type="text" class="form-control" name="search" value="{{@$search}}" placeholder="Search By User Or Review">
                                        <span class="input-group-btn" style="z-index: 10;">
                                                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                            </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="custom-table">
                                <table class="table table-bordered table-striped table-responsive" style="min-width: 600px;">
                                    <thead>
                                    <tr>
                                        <th width="1%">#</th>
                                        <th width="150">Review On</th>
                                        <th width="150">Review By</th>
                                        <th width="120">Rating</th>
                                        <th>Review</th>
                                        <th class="text-center no-sort" width="100">Status</th>
                                        <th class="text-center no-sort" width="100">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($results->total()==0)
                                        <tr class="data">
                                            <td colspan="7" align="center">No Records Found</td>
                                        </tr>
                                    @else
                                        @php
                                            $i = $results->firstItem() - 1;
                                        @endphp
                                        @foreach($results as $key=>$row)
                                            <tr class="data" id="data-{{ $row->reviews_id }}">
                                                <td class="{{$row->reviews_status=='1'?'cls-pending':''}}">{{ ++$i }}</td>
                                                <td><a class="permlink" href="{{url('admin/products/'.$row->products_id)}}">{{ $row->name}}</a></td>
                                                <td><a class="permlink" href="{{url('admin/customers')}}"> {{ $row->customers_name}}</a></td>
                                                <td>
                                                    @for($j=1;$j<=5;$j++)
                                                        <span class="fa fa-star {{$j<=$row->reviews_rating?'checked':''}}"></span>
                                                    @endfor
                                                </td>
                                                <td>{{$row->reviews_text}}</td>
                                                <td class="text-center">
                                                    @if($row->reviews_status=='1')
                                                        <div class="switch @if($row->reviews_status == '1')on @endif"><div class="knob"></div></div>
                                                    @else
                                                        <div>
                                                            <span class="accept_review tool-tip" title="Accept Review"><i class="fa fa-check"></i></span>
                                                            <span class="reject_review tool-tip" title="Reject Review"><i class="fa fa-times"></i></span></div>
                                                    @endif
                                                </td>
                                                <td class="action text-center three">
                                                    <a href="javascript:;" class="delete tool-tip" title="Delete"><span class="fa fa-trash"></span></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @if($results->total()>0)
                        <div class="row">
                            <div class="col-sm-5">
                                <div>Showing <span>{{$results->firstItem()}} - {{$results->lastItem()}} </span> of <span>{{$results->total()}}</span> entries</div>
                            </div>
                            <div class="col-sm-7">
                                <div class="pull-right">
                                    {{$results->render()}}
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
	</div>
</div>	

</section>

    <!-- /.row -->

@endsection