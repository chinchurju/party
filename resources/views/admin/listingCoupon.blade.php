@extends('layouts.admin.app')
@section('content')
<!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Coupons</h2>
            <div class="row">
              <div class="col-xs-12">
			     <div class="btn-group pull-right" >
                        <a class="btn btn-sm btn-primary" href="{{ route('admin.addCoupon') }}"><i class="fa fa-plus"></i>Create New</a>
                 </div>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Coupon Name</th>
                      <th>Coupon Code</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  @if(count($result['coupon'])>0)
                    @foreach ($result['coupon'] as $key=>$coupon)
                        <tr>
                            <td>{{ $coupon->id }}</td>
                            <td>{{ $coupon->name }}</td>
							              <td>{{ $coupon->code }}</td>
                            <td><a data-toggle="tooltip" data-placement="bottom" title="Edit" href="editCoupon/{{ $coupon->id }}" class="badge bg-light-blue"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                            
                           <a data-toggle="tooltip" data-placement="bottom" title="Delete" id="deletecouponId" couponid ="{{ $coupon->id }}" class="badge bg-red"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </tr>
                    @endforeach
                    @else
                       <tr>
                            <td colspan="5">NoRecordFound</td>
                       </tr>
                    @endif
                  </tbody>
                </table>
                <div class="col-xs-12 text-right">
                	{{$result['coupon']->links()}}
                </div>
              </div>
            </div>
          </div>
     </section>  

   <div class="modal fade" id="deletecouponModal" tabindex="-1" role="dialog" aria-labelledby="deletecouponModalLabel">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="deletecouponModalLabel">Delete Coupon</h4>
      </div>
      {!! Form::open(array('url' =>'admin/deleteCoupon', 'name'=>'deleteCoupon', 'id'=>'deleteCoupon', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
          {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
          {!! Form::hidden('id',  '', array('class'=>'form-control', 'id'=>'id')) !!}
      <div class="modal-body">            
        <p>Are you sure you want to delete this?</p>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <button type="submit" class="btn btn-primary" id="deleteBanner">Delete</button>
      </div>
      {!! Form::close() !!}
    </div>
    </div>
  </div>
       
@endsection 
@section('js')
<script>
$(document).on('click', '#deletecouponId', function(){
var id = $(this).attr('couponid');
$('#id').val(id);
$("#deletecouponModal").modal('show');
});
</script> 
@endsection