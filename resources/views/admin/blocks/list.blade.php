@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if(!$result['blocks']->isEmpty())
            <div class="box">
                <div class="box-body">
                    <h2>Blocks</h2>
					 <div class="btn-group pull-right" >
                        <a class="btn btn-sm btn-primary" href="{{ route('admin.block.create') }}"><i class="fa fa-plus"></i> Create Banner</a>
					</div>
                    <table class="table">
                        <thead>
                            <tr>
                                <td>Name</td>
                                <td>Actions</td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($result['blocks'] as $block)
                            <tr>
                                <td>
                                    {!! $block->block_title !!}</a>
                                </td>
                                <td>
                                    <form action="{{ route('admin.block.destroy', $block->block_id) }}" method="post" class="form-horizontal">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="delete">
                                        <div class="btn-group">
                                            <a href="{{ route('admin.block.edit', $block->block_id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                            <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Delete</button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $result['blocks']->links() }}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            @else
            <p class="alert alert-warning">No block created yet. <a href="{{ route('admin.block.create') }}">Create one!</a></p>
        @endif
    </section>
    <!-- /.content -->
@endsection
