

@if($countries->count()>0)
    <div class="form-group">
        <label for="country_id">Countries </label>
        <select multiple="multiple" name="countries[]" id="country_id" class="form-control select2">
            <option value=""></option>
             @foreach ($countries as $country)
            <option value="{{$country->id}}" @if ($selectedIds)@if(in_array($country->id, $selectedIds))selected="selected" @endif @endif>{{$country->name}}</option>

             @endforeach
        </select>
    </div>
@endif


