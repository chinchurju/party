@if(!$products->isEmpty())
    <table class="table">
        <thead>
        <tr>
			 <th></th>
            <th>ID</th>
            <th>Name</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Status</th>
            <th>Actions</th>
		</tr>
        </thead>
        <tbody>
        @foreach ($products as $product)
            <tr>
				<td><input type="checkbox" name="rel_prd[]" value="{{ $product->id }}" @if(in_array($product->id,$relative_products)) checked="checked" @endif /></td>
                <td>{{ $product->id }}</td>
                <td>
                   {{ $product->name }}
                   
                </td>
                <td>{{ $product->quantity }}</td>
                <td>{{ config('cart.currency') }} {{ $product->price }}</td>
                <td>@include('layouts.status', ['status' => $product->status])</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif