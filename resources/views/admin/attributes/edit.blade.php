@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
	<style>
.checkbox label:after, 
.radio label:after {
    content: '';
    display: table;
    clear: both;
}

.checkbox .cr,
.radio .cr {
    position: relative;
    display: inline-block;
    border: 1px solid #a9a9a9;
    border-radius: .25em;
    width: 1.3em;
    height: 1.3em;
    float: left;
    margin-right: .5em;
}

.radio .cr {
    border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
    position: absolute;
    font-size: .8em;
    line-height: 0;
    top: 50%;
    left: 20%;
}

.radio .cr .cr-icon {
    margin-left: 0.04em;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
    display: none;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
    transform: scale(3) rotateZ(-20deg);
    opacity: 0;
    transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
    transform: scale(1) rotateZ(0deg);
    opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
    opacity: .5;
}
</style>
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <form action="{{ route('admin.attributes.update', $attribute->id) }}" method="post" class="form">
                <div class="box-body">
                    <div class="row">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="put">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">Attribute name <span class="text-danger">*</span></label>
                                <input type="text" name="name" id="name" placeholder="Attribute name" class="form-control" value="{!! $attribute->name  !!}">
                            </div>
                        </div>
						<div class="col-md-12">
							<div class="checkbox">
								<label style="font-size: 1em;padding-left:0px"">
									<input type="checkbox" name="is_configurable" id="is_configurable" value="1" @if($attribute->is_configurable==1) checked @endif >
									<span class="cr"><i class="cr-icon fa fa-check"></i></span>
									Is Configurable
								</label>
							</div>
                        </div>
						<div class="col-md-12">
                           	<div class="checkbox">
								<label style="font-size: 1em;padding-left:0px" >
									<input type="checkbox" name="is_searchable" id="is_searchable" value="1" @if($attribute->is_searchable==1) checked @endif>
									<span class="cr"><i class="cr-icon fa fa-check"></i></span>
									Is Searchable
								</label>
							</div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.attributes.index') }}" class="btn btn-default">Back</a>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection
