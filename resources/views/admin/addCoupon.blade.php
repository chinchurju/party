@extends('layouts.admin.app')
@section('content')
 <section class="content">
   
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Add Coupon</h2>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-xs-12">
              		<div class="box box-info">
                        <br>                       
                        @if (count($errors) > 0)
                              @if($errors->any())
                                <div class="alert alert-success alert-dismissible" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  {{$errors->first()}}
                                </div>
                              @endif
                          @endif
                        
                        <!-- form start -->                        
                         <div class="box-body">
                         
                            {!! Form::open(array('url' =>'admin/addNewCoupon', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">Coupon Name</label>
                                  <div class="col-sm-10 col-md-4">
                                    {!! Form::text('name', '', array('class'=>'form-control','id'=>'name')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">Coupon Name</span>
                                  </div>
                                </div>
								                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">Coupon Code</label>
                                  <div class="col-sm-10 col-md-4">
                                    {!! Form::text('code', '', array('class'=>'form-control ','id'=>'code')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">Coupon Code</span>
                                  </div>
                                </div>

                                 <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">Amount</label>
                                  <div class="col-sm-10 col-md-4">
                                    {!! Form::text('amount', '', array('class'=>'form-control ','id'=>'amount')) !!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">Coupon Amount</span>
                                  </div>
                                </div>



                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">User</label>
                                  <div class="col-sm-10 col-md-4">
                                    <select class="form-control" name="userid">
                                      <option value="">Select </option>
                                      @if(count($result['customers'])>0)
                                      @foreach($result['customers'] as $customer)
                                      <option value="{{$customer->id}}">{{$customer->name}}</option>
                                        @endforeach 
                                        @endif
                                    </select>
                                  </div>
                                </div>


                                 <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">Categories</label>
                                  <div class="col-sm-10 col-md-4">
                                    <select class="form-control" name="catid">
                                      <option value="">Select </option>
                                      @if(count($result['categories'])>0)
                                      @foreach($result['categories'] as $category)
                                      <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach 
                                        @endif
                                    </select>
                                  </div>
                                </div>

                           
                              <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">Valid From</label>
                                  <div class="col-sm-10 col-md-4">
                                    <input readonly class="form-control datepicker field-validate" type="text" name="valid_from" value="">
                                   
                                  </div>
                                </div>

                                
                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.ExpiryDate') }}</label>
                                  <div class="col-sm-10 col-md-4">
                                    <input readonly class="form-control datepicker field-validate" type="text" name="expires_date" value="">
                                   
                                  </div>
                                </div>
                                
                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Status') }}</label>
                                  <div class="col-sm-10 col-md-4">
                                      <select class="form-control" name="status">
                                          <option value="1">{{ trans('labels.Active') }}</option>
                                          <option value="0">{{ trans('labels.InActive') }}</option>
                                      </select>
                                     
                                  </div>
                                </div>
                                
                              <!-- /.box-body -->
                              <div class="box-footer text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{ URL::to('admin/listingCoupon')}}" type="button" class="btn btn-default">{{ trans('labels.back') }}</a>
                              </div>
                              <!-- /.box-footer -->
                            {!! Form::close() !!}
                        </div>
                  </div>
              </div>
            </div>
            
          </div>
          <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
   
  </section>
  <!-- /.content --> 
@endsection 