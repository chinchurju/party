@extends('layouts.admin.app')
@section('content')
 <section class="content">
   
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Edit Coupon</h2>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-xs-12">
              		<div class="box box-info">
                    <br>                       
                        @if (count($errors) > 0)
                              @if($errors->any())
                                <div class="alert alert-success alert-dismissible" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  {{$errors->first()}}
                                </div>
                              @endif
                          @endif
                        <!--<div class="box-header with-border">
                          <h3 class="box-title">Edit category</h3>
                        </div>-->
                        <!-- /.box-header -->
                        <!-- form start -->                        
                         <div class="box-body">
                         
                            {!! Form::open(array('url' =>'admin/updateCoupon', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
                              
                                {!! Form::hidden('id',  $result['coupon'][0]->id , array('class'=>'form-control', 'id'=>'id')) !!}
                                
                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">Coupon Nmae </label>
                                  <div class="col-sm-10 col-md-4">
                                    {!! Form::text('name', $result['coupon'][0]->name, array('class'=>'form-control','id'=>'name')) !!}
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">Coupon Code</label>
                                  <div class="col-sm-10 col-md-4">
                                    {!! Form::text('code', $result['coupon'][0]->code, array('class'=>'form-control ','id'=>'code')) !!}
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">Amount</label>
                                  <div class="col-sm-10 col-md-4">
                                    {!! Form::text('amount', $result['coupon'][0]->amount, array('class'=>'form-control ','id'=>'amount')) !!}
                                  </div>
                                </div>


                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">User</label>
                                  <div class="col-sm-10 col-md-4">
                                    <select class="form-control" name="userid">
                                      @if(count($result['customers'])>0)
                                        @foreach($result['customers'] as $customer)
                                          <option value="{{$customer->id}}" @if( $customer->id==$result['coupon'][0]->userid) selected @endif>{{$customer->id}}</option>
                                        @endforeach 
                                      @endif
                                    </select>
                                  </div>
                                </div>

                                 <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">Categories</label>
                                  <div class="col-sm-10 col-md-4">
                                    <select class="form-control" name="catid">
                                      @if(count($result['categories'])>0)
                                        @foreach($result['categories'] as $category)
                                          <option value="{{$category->id}}" @if( $category->id==$result['coupon'][0]->catid) selected @endif>{{$category->id}}</option>
                                        @endforeach 
                                      @endif
                                    </select>
                                  </div>
                                </div>


                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">Valid From</label>
                                  <div class="col-sm-10 col-md-4">
                                  
                                 
                                  
                                   @if(!empty($result['coupon'][0]->valid_from))
                                    {!! Form::text('valid_from', date('d/m/Y', strtotime($result['coupon'][0]->valid_from)), array('class'=>'form-control datepicker', 'id'=>'valid_from')) !!}
                                   @else
                                    {!! Form::text('valid_from', '', array('class'=>'form-control datepicker', 'id'=>'valid_from')) !!}
                                    
                                   @endif
                                  
                                  </div>
                                </div>

            
                                
                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.ExpiryDate') }}</label>
                                  <div class="col-sm-10 col-md-4">
                                  
                                 
                                  
                                   @if(!empty($result['coupon'][0]->expires_date))
                                    {!! Form::text('expires_date', date('d/m/Y', strtotime($result['coupon'][0]->expires_date)), array('class'=>'form-control datepicker', 'id'=>'expires_date')) !!}
                                   @else
                                    {!! Form::text('expires_date', '', array('class'=>'form-control datepicker', 'id'=>'expires_date')) !!}
                                    
                                   @endif
                                  
                                  </div>
                                </div>
                                
                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Status') }}</label>
                                  <div class="col-sm-10 col-md-4">
                                      <select class="form-control" name="status">
                                          <option value="1" @if($result['coupon'][0]->status==1) selected @endif>{{ trans('labels.Active') }}</option>
                                          <option value="0" @if($result['coupon'][0]->status==0) selected @endif>{{ trans('labels.Inactive') }}</option>
                                      </select>
                                      
                                  </div>
                                </div>
                                
                                
                              <!-- /.box-body -->
                              <div class="box-footer text-center">
                                <button type="submit" class="btn btn-primary">{{ trans('labels.Update') }}</button>
                                <a href="{{ URL::to('admin/listingCoupon')}}" type="button" class="btn btn-default">{{ trans('labels.back') }}</a>
                              </div>
                              <!-- /.box-footer -->
                            {!! Form::close() !!}
                        </div>
                  </div>
              </div>
            </div>
            
          </div>
         <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
   
  </section>
  <!-- /.content --> 
@endsection 