<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="background-color: #ebebeb;">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
</head>
<body style="font-family: Verdana, Arial; font-weight: normal; margin: 0; padding: 0; text-align: left; color: #333333; background-color: #ebebeb; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; background: #ebebeb; font-size: 12px;">
<style type="text/css">
@import url(http://fonts.googleapis.com/css?family=Raleway:400,500,700);
@media screen {
  .email-heading h1,
  .store-info h4,
  th.cell-name,
  a.product-name,
  p.product-name,
  .address-details h6,
  .method-info h6,
  h5.closing-text,
  .action-button,
  .action-button a,
  .action-button span,
  .action-content h1 {
    font-family: 'Raleway', Verdana, Arial !important;
    font-weight: normal;
  }
}
@media screen and (max-width: 600px) {
  body {
    width: 94% !important;
    padding: 0 3% !important;
    display: block !important;
  }

  .container-table {
    width: 100% !important;
    max-width: 600px;
    min-width: 300px;
  }

  td.store-info h4 {
    margin-top: 8px !important;
    margin-bottom: 0px !important;
  }

  td.store-info p {
    margin: 5px 0 !important;
  }

  .wrapper {
    width: 100% !important;
    display: block;
    padding: 5px 0 !important;
  }

  .cell-name,
  .cell-content {
    padding: 8px !important;
  }
}
@media screen and (max-width: 450px) {
  .email-heading,
  .store-info {
    float: left;
    width: 98% !important;
    display: block;
    text-align: center;
    padding: 10px 1% !important;
    border-right: 0px !important;
  }

  .address-details, .method-info {
    width: 85%;
    display: block;
  }

  .store-info {
    border-top: 1px dashed #c3ced4;
  }

  .method-info {
    margin-bottom: 15px !important;
  }
}
/* Remove link color on iOS */
.no-link a {
  color: #333333 !important;
  cursor: default !important;
  text-decoration: none !important;
}

.method-info h6,
.address-details h6,
.closing-text {
  color: #3696c2 !important;
}

td.order-details h3,
td.store-info h4 {
  color: #333333 !important;
}

.method-info p,
.method-info dl {
  margin: 5px 0 !important;
  font-size: 12px !important;
}

td.align-center {
  text-align: center !important;
}

td.align-right {
  text-align: right !important;
}

/* Newsletter styles */
td.expander {
  padding: 0 !important;
}

table.button td,
table.social-button td {
  width: 92% !important;
}

table.facebook:hover td {
  background: #2d4473 !important;
}

table.twitter:hover td {
  background: #0087bb !important;
}

table.google-plus:hover td {
  background: #CC0000 !important;
}

/* ============================================ *
 * Product Grid
 * ============================================ */
@media screen and (max-width: 600px) {
  .products-grid tr td {
    width: 50% !important;
    display: block !important;
    float: left !important;
  }
}
.product-name a:hover {
  color: #3399cc !important;
  text-decoration: none !important;
}

td.logo{
  text-align:center;
}
td.logo a{
   display: inline-block !important;
float: none !important;
}
td.logo a img{width:122px !important; height:99px !important}
</style>

<!-- Begin wrapper table -->
<table width="100%" cellpadding="0" cellspacing="0" border="0" id="background-table" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse; padding: 0; margin: 0 auto; background-color: #ebebeb; font-size: 12px;">
    <tr>
        <td valign="top" class="container-td" align="center" style="font-family: Verdana, Arial; font-weight: normal; border-collapse: collapse; vertical-align: top; padding: 0; margin: 0; width: 100%;">
            <table cellpadding="0" cellspacing="0" border="0" align="center" class="container-table" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse; padding: 0; margin: 0 auto; width: 600px;">
                <tr>
                    <td style="font-family: Verdana, Arial; font-weight: normal; border-collapse: collapse; vertical-align: top; padding: 0; margin: 0;">
                        <table cellpadding="0" cellspacing="0" border="0" class="logo-container" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse; padding: 0; margin: 0; width: 100%;">
                            <tr>
                                <td class="logo" style="font-family: Verdana, Arial; font-weight: normal; border-collapse: collapse; vertical-align: top; padding: 15px 0px 10px 5px; margin: 0; text-align:center;">
                                  <a href="{{ route('home') }}" style="color: #3696c2;  display: block; text-align:center;">
                                        <img  src="{!! asset('resources/assets/front/') !!}/images/email_logo.png" alt="SERVICIO" border="0" style="-ms-interpolation-mode: bicubic; outline: none; text-decoration: none;">
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" class="top-content" style="font-family: Verdana, Arial; font-weight: normal; border-collapse: collapse; vertical-align: top; padding: 5px; margin: 0; border: 1px solid #ebebeb; background: #FFF;">
                    <!-- Begin Content -->


<table cellpadding="0" cellspacing="0" border="0" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse; padding: 0; margin: 0; width: 100%;">
    <tr>
        <td style="font-family: Verdana, Arial; font-weight: normal; border-collapse: collapse; vertical-align: top; padding: 0; margin: 0;">
            <table cellpadding="0" cellspacing="0" border="0" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse; padding: 0; margin: 0; width:100%;">
                <tr>
                    <td class="email-heading" style="font-family: Verdana, Arial; font-weight: normal; border-collapse: collapse; vertical-align: top; padding: 0 1%; margin: 0; background: #e1f0f8; border-right: 1px dashed #c3ced4; text-align: center; width: 58%;">
            <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
              <tbody><tr>
                <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">
                  <h1 style="text-align: left;">Request {{$order->reference}}</h1>
                </td>
              </tr>
              </tbody>
            </table>
            
                    </td>
                    <td class="store-info" style="font-family: Verdana, Arial; font-weight: normal; border-collapse: collapse; vertical-align: top; padding: 2%; margin: 0; background: #e1f0f8; width: 40%;">
                        <h4 style="font-family: Verdana, Arial; font-weight: bold; margin-bottom: 5px; font-size: 12px; margin-top: 13px;">Request Questions?</h4>
                        <p style="font-family: Verdana, Arial; font-weight: normal; font-size: 11px; line-height: 17px; margin: 1em 0;">
                            
                            <b>Email:</b> <a href="mailto:info@partyapp.qa" style="color: #3696c2; text-decoration: underline;">info@partyapp.qa</a>
                            
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    
    <tr class="order-information">
        <td style="font-family: Verdana, Arial; font-weight: normal; border-collapse: collapse; vertical-align: top; padding: 0; margin: 0;">
            
      <table width="100%" cellspacing="0" cellpadding="0" style="margin:0 auto;max-width:600px;background:#ffffff">
              <tbody>
                  <tr>
                    <td align="left" valign="top" class="m_-2941836887641383744m_-7336771439130682003container" style="color:#212121;display:block;margin:0 auto;clear:both;padding:30px 0 24px 0">
                      <table width="100%" cellspacing="0" cellpadding="0">
                        <tbody>
                            @if(!$items->isEmpty())
                            @foreach($items as $item) 
                          
                              <tr>
                                <td width="120" valign="top" align="left"> 
                                <a style="color:#ffffff;text-decoration:none;font-size:13px;display:block;width:100px">
                                  <img border="0" src="{{ asset("storage/products/$item->cover") }}" alt="{{ $item->name }}" style="border:none;width:100%; max-width:200px;max-height:200px;" class="m_-2941836887641383744CToWUd CToWUd">
                                </a> 
                                </td>
                                <td width="8"></td>
                                <td valign="top" align="left"> 
                                <p style="margin-bottom:13px">
                                  <a style="font-family:Roboto,sans-serif;font-size:14px;font-weight:normal;font-style:normal;line-height:1.25;color:#2175ff;text-decoration:none">
                                  <strong style="font-size:11px">{{ $item->name }}</strong>
                                  </a>
                                </p>
                               </td>
                              </tr>
                            @endforeach
                          @endif
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            
            <table cellpadding="0" cellspacing="0" border="0" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse; padding: 0; margin: 0; width: 100%;">
                <tr>
                    <td class="address-details" style="font-family: Verdana, Arial; font-weight: normal; border-collapse: collapse; vertical-align: top; padding: 10px 15px 0; margin: 0; padding-top: 10px; text-align: left;">
                          <a href= "{{$location_url}}" ><img src = "{{$img_location}}" /></a>
                    </td>
                    
                </tr>
                
            </table>
           
        </td>
    </tr>
</table>

                    <!-- End Content -->
                    </td>
                </tr>
            </table>
            <h5 class="closing-text" style="font-family: Verdana, Arial; font-weight: normal; text-align: center; font-size: 22px; line-height: 32px; margin-bottom: 75px; margin-top: 30px;">Thank you, PARTYAPP!</h5>
        </td>
    </tr>
</table>
<!-- End wrapper table -->
</body>
</html>
