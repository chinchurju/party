@extends('layouts.front.app')

@section('content')
    <!-- Main content -->

<div class="abt-tp"><h2>MY Account</h2></div>
	
	<div class="account">
	  <div class="container">
		<div class="box-body">
			@include('layouts.errors-and-messages')
		</div>
		<div class="col-md-12 no-padding">
				 <div class="tabs">
	  <div class="tab"    >
		<div class="tab-toggle @if(request()->input('tab') == 'profile') profile @endif " ><div class="tab-b"></div> Acount Details</div>
	  </div>
	  <div class="content " >
	  <h3>Acount Details</h3>
		
	  <div class="ad cl">
		<form method="post" action="{{ route('accounts.update') }}" >
			{{ csrf_field() }}
			<input type="hidden" name="type" value="details" />
			<div class="form-group clearfix">
			  <div class="row">
				<div class="col-md-6"><label>First Name</label><input type="text" class="form-control" name="name" value="{{$customer->name}}"></div>
				<div class="col-md-6"><label>Last Name</label><input type="text" class="form-control" value="{{$customer->lname}}"  name="lname"></div>
			  </div>
			</div>
			<div class="form-group clearfix">
			  <div class="row">
				<div class="col-md-6"><label>Email</label><input type="text" class="form-control" value="{{$customer->email}}" name="email"></div>
			  </div>
			</div>
			<div class="form-group clearfix">
			  <div class="row">
				<div class="col-md-12"><button class="log">Save Changes</button></div>
			  </div>
			</div>
		</form>	
	  </div>
	  
	  <div class="ad">
	  <h3>CHANGE PASSWORD</h3>
		<form method="post" action="{{ route('accounts.update') }}" >
			{{ csrf_field() }}
			<input type="hidden" name="type" value="password" />
			<div class="form-group clearfix">
			  <div class="row">
				<div class="col-md-6"><label>Current Password</label><input type="password" class="form-control" name="current-password" ></div>
				<div class="col-md-6"><label>New Password</label><input type="password" class="form-control" name="password" ></div>
			  </div>
			</div>
			<div class="form-group clearfix">
			  <div class="row">
				<div class="col-md-12"><label>Confirm Password</label><input type="password" class="form-control" name="c-password" ></div>
			  </div>
			</div>
			<div class="form-group clearfix">
			  <div class="row">
				<div class="col-md-12"><button class="log">Save Changes</button></div>
			  </div>
			</div>
		</form>
	  </div>


	  </div>
	  <div class="tab">
		<div class="tab-toggle toggle1"><div class="tab-og"></div>My Wishlist</div>
	  </div>
	  <div class="content">
		@if(!$wishlist->isEmpty())
			<div class="table-responsive">
				<div class="table-responsive">
					<table class="table table-condensed ">
					  <thead>
						<tr>
						  <th align="left">Products</th>
						  <th align="center">Price</th>
						</tr>
					  </thead>
					  <tbody>
						@foreach ($wishlist as $product)
						<tr>
						  <th align="left" scope="row" width="45%"> <div class="crt-img clearfix"><img src="{{ asset("storage/products/thumb270x300_$product->cover") }}" class="img-responsive"></div>
							<div class="crt-sec">
							  <h4>{{ $product->name }}</h4>
						  </th>
						  <td width="13%" align="center"><h5>{{ config('cart.currency') }} {{ $product->price }}</h5></td>
						  <td width="13%" align="center"><h5>
						  
						  <a href="javascript:void(0)" class="remove_wishlist" data-id="wisthlist_{{ $product->id }}" >REMOVE</a> 
							<form action="{{ route('accounts.wishlist') }}" class="form-inline" method="post" id="wisthlist_{{ $product->id }}" style="display:none">
								 {{ csrf_field() }}
								<input type="hidden" name="product_id" value="{{ $product->id }}">
								<input type="hidden" name="action" value="delete">
							</form>
						  </h5></td>
						  
						</tr>
						@endforeach
					  </tbody>
					</table>
					<div class="ccs clearfix"> <a href="{{ route('product.shop')}}">Continue With Shoping</a> <a href="{{ route('checkout.index')}}">Check Out</a></div>
				</div>
			</div>
		@else
			<p>No records found</p>	
		@endif
		


	  </div>
	  <div class="tab">
		<div class="tab-toggle toggle1 @if(request()->input('tab') == 'orders') orders @endif "><div class="tab-og"></div>My Orders</div>
	  </div>
	  <div class="content">
		<div class="odr">
		
			@if(!$orders->isEmpty())
				@foreach ($orders as $order)
					
					<article class=" accord-single1">
					  <div class="accord__head">
						<div class="row">
							<div class="col-md-3"><div class="ord-id">#{{$order['reference']}}</div></div>
							<div class="col-md-4"><div class="ord-dte"><span>Ordered On</span> {{ date('M d, Y h:i a', strtotime($order['created_at'])) }}</div></div>
							<div class="col-md-2"><div class="ord-price">{{ config('cart.currency_symbol') }} {{$order['total']}}</div></div>
							<div class="col-md-3 text-center"><div class="ord-stus" style="color: #ffffff; background-color: {{ $order['status']->color }}">{{ $order['status']->name }}</div></div>
						</div>	
					 </div>
					 
					  <div class="accord__body" style="display: block;">
						
						<div class="row">
							<div class="col-md-5">
								<h5>Delivery Address</h5>
								<address>
									<strong>{{$order['address']->alias}}</strong><br />
									{{$order['address']->address_1}} <br />{{$order['address']->address_2}}<br>
								</address>
								@if($order['status']->name !="Completed" && $order['status']->name !="Cancelled")  
									<form method="post" action="{{ route('accounts.cancelorder',$order['id']) }}" id="ord_{{$order['id']}}">
									{{ csrf_field() }}
									<input type="hidden" name="order_id" value="{{$order['id']}}" />
									</form>
									<div class="col-md-4 no-padding" ><a href="javascript:void(0)" data-id="ord_{{$order['id']}}" class="cancel_ord"><div class="ord-stus" style="color: #ffffff; background-color: #292929">Cancel</div></a></div>
								@endif
								<div class="col-md-4 no-padding" ><a href="{{ route('accounts.invoice', $order['id'])}}"><div class="ord-stus" style="color: #ffffff; background-color: #292929">Invoice</div></a></div>
							</div>
							<div class="col-md-7">
								@foreach ($items[$order['id']] as $product)
								<div class="order-pdct clearfix">
										<div class="odr-img"><img src="{{ asset("storage/products/thumb270x300_$product->cover") }}" class="img-responsive"></div>
										<h5>{{$product->product_name}}<br> <span>Qty: {{$product->qty}}</span> </h5>
										<h6>QAR {{$product->product_price}}</h6>
								</div>
								@endforeach
								
							</div>
							
						</div>
						
					  </div>
					</article>
				@endforeach
            @else
				<p class="alert alert-warning">No orders yet. <a href="{{ route('home') }}">Shop now!</a></p>
			@endif
			
			
		</div>
		
	  </div>
	  <div class="tab">
		<div class="tab-toggle toggle1 @if(request()->input('tab') == 'address') address @endif"><div class="tab-og"></div>Address Book</div>
	  </div>
	  <div class="content">
	   <h3>Address Book</h3>
	   <div class="ana"><a href="{{ route('customer.address.create', auth()->user()->id) }}">Add a New Address</a></div>
	    @if(!$addresses->isEmpty())
			@foreach($addresses as $address)
			   <div class="ab">
				  <h4>{{$address->alias}} </h4>
				  <p>{{$address->address_1}}<br>{{$address->address_2}},  {{$address->city}} {{$address->country->name}} </p>
				  <p>Mobile: {{$address->phone}}</p>
				  <div class="rem">
					<form id="frm_{{ $address->id }}" method="post" action="{{ route('customer.address.destroy', [auth()->user()->id, $address->id]) }}" class="form-horizontal" style="display:none;">
						<div class="btn-group">
							<input type="hidden" name="_method" value="delete">
							{{ csrf_field() }}
						
						</div>
					</form>
					<ul>
					  <li><a href="javascript:void(0);" data-id="frm_{{ $address->id }}" class="del_addrs"> <i class="fa fa-close"></i> Remove</a></li>
					  <li><a href="{{ route('customer.address.edit', [auth()->user()->id, $address->id]) }}"> <i class="fa fa-edit"></i> Edit</a></li>
					</ul>
				  </div>
			   </div>
		    @endforeach
		   
		@endif
	  </div>
	  
	  
	  
	</div>
	</div>
	  </div>
	</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script> 

<script>
$(document).ready(function(){
	

	$('.cancel_ord').click(function(e){
		
		e.preventDefault();
		var id = $(this).data('id');
		bootbox.confirm({
			message: "Are you sure, Do you want cancel this order?",
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'No',
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				
				if(result){
					$('#'+id).submit();
				}
			}
		});
		
	
	});
	$('.del_addrs').click(function(event){
		event.preventDefault();
		var id = $(this).data('id');
		bootbox.confirm({
			message: "Are you sure, Do you want delete this address?",
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'No',
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				
				if(result){
					$('#'+id).submit();
				}
			}
		});
	});
	$('.remove_wishlist').click(function(event){
		event.preventDefault();
		var ele = $(this);
		bootbox.confirm({
			message: "Are you sure, Do you want delete this address?",
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'No',
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				
				if(result){
					
					var id =ele.data('id');
					var frm = $('#'+id);
					var post_url = frm.attr("action"); //get form action url
					var request_method = frm.attr("method"); //get form GET/POST method
					var form_data = frm.serialize(); //Encode form elements for submission
					$.ajax({
						url : post_url,
						type: request_method,
						data : form_data
					}).done(function(response){ //
						ele.parent().parent().parent().fadeOut(300, function(){ ele.parent().parent().parent().remove(); });
					})
				}
			}
		});
	});
});
wrapper = $('.tabs');
tabs = wrapper.find('.tab');
tabToggle = wrapper.find('.tab-toggle');
function openTab() {
    var content = $(this).parent().next('.content'), activeItems = wrapper.find('.active');
    if (!$(this).hasClass('active')) {
        $(this).add(content).add(activeItems).toggleClass('active');
        wrapper.css('min-height', content.height());
    }
};
tabToggle.on('click', openTab);
$(window).load(function () {
	@if(request()->has('tab'))
		$('.{{ request()->input("tab") }}').trigger('click');
	@else
		tabToggle.first().trigger('click');
	@endif
});
</script>
<script src="{!! asset('resources/assets/front/') !!}/js/jquery.accord.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script> 

<script>
$('.accord-single1').accord({
	onOpen: function() {
		var content = $('.content.active');
		wrapper.css('min-height', content.height());

	},
	onClose: function() {
		var content = $('.content.active');
		wrapper.css('min-height', content.height());

	}
});	
</script>
<style>
.tabs{
	margin-bottom:30px;
}
</style>
    <!-- /.content -->
@endsection
