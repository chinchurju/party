<div class="col-md-3">

	@if(!empty($categories1) && !collect($categories1)->isEmpty())
	<div id="collapse-menu" class="collapse-container">
		@foreach($categories1 as $key1=>$category)
			@if($category->children()->count() > 0)
				<h3>{{ $category->name }} <span class="arrow-d"></span></h3>
				@include('layouts.front.category-sidebar-sub', ['subs' => $category->children])
			@endif
		@endforeach
	</div>
	@endif

	@if(!empty($brands) && !collect($brands)->isEmpty())
	<div id="collapse-menu" class="collapse-container">
	    <h3>Barnds <span class="arrow-d"></span></h3>
		<div >
			<ul>
			@foreach($brands as $key1=>$brand)
					<input type="checkbox" class="read-more-state" id="post-{{ $key1+1 }}"  />
					<ul class="filt read-more-wrap">
						<li class="filt__item">
						<label class="label--checkbox">
							<input type="checkbox" class="checkbox filter_chk" name="brands[]" value="{{ $brand->id }}" @if(in_array($brand->id,$brans_arr)) checked="checked" @endif>{{ $brand->name }} ({{ $brand->pdr_count }})</label>
						</li>
					</ul>
			@endforeach  
			</ul>
		</div>
	</div>
	@endif
</div>
