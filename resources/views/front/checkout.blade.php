@extends('layouts.front.app')

@section('content')
  
<div class="abt-tp">
  <h2>Checkout</h2>
</div>

<div class="chck-sec">
  <div class="container">
	  <div class="box-body">
                        @include('layouts.errors-and-messages')
       </div>
	   
	   @if(!$products->isEmpty())
	  <div class="col-md-12 no-padding">
	     <div class="col-md-8">
            <div class="accordion js-accordion">
  
	  
	  
	  <div class="accordion__item js-accordion-item active">
    <div class="accordion-header js-accordion-header" id="billing_information">Billing Information</div> 
  <div class="accordion-body js-accordion-body">
    <div class="accordion-body__contents clearfix">
	  @if(count($addresses) > 0)
		<div class="col-md-12" style="margin-bottom: 19px;">	
	  <select name="billing_address_id" id="billing-address-select" class="form-control validation-passed" title="" >
		@foreach($addresses as $key => $address)
		
			<option value="{{ $address->id }}" selected="selected">{{ $address->address_1 }} ,{{ $address->address_2 }},
                                                       
                                                        {{ $address->city }} {{ $address->state_code }},
                                                        {{ $address->country->name }} {{ $address->zip }}</option>
			
		@endforeach
			<option value="">New Address</option>
	  </select>
	  </div>	
      <form style="display:none;" id="bill_addrs" method="post" action="{{ route('checkout.step','saveBillingAddress')}}" >
		{{ csrf_field() }}
		<input type="hidden" id="bill_addrs_id" name="address_id" />
		<div class="form-group clearfix">
		  <div class="col-md-12"><label>Address</label><input type="text" name="address_1" class="form-control"></div>
		</div>
		<div class="form-group clearfix">
		  <div class="col-md-12"><label> Street Address 2</label><input type="text" name="address_2" class="form-control"></div>
		</div>
		  <div class="form-group clearfix">
		  <div class="col-md-6"><label>City</label><input type="text" name="city_id" id="city_id"  class="form-control"></div>
			<div class="col-md-6"  id="bill_provinces" style="display: none;"></div>
		</div>
		  <div class="form-group clearfix">
			<div class="col-md-6"><label>Country</label>
			<select class="form-control" id="bill_country_id" name="country_id">
			@foreach($countries as $country)
				<option @if(env('SHOP_COUNTRY_ID') == $country->id) selected="selected" @endif value="{{ $country->id }}">{{ $country->name }}</option>
			@endforeach
			</select></div>
			<div class="col-md-6"><label>Telephone</label><input type="text" name="phone" class="form-control"></div>
		</div>
		
		<div class="col-md-12">
		    <div class="btn-group btn-group-vertical" data-toggle="buttons">
        <label class="btn active">
          <input type="radio" name='same_as_billing' value="1" checked><i class="fa fa-circle-o"></i><i class="fa fa-dot-circle-o"></i> <span>  Ship to this address</span>
        </label>
        <label class="btn">
          <input type="radio" name='same_as_billing' value="0"><i class="fa fa-circle-o"></i><i class="fa fa-dot-circle-o"></i><span> Ship to different address</span>
        </label>
      </div>

		  </div>
		  
	  </form>
	  <br/>
	  <div class="col-md-4 no-padding"><button class="log tab_continue_btn" data-id="shipping_information"  data-header="billing_information" data-form="bill_addrs">Continue</button></div>
	  @else
		 <p class="alert alert-danger"><a href="{{ route('customer.address.create', [$customer->id]) }}">No address found. You need to create an address first here.</a></p>
	  @endif
    </div>
    </div
    </div>
  </div>
		  
		  
		  <div class="accordion__item js-accordion-item">
    <div class="accordion-header js-accordion-header" id="shipping_information" >Shipping Information</div> 
  <div class="accordion-body js-accordion-body">
    <div class="accordion-body__contents clearfix">
		<div class="col-md-12" style="margin-bottom: 19px;">
		 <select name="shipping_address_id" id="shipping-address-select" class="form-control address-select validation-passed" title="" >
		@foreach($addresses as $key => $address)
		
			<option value="{{ $address->id }}" selected="selected">{{ $address->address_1 }} ,{{ $address->address_2 }},
                                                        
                                                        {{ $address->city }} {{ $address->state_code }},
                                                        {{ $address->country->name }} {{ $address->zip }}</option>
			
		@endforeach
		<option value="">New Address</option>
	  </select>
	  </div>	
      <form style="display:none;" id="ship_addrs" method="post" action="{{ route('checkout.step','saveShippingAddress')}}">
		{{ csrf_field() }}
		<input type="hidden" id="shipping_addrs_id" name="address_id" />
		<div class="form-group clearfix">
		  <div class="col-md-12"><label>Address</label><input type="text" name="address_1" class="form-control"></div>
		</div>
		<div class="form-group clearfix">
		  <div class="col-md-12"><label> Street Address 2</label><input type="text" name="address_2" class="form-control"></div>
		</div>
		  <div class="form-group clearfix">
			<div class="col-md-6"><label>City</label><input type="text"  name="city" id="city"  class="form-control"></div>
			<div class="col-md-6" id="ship_provinces" style="display: none;">
			
			</div>
		</div>
		  <div class="form-group clearfix">
			<div class="col-md-6"><label>Country</label>
			<select class="form-control" id="ship_country_id" name="country_id">
				@foreach($countries as $country)
                    <option @if(env('SHOP_COUNTRY_ID') == $country->id) selected="selected" @endif value="{{ $country->id }}">{{ $country->name }}</option>
                @endforeach
			
			</select></div>
			 <div class="col-md-6"><label>Telephone</label><input type="text" name="phone" class="form-control"></div>
		</div>
		 
		
		
	  </form>  
	  <br/>
	  <div class="col-md-4 no-padding"><button class="log tab_continue_btn" data-id="shipping_method" data-header="shipping_information" data-form="ship_addrs" >Continue</button></div>
    </div>
    </div
    </div>
  </div>
			  
	<div class="accordion__item js-accordion-item">
		<div class="accordion-header js-accordion-header" id="shipping_method">Shipping Method</div> 
			<div class="accordion-body js-accordion-body">
				<div class="accordion-body__contents clearfix">
					<div class="col-md-12 no-padding">
					<div class="btn-group btn-group-vertical" data-toggle="buttons" id="shipping_method_div">
					</div>
					</div>
				<div class="col-md-4 no-padding"><button class="log tab_continue_btn" data-id="payment_information" data-header="shipping_method" >Continue</button></div>
			</div>
    </div>
  </div>
				  
	 <div class="accordion__item js-accordion-item">
		<div class="accordion-header js-accordion-header" id="payment_information" >Payment Information</div> 
		<div class="accordion-body js-accordion-body">
			<div class="accordion-body__contents clearfix">
				<div class="col-md-12">
					@if(isset($payments) && !empty($payments))
					<div class="btn-group btn-group-vertical" data-toggle="buttons">
						@foreach($payments as $payment)
							<label class="btn active">
								<input type="radio" name='payment' checked="checked"  value="cash_on_delivery" ><i class="fa fa-circle-o"></i><i class="fa fa-dot-circle-o"></i> <span>  Cash On Delivery</span>
							</label>
						@endforeach
						
					</div>
					@endif
				</div>
				<div class="col-md-4 no-padding"><button class="log tab_continue_btn" data-id="order_review" data-header="payment_information" >Continue</button></div>
			</div>
		</div>
	</div>
					  
	<div class="accordion__item js-accordion-item">
    <div class="accordion-header js-accordion-header" id="order_review">Order Review</div> 
  <div class="accordion-body js-accordion-body">
    <div class="accordion-body__contents clearfix">
		  <div class="table-responsive">
             @include('front.products.product-list-table', compact('products'))
          </div>
		   <form action="{{ route('cash-on-delivery.store') }}" method="post">
				{{ csrf_field() }}
				<input type="hidden" class="billing_address" name="billing_address" value="">
				<input type="hidden" class="shipping_address" name="shipping_address" value="">
				<input type="hidden" class="rate" name="rate" value="">
				<input type="hidden" class="shipping_id" name="shipping_id" value="">
				<input type="hidden" name="shipment_obj_id" value="{{ $shipment_object_id }}">
			<div class="col-md-4 no-padding"><button type="submit" class="log" >Place Order</button></div>
		   </form>
    </div>
    </div
    </div>
  </div>				  
     
</div>
          <form action="{{ route('cash-on-delivery.store') }}" method="post">
				{{ csrf_field() }}
				<input type="hidden" class="billing_address" name="billing_address" value="26">
				<input type="hidden" class="shipping_address" name="shipping_address" value="26">
				<input type="hidden" class="rate" name="rate" value="">
				<input type="hidden" class="shipping_id" name="shipping_id" value="1">
				<input type="hidden" name="shipment_obj_id" value="{{ $shipment_object_id }}">
			<div class="col-md-4 no-padding"><button type="submit" class="log" >Place Order</button></div>
		   </form>
          </div>
				  
				  <div class="col-md-4">
				    <div class="chk-rgt">
						<h4>BILLING ADDRESS</h4>
						<h4>SHIPPING ADDRESS</h4>
						<h4>SHIPPING METHOD</h4>
						<h4>PAYMENT METHOD</h4>
				   </div>
				  </div>
				  
	  </div>
	@else
            <div class="row">
                <div class="col-md-12">
                    <p class="alert alert-warning">No products in cart yet. <a href="{{ route('home') }}">Show now!</a></p>
                </div>
            </div>
			
	@endif  
	  
	  
	</div>	
	
</div>	
<script type="text/javascript">
    $(document).ready(function () {
		
		$('#billing-address-select').change(function(e){
			if($(this).val() == ""){
				$('#bill_addrs').show();	
			}else{
				$('.billing_address').val($(this).val());
				$('#bill_addrs').hide();
			}
		})
		$('#shipping-address-select').change(function(e){
			if($(this).val() == ""){
				$('#ship_addrs').show();	
			}else{
				$('.shipping_address').val($(this).val());
				$('#ship_addrs').hide();
			}
		})
        let billingAddressId = $("#billing-address-select").val();
        $('.billing_address').val(billingAddressId);
        let courierRadioBtn = $('input[name="courier"]');
		//console.log($('input[name="courier"]'))
		
		
        courierRadioBtn.on('click',function () {
			$('.shipping_id').val($(this).val());
			$('#shipping_m_div').html($(this).data('name'));
			$('#shipping_m_div_val').html($(this).data('cost'));
			if($(this).data('cost') !='free')
				$('#total_span_s').html(parseFloat($('#total_span').val())+parseFloat($(this).data('cost')))
			else
				$('#total_span_s').html(parseFloat($('#total_span').val()));
        });
		
    });
</script>	
<script type="text/javascript">

        function findProvinceOrState(countryId,div_id) {
            $.ajax({
                url : '{{ route("home")}}/api/country/zone/' + countryId ,
                contentType: 'json',
                success: function (res) {
                    if (res.zone.length > 0) {
                        let html = '<label for="province_id">Provinces </label>';
                        html += '<select name="province_id" id="province_id" class="form-control select2">';
                        $(res.zone).each(function (idx, v) {
                            html += '<option value="'+ v.zone_id+'">'+ v.zone_name +'</option>';
                        });
                        html += '</select>';

                        $('#'+div_id+'_provinces').html(html).show();

                    } else {
                        $('#'+div_id+'_provinces').hide().html('');
                    }
                }
            });
    }
	$(document).ready(function () {
		var countryId = +$('#bill_country_id').val();
		if (countryId === 174) {
			findProvinceOrState(countryId,'bill');
		}
		var countryId = +$('#ship_country_id').val();
		if (countryId === 174) {
			findProvinceOrState(countryId,'ship');
		}

		$('#bill_country_id').on('change', function () {
			countryId = +$(this).val();
			findProvinceOrState(countryId,'bill');
		});
		$('#ship_country_id').on('change', function () {
			countryId = +$(this).val();
			findProvinceOrState(countryId,'ship');
		});
		
		
		
	   
	});
</script>		
<script >var accordion = (function(){
  
  var $accordion = $('.js-accordion');
  var $accordion_header 	= $accordion.find('.js-accordion-header');
  var $accordion_continue 	= $accordion.find('.tab_continue_btn');
  var $accordion_item 		= $('.js-accordion-item');
 
 
  var settings = {
 
    speed: 400,
  
    oneOpen: false
  };
    
  return {
  
    init: function($settings) {
		$accordion_continue.on('click', function() {
		event.preventDefault();
		var acc_header = $('#'+$(this).data('id')); 
		var c_acc_header = $('#'+$(this).data('header'));
		var hd = $(this).data('header');
		if($(this).data('header') == 'billing_information' || $(this).data('header') == 'shipping_information'){
			var ele = $(this);
			var id = $(this).data('form');
			var frm = $('#'+id);
			var post_url = frm.attr("action"); //get form action url
			var request_method = frm.attr("method"); //get form GET/POST method
			var form_data = frm.serialize(); //Encode form elements for submission
			if($(this).data('header') == 'billing_information'){
				if($('#billing-address-select').val()!='')
					form_data = form_data+'&billing_address_id='+$('#billing-address-select').val();
			}
			if($(this).data('header') == 'shipping_information'){
				if($('#shipping-address-select').val()!='')
					form_data = form_data+'&shipping_address_id='+$('#shipping-address-select').val();
			}
			$.ajax({
				url : post_url,
				type: request_method,
				data : form_data
			}).done(function(response){ //
				if(hd == 'billing_information'){
					if(response.type == "new"){
						if($("input[name='same_as_billing']:checked").val() == "1"){
							$('#ship_addrs').find("input[name='address_1']").val(response.address.address_1);
							$('#ship_addrs').find("input[name='address_2']").val(response.address.address_2);
							$('#ship_addrs').find("input[name='city']").val(response.address.city);
							$('#ship_addrs').find("[name='country_id']").val(response.address.country_id);
							$('#ship_addrs').find("[name='zip']").val(response.address.zip);
							$('#ship_addrs').find("[name='province_id']").val(response.address.province_id);
							$('#ship_addrs').find("input[name='phone']").val(response.address.phone);
							$('#ship_addrs').show();
						}
						$('#bill_addrs_id').val(response.address.id);
						$('#shipping_addrs_id').val(response.address.id);
						$('.billing_address').val(response.address.id);
						$('.shipping_address').val(response.address.id);
						
					}
				}
				if(hd == 'shipping_information'){
					html = '';
					var i = 0;
					$(response.shipping_methods).each(function (idx, v) {
						var chk = '';
						if(i==0){
							$('.shipping_id').val(v.id);
							chk = 'checked="checked"';
						}else{
							chk ='';
						}
						if(v.is_free == "0")
							html += '<label class="btn active"><input type="radio" name="courier" '+chk+' value="'+v.id+'" data-cost="'+v.cost+'" data-name="'+v.name+'" ><i class="fa fa-circle-o"></i><i class="fa fa-dot-circle-o"></i> <span> '+v.name+'( {{config('cart.currency')}} '+v.cost+')</span></label>';
						else	
							html += '<label class="btn active"><input type="radio" name="courier" '+chk+' value="'+v.id+'" data-cost="Free" data-name="'+v.name+'"  ><i class="fa fa-circle-o"></i><i class="fa fa-dot-circle-o"></i> <span> '+v.name+'</span></label>';

						i++;
					});
					$('#shipping_method_div').html(html);	
					$('.shipping_address').val(response.address.id);
					$('#shipping_addrs_id').val(response.address.id);
				}
				c_acc_header.addClass('can_click');
				accordion.toggle(acc_header);
			})
			
		}else{
		$('.shipping_id').val($('#shipping_method_div').find('input[name="courier"]:checked').val()); 
		$('#shipping_m_div').html($('#shipping_method_div').find('input[name="courier"]:checked').data('name'));
		$('#shipping_m_div_val').html($('#shipping_method_div').find('input[name="courier"]:checked').data('cost'));
		alert($('#shipping_method_div').find('input[name="courier"]:checked').data('cost'));
		if($('#shipping_method_div').find('input[name="courier"]:checked').data('cost') != 'Free'){
			$('#total_span_s').html(parseFloat($('#total_span').val())+parseFloat($('#shipping_method_div').find('input[name="courier"]:checked').data('cost')))
		}else{
			$('#total_span_s').html(parseFloat($('#total_span').val()));
		}
		c_acc_header.addClass('can_click');
        accordion.toggle(acc_header);
		}
		 
		 
      });
	  
	  $accordion_header.on('click', function() {
		  if($(this).hasClass('can_click')){ 
				accordion.toggle($(this));
		  }
      });
      
      $.extend(settings, $settings); 
      

      if(settings.oneOpen && $('.js-accordion-item.active').length > 1) {
        $('.js-accordion-item.active:not(:first)').removeClass('active');
      }
      

      $('.js-accordion-item.active').find('> .js-accordion-body').show();
    },
    toggle: function($this) {
        console.log($this);    
      if(settings.oneOpen && $this[0] != $this.closest('.js-accordion').find('> .js-accordion-item.active > .js-accordion-header')[0]) {
        $this.closest('.js-accordion')
               .find('> .js-accordion-item') 
               .removeClass('active')
               .find('.js-accordion-body')
               .slideUp()
      }
      
      $this.closest('.js-accordion-item').toggleClass('active');
      $this.next().stop().slideToggle(settings.speed);
    }
  }
})();

$(document).ready(function(){
  accordion.init({ speed: 300, oneOpen: true });
});
</script>	
	
	
	
	
@endsection