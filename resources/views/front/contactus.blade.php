@extends('layouts.front.app')

@section('content')
<style>
 .scaffold-form {
    margin: auto;
    width: 80%;
    padding: 35px;
    background: #f4f4f4;
    margin-bottom: 50px;
}
.butn{
	float: none;
    background: #cfa34c;
    padding: 8px 35px;
}
label.required:after, span.required:after {
    content: ' *';
    color: #df280a;
    font-weight: normal;
    font-family: "Helvetica Neue", Verdana, Arial, sans-serif;
    font-size: 12px;
}
em{
	display:none;
}
</style> 
<div class="abt-tp"><h2>file upload</h2></div>

<div class="cms_div">
	
	<div class="container">
	  <div class="col-md-12 no-padding">
            <div class="form-group">
                <label for="image">Images</label>
                <input type="file" name="image[]" id="image" class="form-control" multiple>
                <small class="text-warning">You can use ctr (cmd) to select multiple images</small>
            </div>
        </div>
	
	</div>
</div>
<script src="{!! asset('resources/assets/front') !!}/js/validation.js" type="text/javascript"></script>
<script>
$('#contactForm').validate();
</script>
@endsection