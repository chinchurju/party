@extends('layouts.front.app')

@section('content')
<div class="abt-tp">
  <h2>Login</h2>
</div>

<div class="login-sec">
   <div class="container">
      <div class="col-md-12 no-padding">
         <div class="lgn">
			
           <div class="col-md-6">

		   <!--<div class="login">
              <h3>REGISTERED CUSTOMERS</h3>
			   <div class="col-md-12">@include('layouts.errors-and-messages')</div>
                  <form action="{{ route('login') }}" method="post" >
					{{ csrf_field() }}
                    <div class="form-group clearfix">
                       <div class="input-group input-group-lg"> <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-user"></i></span>
						<input type="email" class="form-control" placeholder="Email" required id="email" name="email" value="{{ old('email') }}">
					   </div>
                    </div>
                    
                    <div class="form-group clearfix">
                       <div class="input-group input-group-lg"> <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-lock"></i></span>
						<input type="password" name="password" id="password" value="" class="form-control" placeholder="Password" required>
					   </div>
                    </div>
                    
                    <a href="{{route('password.request')}}">Forgot Password</a>
                    <button class="log" type="submit">Log In</button>
                 </form>
              </div>-->
              
              
              
              <div class="login">
              <h3>I ALREADY HAVE AN ACCOUNT</h3>
			   <div class="col-md-12">@include('layouts.errors-and-messages')</div>
                  <form action="{{ route('login') }}" method="post" >
					{{ csrf_field() }}
                    <div class="form-group clearfix">
                       <input type="email" class="form-control" placeholder="Email" required id="email" name="email" value="{{ old('email') }}" style="border-radius:5px 5px 0 0">
                    </div>
                    
                    <div class="form-group clearfix">
                       <input type="password" name="password" id="password" value="" class="form-control" placeholder="Password" required style="border-radius:0 0 5px 5px">
                    </div>
                    
                    <a href="{{route('password.request')}}">Forgot Password</a>
                    <button class="log" type="submit">Log In</button>
                 </form>
              </div>
              
              
           </div>
           
           <div class="col-md-6">
              <div class="login re">
              <h3>I WANT TO REGISTER</h3>
                  <button class="log" onclick="window.location.href='{{ route('register') }}'">Create an Account</button>
              </div>
           </div>
           
         </div>
      </div>
   </div>
</div>
@endsection