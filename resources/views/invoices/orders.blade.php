<!DOCTYPE html>
<html>
<head></head>
<body style='background: white; color: #222'>
	<table width='750px' border='0'>
		<thead>
			<tr>
				<th colspan='2' style='color:#FFF'>
					<img style='margin-left: 350px;height:75px;' src='{!! asset('resources/assets/front/') !!}/images/'>
					<h4 style="color: black">Request From Uni <span style="float:right">Request Date : {{ date('M d, Y', strtotime($order->servicedate)) }}<br/>Request Date : {{ date('h:i:sa', strtotime($order->servicedate)) }}</span></h4>
				</th>
				<th><h4 style="color: black"></h4></th>
			</tr>
			<tr>
				<th>
					RequestID #{{$order->reference}}
				</th>
			</tr>
		</thead>
		<tbody>
			<tr><td>Name</td><td>{{$customer->name}}</td></tr>
			<tr><td>Email</td><td>{{$customer->email}}</td></tr>
			<tr><td>Phone</td><td>{{$customer->phone}}</td></tr>
		    <tr><td>Location</td>
		    	<td><div id="map" style="width:100%;height:250px"></div><br/></td></tr>
			
			<tr>
				<td colspan='2'>
					<table width='750px' class='table' border='0'>
						<thead style='background:#949494;'>
							<tr>
								<th colspan="3">Service</th>
								<th>Amount</th>
							  <!--<th>Quantity</th>-->
								<th>Total</th>
							</tr>
						</thead>
						<tbody>
							@foreach($products as $product)
								<tr style='text-align:center; border-bottom: #222 solid; '>
									<td><!--<img style='width:50px;' src='{{ asset("storage/products/thumb270x300_$product->cover") }}'>--></td> 
									<td></td>
									<td>{{$product->name}}</td>
									@if($order->country_id !=174)
									<td>USD {{$product->price*(env('USD_RATE'))}}</td>
							    	<!--<td>{{$product->pivot->quantity}}</td>-->
									<td>USD {{number_format($product->price * $product->pivot->quantity, 2)*(env('USD_RATE'))}}</td>
									@else
									<td>QAR {{$product->price}}</td>
							    	<!--<td>{{$product->pivot->quantity}}</td>-->
									<td>QAR {{number_format($product->price * $product->pivot->quantity, 2)}}</td>
									@endif
								</tr>
							@endforeach
								<tr style='background:#949494;color:#FFF;text-align:right;'>
									<td  colspan='3' style=' padding:0.8em;'>Subtotal</td>
									@if($order->country_id !=174)
									<td  colspan='3'  style=' padding:0.8em;'>USD {{$order->total_products*(env('USD_RATE'))}}</td>
									@else
									<td  colspan='3'  style=' padding:0.8em;'>QAR {{$order->total_products}}</td>
									@endif
								</tr>
							    <!--<tr style='background:#949494;color:#FFF;text-align:right;'>
									<td  colspan='3' style=' padding:0.8em;'>{{$order->courier}}</td>
									<td  colspan='3'  style=' padding:0.8em;'>{{$order->shipping_charge}}</td>
								</tr>-->
								<tr style='background:#949494;color:#FFF;text-align:right;'>
									<td  colspan='3' style=' padding:0.8em;'>TOTAL</td>
									@if($order->country_id !=174)
									<td  colspan='3' style=' padding:0.8em;'>USD {{$order->total*(env('USD_RATE'))}}</td>
									@else
									<td  colspan='3' style=' padding:0.8em;'>QAR {{$order->total}}</td>
									@endif
								</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>

	@section('js')
 <script>
      function initMap() {
		     var myLatLng = {lat: {{ $order->location_latitude }}, lng: {{ $order->location_longitude }}};

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 8,
          center: myLatLng
        });
        var geocoder = new google.maps.Geocoder();

        document.getElementById('submit').addEventListener('click', function() {
          geocodeAddress(geocoder, map);
        });
      }

      function geocodeAddress(geocoder, resultsMap) {
        var address = document.getElementById('address').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
              map: resultsMap,
              position: results[0].geometry.location
            });
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
    </script>

 <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC1TJP5byEqvvguoqDfOVihZ5yIa5rykg4&callback=initMap">
    </script>

@endsection


</body>
</html>
