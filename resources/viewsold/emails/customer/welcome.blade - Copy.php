<html xmlns="http://www.w3.org/1999/xhtml" style="background-color: #ebebeb;"><head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="initial-scale=1.0, width=device-width">
</head>
<body style="font-family: Verdana, Arial; font-weight: normal; margin: 0; padding: 0; text-align: left; color: #333333; background-color: #ebebeb; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; background: #ebebeb; font-size: 12px;">
<p style="font-family: Verdana, Arial; font-weight: normal;"><style type="text/css">
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    design
 * @package     base_default
 * @copyright   Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
@import url(http://fonts.googleapis.com/css?family=Raleway:400,500,700);
/* Font Styles */
/* Media Queries */
/* Setting the Web Font inside a media query so that Outlook doesn't try to render the web font */
@media screen {
  .email-heading h1,
  .store-info h4,
  th.cell-name,
  a.product-name,
  p.product-name,
  .address-details h6,
  .method-info h6,
  h5.closing-text,
  .action-button,
  .action-button a,
  .action-button span,
  .action-content h1 {
    font-family: 'Raleway', Verdana, Arial !important;
    font-weight: normal;
  }
}
@media screen and (max-width: 600px) {
  body {
    width: 94% !important;
    padding: 0 3% !important;
    display: block !important;
  }

  .container-table {
    width: 100% !important;
    max-width: 600px;
    min-width: 300px;
  }

  td.store-info h4 {
    margin-top: 8px !important;
    margin-bottom: 0px !important;
  }

  td.store-info p {
    margin: 5px 0 !important;
  }

  .wrapper {
    width: 100% !important;
    display: block;
    padding: 5px 0 !important;
  }

  .cell-name,
  .cell-content {
    padding: 8px !important;
  }
}
@media screen and (max-width: 450px) {
  .email-heading,
  .store-info {
    float: left;
    width: 98% !important;
    display: block;
    text-align: center;
    padding: 10px 1% !important;
    border-right: 0px !important;
  }

  .address-details, .method-info {
    width: 85%;
    display: block;
  }

  .store-info {
    border-top: 1px dashed #c3ced4;
  }

  .method-info {
    margin-bottom: 15px !important;
  }
}
/* Remove link color on iOS */
.no-link a {
  color: #333333 !important;
  cursor: default !important;
  text-decoration: none !important;
}

.method-info h6,
.address-details h6,
.closing-text {
  color: #3696c2 !important;
}

td.order-details h3,
td.store-info h4 {
  color: #333333 !important;
}

.method-info p,
.method-info dl {
  margin: 5px 0 !important;
  font-size: 12px !important;
}

td.align-center {
  text-align: center !important;
}

td.align-right {
  text-align: right !important;
}

/* Newsletter styles */
td.expander {
  padding: 0 !important;
}

table.button td,
table.social-button td {
  width: 92% !important;
}

table.facebook:hover td {
  background: #2d4473 !important;
}

table.twitter:hover td {
  background: #0087bb !important;
}

table.google-plus:hover td {
  background: #CC0000 !important;
}

/* ============================================ *
 * Product Grid
 * ============================================ */
@media screen and (max-width: 600px) {
  .products-grid tr td {
    width: 50% !important;
    display: block !important;
    float: left !important;
  }
}
.product-name a:hover {
  color: #3399cc !important;
  text-decoration: none !important;
}


</style></p>

<div style="width: 600px; height: 100%; margin: 0 auto; padding: 0; background-color: #f1f2f3; font-family: 'Roboto-Regular', Arial, Tahoma, Verdana, sans-serif; font-weight: 300; font-size: 13px; text-align: center; border-collapse: collapse;" class="logo-container">
<table cellpadding="0" cellspacing="0" style="height: 60px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse; padding: 0; margin: 0;" width="100%"><tbody><tr style="">
<td style="font-family: Verdana, Arial; font-weight: normal; border-collapse: collapse; vertical-align: top; padding: 0; margin: 0;">
			<table style="max-width: 600px; margin: 0 auto; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse; padding: 0;" width="100%"><tbody><tr>
<td style="width: 50%; text-align: left; padding-left: 16px; font-family: Verdana, Arial; font-weight: normal; border-collapse: collapse; vertical-align: top; padding: 0; margin: 0;"><a href="{{ route('home')}}" style="color: #3696c2;">
                                        <img width="165" height="80" src="{!! asset('resources/assets/front/') !!}/images/logo.png" alt="Jhaircare" border="0" style="-ms-interpolation-mode: bicubic;"></a></td>
					</tr></tbody></table>
</td>
		</tr></tbody></table>
<table cellpadding="0" cellspacing="0" style="margin: 0 auto; padding: 10px; background-color: #f1f2f3; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;" width="100%"><tbody><tr>
<td style="font-family: Verdana, Arial; font-weight: normal; border-collapse: collapse; vertical-align: top; padding: 0; margin: 0;">


<table cellpadding="0" cellspacing="0" border="0" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse; padding: 0; margin: 0;"><tbody><tr>
<td class="action-content" style="font-family: Verdana, Arial; font-weight: normal; border-collapse: collapse; vertical-align: top; padding: 10px 20px 15px; margin: 0; line-height: 18px;">
            <h1 style="font-family: Verdana, Arial; font-weight: bold; font-size: 25px; margin-bottom: 25px; margin-top: 5px; line-height: 28px;">Welcome to Jhaircare.</h1>
            <p style="font-family: Verdana, Arial; font-weight: normal;">To log in when visiting our site just click <a href="{{ route('home')}}" style="color: #3696c2;">Login</a> or <a href="{{ route('home')}}" style="color: #3696c2;">My Account</a> at the top of every page, and then enter your email address and password.</p>
            <p class="highlighted-text" style="font-family: Verdana, Arial; font-weight: normal; border: 1px solid #c3ced4; padding: 13px 18px; background: #f1f1f1;">
                Use the following values when prompted to log in:<br><strong style="font-family: Verdana, Arial; font-weight: normal;">Email</strong>: {{ $email }} <br><strong style="font-family: Verdana, Arial; font-weight: normal;">Password</strong>: **************
            </p>
     
        </td>
    </tr></tbody></table>
<!-- End Content -->
</td>
            </tr></tbody></table>
</div>
    </table>
<table width="100%" cellspacing="0" cellpadding="0" style="margin: 0 auto; padding: 5px 5px;; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;"><tbody><tr>
<td align="left" valign="top" class="m_-7336771439130682003container" style="color: #2c2c2c; display: block; line-height: 20px; font-weight: 300; margin: 0 auto; clear: both; background-color: transparent; font-family: Verdana, Arial; border-collapse: collapse; vertical-align: top; padding: 0;">
                      
                              <p style="width: 100%; text-align: center; color: #fff; font-family: Verdana, Arial; font-weight: normal;"> Copyright © 2018 Jhaircare. All Rights Reserved. </p>
                        
                      </td>
                    </tr></tbody>

</body></html>