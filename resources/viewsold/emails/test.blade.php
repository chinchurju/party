<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <body>
    <div style="width:100%;height:100%;margin:10px auto;padding:0;background-color:#f1f2f3;font-family:'Roboto-Regular', Arial, Tahoma, Verdana, sans-serif;font-weight:300;font-size:13px;text-align:center;">
      <table width="100%" cellspacing="0" cellpadding="0" style="height:100px;">
        <tbody>
          <tr style="">
            <td>
              <table width="100%" style="max-width:600px;margin:0 auto;">
                <tbody>
                  <tr>
                    <td style="width:50%;text-align:left;padding-left:16px;"> <a style="text-decoration:none;color:#ffffff;font-size:13px;" href="{{ route('home') }}" target="_blank"> <img border="0" height="80" src="{!! asset('resources/assets/front/') !!}/images/logo.png" style="border:none" class="CToWUd"></a> </td>
                    <td style="width:50%;text-align:right;color:rgba(255,255,255,0.8);font-family:'Roboto-Medium', sans-serif;font-size:14px;font-style:normal;padding-right:16px;"> Order confirmed </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
      <table width="100%" cellspacing="0" cellpadding="0" style="margin:0 auto;padding:10px;background-color:#f1f2f3;">
        <tbody>
          <tr>
            <td>
              <table width="100%" cellspacing="0" cellpadding="0" style="margin:0 auto;max-width:600px;background:#ffffff;">
                <tbody>
                  <tr>
                    <td align="left" valign="top" class="m_-7336771439130682003container" style="display:block;margin:0 auto;clear:both;padding:0px 40px;">
                      <table width="100%" cellspacing="0" cellpadding="0" style="margin:0 auto;max-width:600px;background:#ffffff;">
                        <tbody>
                          <tr>
                            <td align="left" valign="top" class="m_-7336771439130682003container" style="color:#212121;display:block;margin:0 auto;clear:both;padding:3px 0 0 0;">
                              <table width="100%" cellspacing="0" cellpadding="0">
                                <tbody>
                                  <tr>
                                    <td id="m_-7336771439130682003journey" valign="top" align="left" style="float:right;background-color:#fafafa;padding:0;text-align:center;vertical-align:middle;"> <img border="0" src="https://ci6.googleusercontent.com/proxy/TBbuYm39N7ohpZq1IDeYxMoTDi1A3nGMJs_hHCcrbpqQHyu3QN9PvuFj_yyGa7Eo1HwFKdqkTXNBezPD1DLFgZkVpb9Q2f7fqSXcj26hgKhCE6ob33-7O2se1UIcYOGBtBtXnzFbdV5vFmh-DRGKKpTs3tEFbU-r0fHh5q4=s0-d-e1-ft#http://rukmini1.flixcart.com/www/330/60/promos/21/12/2016/99f04ab8-e5c1-4b19-a496-8e1a910800da.png?q=90" alt="Order Jouney" style="border:none;width:80%" class="CToWUd">
                                    </td>
                                    <td valign="top" align="left" style="float:left;vertical-align:middle;"> <p style="font-family:'Roboto-Medium', sans-serif;font-size:16px;font-weight:normal;font-style:normal;line-height:1.5;color:#212121;margin:16px 0px;">Hi {{ $customer->name }},</p> </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <table width="100%" cellspacing="0" cellpadding="0" style="margin:0 auto;padding:30px 0 0 0;max-width:600px;background:#ffffff;" border="0">
                        <tbody>
                          <tr>
                            <td>
                              <table width="100%" border="0" cellpadding="0" cellspacing="0" align="left" style="table-layout:fixed;border-spacing:0;border-collapse:collapse;width:100%;max-width:260px;border:none;margin-bottom:24px;margin-right:30px;">
                                <tbody>
                                  <tr>
                                    <td valign="top" align="left" style="border-right:1px solid #f4f4f4;padding-right:10px;"> <p style="padding:0;margin:0;font-size:16px;font-family:'Roboto-Medium', sans-serif;color:#212121;">Order successfully placed.</p> <p style="padding:0;margin:0;font-size:16px;font-family:'Roboto-Medium', sans-serif;color:#212121;margin-bottom:12px;"> </p>
                                      <p style="padding:0;margin:0;font-size:12px;line-height:20px;font-family:Roboto, sans-serif;font-weight:400;padding-bottom:12px;"> We are pleased to confirm your order no #{{$order->id}}. <br> Thank you for shopping with Jhaircare! </p>
                                      <a style="font-family:'Roboto-Medium', sans-serif;text-decoration:none;background-color:rgb(41,121,251);color:#fff;min-width:160px;padding:7px 16px;border-radius:2px;text-align:center;display:inline-block;font-size:14px;" href="{{route('home')}}" class="m_-7336771439130682003fk-email-button" target="_blank">Manage your order</a> </td>
                                    </tr>
                                  </tbody>
                                </table>
                             <td>   
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="left" style="border-spacing:0;border-collapse:collapse;width:100%;max-width:220px;border:none;margin-bottom:24px;">
                                  <tbody>
                                    <tr>
                                    <td valign="top" align="left"> <p style="padding:0;margin:0;font-size:16px;font-family:'Roboto-Medium', sans-serif;color:#212121;margin-bottom:12px;">Delivery Address</p> <p style="padding:0;margin:0;font-size:12px;font-family:Roboto, sans-serif;line-height:20px;padding-right:35px;color:#212121;"> </p>
										<p style="padding:0;margin:0;font-size:12px;font-family:Roboto,sans-serif;line-height:20px;padding-right:35px;color:#212121"> </p>
										{{ $order->address->address_1 }}<br/>
										{{ $order->address->address_2 }}<br/>
										{{ $order->address->city }}<br/>
										 @if(isset($order->address->province))
                                            {{ $order->address->province->name }}<br/>
                                         @endif
										{{ $order->address->zip }}<br/>
										{{ $order->address->country->name }}<br/>
										{{ $order->address->phone }}<br/>
											
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                       
						<table width="100%" cellspacing="0" cellpadding="0" style="margin:0 auto;max-width:600px;background:#ffffff">
							<tbody>
									<tr>
										<td align="left" valign="top" class="m_-2941836887641383744m_-7336771439130682003container" style="color:#212121;display:block;margin:0 auto;clear:both;padding:30px 0 24px 0">
											<table width="100%" cellspacing="0" cellpadding="0">
												<tbody>
														
													@if(!$items->isEmpty())
														@foreach($items as $item)	
													
															
															<tr>
															  <td width="120" valign="top" align="left"> 
																<a style="color:#ffffff;text-decoration:none;font-size:13px;display:block;width:100px">
																 <img border="0" src="{{ asset("storage/products/$item->cover") }}" alt="Blue Lady Bag" style="border:none;width:100%" class="m_-2941836887641383744CToWUd CToWUd">
																</a> 
															  </td>
															  <td width="8"></td>
															  <td valign="top" align="left"> 
																<p style="margin-bottom:13px">
																	<a style="font-family:Roboto,sans-serif;font-size:14px;font-weight:normal;font-style:normal;line-height:1.25;color:#2175ff;text-decoration:none">
																	<strong style="font-size:11px">{{ $item->name }}</strong>
																	</a>
																			</p>
																																
																<p style="font-family:'Roboto-Medium',sans-serif;font-style:normal;line-height:1.5;color:#212121;margin:5px 0px">
																<span style="padding-right:10px">                                        <span class="m_-2941836887641383744price">QAR {{ $item->price }}</span>            </span>
																 <span style="font-family:'Roboto-Medium',sans-serif;font-size:12px;font-weight:normal;font-style:normal;line-height:1.5;font-stretch:normal;color:#878787;margin:0px 0px;border:1px solid #dfdfdf;display:inline;border-radius:3px;padding:3px 10px">
																 Qty: {{ $item->pivot->quantity }}</span> </p> </td>
															</tr>
														@endforeach
													@endif
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
						
						
						 <table width="100%" cellspacing="0" cellpadding="0" style="margin:0 auto;max-width:600px;background:#ffffff">
							<tbody>
							<tr>
							
								<td class="bg-warning" style="width: 82%;text-align: right;">Subtotal</td>
								<td class="bg-warning" style="text-align: right;">{{ $order['total_products'] }}</td>
							</tr>
							<tr>
							
								<td class="bg-warning"style="width: 82%;text-align: right;">Tax</td>
								<td class="bg-warning" style="text-align: right;">{{ $order['tax'] }}</td>
							</tr>
							<tr>
								<td class="bg-warning"style="width: 82%;text-align: right;">Discount</td>
								<td class="bg-warning" style="text-align: right;">{{ $order['discounts'] }}</td>
							</tr>
							<tr>
								<td class="bg-success text-bold"style="width: 82%;text-align: right;">Order Total</td>
								<td class="bg-success text-bold" style="text-align: right;">{{ $order['total'] }}</td>
							</tr>
							@if($order['total_paid'] != $order['total'])
								<tr>
									<td class="bg-danger text-bold"style="width: 82%;text-align: right;"><b>Total paid</b></td>
									<td class="bg-danger text-bold" style="text-align: right;"><b>{{ $order['total_paid'] }}</b></td>
								</tr>
							@endif
							</tbody>
						</table>
                    
                       
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tbody>
                            <tr>
                              <td height="1" style="background-color:#f0f0f0;font-size:0px;line-height:0px;" bgcolor="#f0f0f0"></td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
                
              </td>
            </tr>
          </tbody>
        </table>
        <table width="100%" cellspacing="0" cellpadding="0" style="margin:0 auto;padding:5px 5px;">
                  <tbody>
                    <tr>
                      <td align="left" valign="top" class="m_-7336771439130682003container" style="color:#2c2c2c;display:block;line-height:20px;font-weight:300;margin:0 auto;clear:both;background-color:transparent;">
                      
                              <p style="width:100%;text-align:center;"> Copyright © 2018 Jhaircare. All Rights Reserved. </p>
                        
                      </td>
                    </tr>
                  </tbody>
                </table>
      </div>
    </body>
</html>