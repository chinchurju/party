
<div class="col-md-8">
    <!-- search form -->
    <form action="{{$route}}" method="get" id="admin-search">
        <div class="input-group col-md-4" style="float:left;">
			<select class="form-control" style="background: #eeeeee;margin-left:5px;" name="status">
				<option value="">Select Order Status</option>
				@if(count($statuses)>0)
					@foreach ($statuses as $status)
						<option @if(request()->has('status')) @if(request()->input('status') == $status->id) selected="selected" @endif @endif value="{{$status->id}}">{{$status->name}}</option>
					@endforeach
					
				@endif		
			</select>
        </div>
		<div class="input-group col-md-6" style="float:left;margin-left: 12px;">
			<input class="form-control reservation dateRange" placeholder="Order Date" readonly="" value="{!! request()->input('dateRange') !!}" name="dateRange" aria-label="Text input with multiple buttons ">
            <span class="input-group-btn">
                 <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i> Search </button>
				 <a href="{{$route}}"  class="btn btn-flat" style="margin-left:5px;    background: #dddddd;    color: #000;">Reset Filter </a>
            </span>
        </div>
		
    </form>
	
    <!-- /.search form -->
</div>
<div class="col-md-4">
		<div style="float:right;">
			<a href="{{$route}}/export"  class="btn btn-flat" style="margin-left:5px;    background: #dddddd;    color: #000;">Export </a>
		</div>
</div>