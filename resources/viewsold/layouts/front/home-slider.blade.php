<div class="slider">
  <div class="tp-banner-container">
		<div class="tp-banner" >
			<ul>	
				@if(!$banners->isEmpty())
					@foreach($banners as $banner)
						<li data-transition="slideleft" data-slotamount="7" data-masterspeed="1000" >
							<img src='{{ asset("$banner->banners_image") }}'  alt="{{$banner->banners_title}}"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
							
							<div class="tp-caption large_text skewfromleft customout"
								data-x="right" data-hoffset="0"
								data-y="200"
								data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
								data-speed="800"
								data-start="800"
								data-easing="Power4.easeOut"
								data-endspeed="300"
								data-endeasing="Power1.easeIn"
								data-captionhidden="on"
								style="z-index: 6"><span>{{$banner->banners_title}}</span> <br> {{$banner->banners_title}}
							</div>
							
							
							<div class="tp-caption large_text skewfromleft customout"
								data-x="right" data-hoffset="-160"
								data-y="350"
								data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
								data-speed="800"
								data-start="1200"
								data-easing="Power4.easeOut"
								data-endspeed="300"
								data-endeasing="Power1.easeIn"
								data-captionhidden="on"
								style="z-index: 9"><a href="{{$banner->banners_link}}">{{$banner->banners_link_text}} <img src="{!! asset('resources/assets/front/') !!}/images/slider/arw.svg"></a>
							</div>
							
						</li>
					@endforeach	
                @endif
                
			</ul>
			<div class="tp-bannertimer"></div>
		</div>
	</div>
</div>