<ul class="nav navbar-nav" >
	@foreach($categories_new as $category)
		@if($category->children()->count() > 0)
			 <li class="dropdown">
				<a href="{{route('front.category.slug', $category->slug)}}" class="dropdown-toggle" data-toggle="dropdown" >{{$category->name}}</a>
					@include('layouts.front.category-sub', ['subs' => $category->children])
			</li>		
		@else
			<li><a href="{{route('front.category.slug', $category->slug)}}" @if(request()->segment(2) == $category->slug) class="active" @endif>{{$category->name}}</a></li>
		@endif
		
	@endforeach
	<li>
	
	<button id="toggle-search" class="header-button"><i class="fa fa-search"></i></button>
          	<form id="search-form" action="{{ route('search.product')}}">
				<fieldset>
					<input name="q" type="search" placeholder="Search" value="{!! request()->input('q') !!}" />
				</fieldset>
				<input type="submit" value="Search" />
			</form>    
	    
	</li>
</ul>   

