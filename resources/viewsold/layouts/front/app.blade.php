<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>{{ config('app.title') }}</title>
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link rel="stylesheet" href="{!! asset('resources/assets/front/css/bootstrap.min.css') !!}" type="text/css">
<link rel="stylesheet" href="{!! asset('resources/assets/front/css/font-awesome.css') !!}" type="text/css">
<link rel="stylesheet" href="{!! asset('resources/assets/front/css/bootsnav.css') !!}" type="text/css">
<link rel="stylesheet" type="text/css" href="{!! asset('resources/assets/front/css/slider.css') !!}">
<link rel="stylesheet" href="{!! asset('resources/assets/front/css/stylesheet.css') !!}" type="text/css">

<script src="{!! asset('resources/assets/front/js/jquery-2.1.0.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('resources/assets/front/js/bootstrap.min.js') !!}" type="text/javascript"></script>

</head>

<body>

<body>
<noscript>
    <p class="alert alert-danger">
        You need to turn on your javascript. Some functionality will not work if this is disabled.
        <a href="https://www.enable-javascript.com/" target="_blank">Read more</a>
    </p>
</noscript>
@include('layouts.front.header')

@yield('content')


@include('layouts.front.footer')
</html>