@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-6">
                        <h2>
                            <a href="{{ route('admin.customers.show', $customer->id) }}">{{$customer->name}}</a> <br />
                            <small>{{$customer->email}}</small> <br />
                            <small>Request ID: <strong>{{$order->reference}}</strong></small>
                        </h2>
                    </div>
                    <div class="col-md-3 col-md-offset-3">
                        <h2><a href="{{route('admin.orders.invoice.generate', $order['id'])}}" class="btn btn-primary btn-block">Download Invoice</a></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-body">
                <h4> <i class="fa fa-shopping-bag"></i> Request Information</h4>
                <table class="table">
                    <thead>
                        <tr>
                            <td class="col-md-3">Date</td>
                            <td class="col-md-3">Customer</td>
                            <td class="col-md-3">Payment</td>
                            <td class="col-md-3">Status</td>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{ date('M d, Y h:i a', strtotime($order['created_at'])) }}</td>
                        <td><a href="{{ route('admin.customers.show', $customer->id) }}">{{ $customer->name }}</a></td>
                        <td><strong>{{ $order['payment'] }}</strong></td>
                        <td><button type="button" class="btn btn-info btn-block">{{ $currentStatus->name }}</button></td>
                    </tr>
                    </tbody>
                    <tbody>
                 <!--   <tr>
                        <td></td>
                        <td></td>
                        <td class="bg-warning">Subtotal</td>
                        <td class="bg-warning">{{ $order['total_products'] }}</td>
                    </tr>
                    
                    <tr>
                        <td></td>
                        <td></td>
                        <td class="bg-warning">{{ $order['courier'] }}</td>
                        <td class="bg-warning">{{ $order['shipping_charge'] }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td class="bg-success text-bold">Order Total</td>
                        <td class="bg-success text-bold">{{ $order['total'] }}</td>
                    </tr>-->
					<tr></tr>
                    @if($order['total_paid'] != $order['total'])
                        <tr>
                            <td></td>
                            <td></td>
                            <td class="bg-danger text-bold">Service Total</td>
                            <td class="bg-danger text-bold">{{ $order['total'] }}</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        @if($order)
            
            <div class="box">
                @if(!$items->isEmpty())
                    <div class="box-body">
                        <h4> <i class="fa fa-gift"></i> Services</h4>
                        <table class="table">
                            <thead>
                            <!--<th class="col-md-2">SKU</th>-->
                            <th class="col-md-2">Services</th>
                            <th class="col-md-2">Description</th>
                          <!--  <th class="col-md-2">Quantity</th>-->
                            <th class="col-md-2">Price</th>
                            </thead>
                            <tbody>
                            @foreach($items as $item)
                                <tr>
                               <!--    <td>{{ $item->sku }}</td>-->
                                    <td>{{ $item->name }}</td>
                                    <td>{!! $item->description !!}</td>
                                  <!--  <td>{{ $item->pivot->quantity }}</td>-->
                                    <td>{{ $item->price }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
								
				<div class="box-body">
                    <table class="table">
                        <thead>
                            <th class="col-md-2">Date</th>
                            <th class="col-md-2">Time</th>
                            <th class="col-md-2">Description</th>
                        </thead>
						<tbody>
							<tr>
								<td>{{ $order->servicedate }}</td>
								<td>{{ $order->servicetime }}</td>
								<td>{{ $order->description }}</td>
                            </tr>
						</tbody>
                    </table>
                </div>
				
               <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4> <i class="fa fa-shopping-bag"></i> Attachments</h4>
                            @foreach($order->attachment as $attach)
                                <td>
                                    @if($attach->type == 'image')
                                        <a href="{{asset("storage/app/$attach->images") }}" target="_blank"> <img src="{{ asset("storage/app/$attach->images") }}" alt="" class="img-responsive"></a>

                                        @elseif($attach->type == 'document')
                                        <a href="{{asset("storage/app/$attach->images") }}" target="_blank"> <img src="{{ asset("storage /app/$attach->images") }}" alt="" class="img-responsive">Document</a>

                                        @else
                                        <a href="{{asset("storage/app/$attach->images") }}" target="_blank"> <img src="{{ asset("storage/app/$attach->images") }}" alt="" class="img-responsive">Video</a>
                                    @endif
                                </td>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4> <i class="fa fa-map-marker"></i> Address</h4>
                            <table class="table">
                                <thead>
                                    <th>Address 1</th>
                                    <th>Address 2</th>
                                    <th>City</th>
                                   <!-- <th>Province</th>-->
                                    <th>Street Number</th>
                                   <!-- <th>Country</th>-->
                                    <th>Phone</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $order->address->address_1 }}</td>
                                        <td>{{ $order->address->address_2 }}</td>
                                        <td>{{ $order->address->city }}</td>
                                       <!-- <td>
                                            @if(isset($order->address->province))
                                                {{ $order->address->province->zone_name }}
                                            @endif
                                        </td>-->
                                        <td>{{ $order->address->zip }}</td>
                                        <!--<td>{{ $order->address->country->name }}</td>-->
                                        <td>{{ $order->address->phone }}</td>
                                        <td width="40%">
                                            <div id="map" style="width:100%;height:250px"></div><br/>
                                           <!-- <div class="sharethis-inline-share-buttons"></div>-->
                                           <div><a href="https://api.whatsapp.com/send?text=<?php echo urlencode('https://maps.google.com/maps?q='.$order->address->location_latitude.','.$order->address->location_longitude.'&hl');?>">
										   <i class="fa fa-whatsapp fa-2x" style="color: #6ee07d;"></i> </a></div>
										  <!-- <div> 
										   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
										   <i class="fa fa-envelope-o fa-2x"></i></button>
										   </div>
										   <div class="modal" id="myModal">
											<div class="modal-dialog">
											  <div class="modal-content">
											  
											  <form id="email" method="post" action="mailit.php">
											   <div class="col-md-6 col-sm-6">
													<input class="form-control" type="email" name="email" placeholder="Email" required>
											   </div>
												<div class="col-md-12 col-sm-12">
													<button class="btn btn-default hvr-bounce-to-right" name="submit" type="submit">SUBMIT</button>
												</div>
											  </form>
												
												</div>
											</div>
										</div>-->
									    </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
            <div class="box-footer">
                <div class="btn-group">
                    <a href="{{ route('admin.orders.index') }}" class="btn btn-default">Back</a>
                    @if($user->hasPermission('update-order'))<a href="{{ route('admin.orders.edit', $order->id) }}" class="btn btn-primary">Edit</a>@endif
                </div>
            </div>
        @endif
    </section>
    <!-- /.content -->
@endsection



@section('js')
 <script>
      function initMap() {
		     var myLatLng = {lat: {{ $order->address->location_latitude }}, lng: {{ $order->address->location_longitude }}};

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 8,
          center: myLatLng
        });
        var geocoder = new google.maps.Geocoder();

        document.getElementById('submit').addEventListener('click', function() {
          geocodeAddress(geocoder, map);
        });
      }

      function geocodeAddress(geocoder, resultsMap) {
        var address = document.getElementById('address').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
              map: resultsMap,
              position: results[0].geometry.location
            });
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
    </script>

 <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC1TJP5byEqvvguoqDfOVihZ5yIa5rykg4&callback=initMap">
    </script>


<!--
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC1TJP5byEqvvguoqDfOVihZ5yIa5rykg4"></script>

<script type="text/javascript">
    function initMap() {
       // alert('sssssssssss');
        var myLatLng = {lat: {{ $order->address->location_latitude }}, lng: {{ $order->address->location_longitude }}};

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map
          //title: 'Hello World!'
        });
    }
    initMap();
</script>-->

<!--<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5c270e7da1277a001142049a&product=inline-share-buttons' async='async'></script>-->

@endsection

