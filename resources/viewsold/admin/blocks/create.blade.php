@extends('layouts.admin.app')
@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
		<h2>Create Block</h2>
        <div class="box">
            <form action="{{ route('admin.block.store') }}" method="post" class="form" enctype="multipart/form-data">
                <div class="box-body">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Title <span class="text-danger">*</span></label>
                        <input type="text" name="block_title" id="name" placeholder="Title" class="form-control" value="{{ old('name') }}">
                    </div>
					 {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Content</label>
                        <textarea  id="editor1" name="block_content" class="form-control" ></textarea>
                    </div>
					<div class="form-group">
                        <label for="name">Display Order</label>
                        <input type="number" name="display_order" id="display_order" min="1"  placeholder="Display Order" class="form-control" value="1">
                    </div>
					<div class="form-group">
                        <label for="name">Status</label>
                        <select name="status" class="form-control" >
							<option value="1">Enable</option>
							<option value="0">Disable</option>
						</select>		
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.block.index') }}" class="btn btn-default">Back</a>
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box -->
    </section>

    <!-- /.content -->
	<script src="{!! asset('resources/views/admin/plugins/jQuery/jQuery-2.2.0.min.js') !!}"></script>
<script type="text/javascript">
		$(function () {
			
			//for multiple languages
			
				// Replace the <textarea id="editor1"> with a CKEditor
				// instance, using default configuration.
			CKEDITOR.replace('editor1');
			
		
			
			//bootstrap WYSIHTML5 - text editor
			$(".textarea").wysihtml5();
			
    });
</script>
	
@endsection
