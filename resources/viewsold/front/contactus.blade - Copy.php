@extends('layouts.front.app')

@section('content')
<style>
 .scaffold-form {
    margin: auto;
    width: 80%;
    padding: 35px;
    background: #f4f4f4;
    margin-bottom: 50px;
}
.butn{
	float: none;
    background: #cfa34c;
    padding: 8px 35px;
}
label.required:after, span.required:after {
    content: ' *';
    color: #df280a;
    font-weight: normal;
    font-family: "Helvetica Neue", Verdana, Arial, sans-serif;
    font-size: 12px;
}
em{
	display:none;
}
</style> 
<div class="abt-tp"><h2>Contact US</h2></div>

<div class="cms_div">
	
	<div class="container">
	  <div class="col-md-12 no-padding">
            <form action="{{route('contactus.send')}}" id="contactForm" method="post" class="scaffold-form">
				<div class="box-body">
					@include('layouts.errors-and-messages')
				</div>
				{{ csrf_field() }}
                <div class="form-group">
                   
                    <div class="form-group">
                        <label for="name" class="required"><em>*</em>Name</label>
                        <input name="name" id="name" title="Name" value="" required class="form-control required-entry" type="text">
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 em">
                            <label for="email" class="required"><em>*</em>Email</label>
                            <input name="email" id="email" required title="Email" value="" class="form-control required-entry validate-email" type="email" autocapitalize="off" autocorrect="off" spellcheck="false">
                        </div>
                        <div class="col-md-6 col-sm-6 em">
                            <label for="telephone">Phone</label>
                            <input name="telephone" id="telephone" title="Telephone" value="" class="form-control" type="tel">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="comment" class="required"><em>*</em>Message</label>

                    <textarea name="comment" id="comment"  title="Comment" required class="form-control required-entry"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" title="Submit" class="butn"><span><span>Send</span></span></button>
                </div>
            </form>
        </div>
	
	</div>
</div>
<script src="{!! asset('resources/assets/front') !!}/js/validation.js" type="text/javascript"></script>
<script>
$('#contactForm').validate();
</script>
@endsection