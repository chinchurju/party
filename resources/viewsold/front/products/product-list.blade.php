 <div class="col-md-12">
@if(!empty($products) && !collect($products)->isEmpty())
    @foreach($products as $product)
 
	<div class="col-md-4 col-sm-4 prd_item">
		 <figure>
		 
		 
			@if(isset($product->cover))
				<img src="{{ asset("storage/products/thumb270x300_$product->cover") }}" alt="{{ $product->name }}">
			@else
				<img src="https://placehold.it/263x330" alt="{{ $product->name }}" class="img-bordered img-responsive" />
			@endif
				<form action="{{ route('cart.ajaxcart') }}" class="form-inline" method="post" id="prd_{{ $product->id }}" style="display:none">
					 {{ csrf_field() }}
					<input type="hidden" name="quantity" value="1" />
					<input type="hidden" name="product" value="{{ $product->id }}">
				</form>
				<form action="{{ route('accounts.wishlist') }}" class="form-inline" method="post" id="wisthlist_{{ $product->id }}" style="display:none">
					 {{ csrf_field() }}
					<input type="hidden" name="product_id" value="{{ $product->id }}">
				</form>
			 <figcaption>
				<a href=@if(!is_null($product->attributes->where('default', 1)->first())) "{{ route('front.get.product', str_slug($product->slug)) }} " @else "javascript:void(0);" class="add_to_cart"  data-id="prd_{{ $product->id }}" @endif  ><img src="{!! asset('resources/assets/front/') !!}/images/crt.png"></a>
				<a href=@if(!auth()->check())"{{ route('login') }}" @else"javascript:void(0);" class="wish_list_a @if(in_array($product->id,$mywishlist))  wishlist_img @endif" data-id="wisthlist_{{ $product->id }}" @endif><img src="{!! asset('resources/assets/front/') !!}/images/wish.png"></a>
			 </figcaption>
		 </figure>
		 
		 <a href="{{ route('front.get.product', str_slug($product->slug)) }}"><h4>@if((int)$product->brand_id > 0 ){{ $product->brand_name }} @else {{ $product->name }} @endif</h4>
		   @if((int)$product->brand_id > 0 )
		   <h5>{{ $product->name }}</h5>
	       @endif 
		   <!--<p>{{ config('cart.currency') }}
				@if(!is_null($product->attributes->where('default', 1)->first()))
					@if(!is_null($product->attributes->where('default', 1)->first()->sale_price))
						{{ number_format($product->attributes->where('default', 1)->first()->sale_price, 2) }}
						<p class="text text-danger">Sale!</p>
					@else
						{{ number_format($product->attributes->where('default', 1)->first()->price, 2) }}
					@endif
				@else
					{{ number_format($product->price, 2) }}
				@endif</p>-->
			<p>{{ config('cart.currency') }} {{ number_format($product->price, 2) }}</p>
		 </a>
	</div>
	
@endforeach
@endif

</div>

</div> 