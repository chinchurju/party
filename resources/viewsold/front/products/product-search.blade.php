@extends('layouts.front.app')

@section('content')

<div class="abt-tp">

<h2>Search Results</h2>

</div>	
	<div class="clo-sec">
	  <div class="container">
		 <div class="col-md-12 no-padding">
		   <div class="row">
			  @include('front.categories.sidebar-category')
			  
			  
			  <div class="col-md-9 no-padding">
			  	<div class="col-md-6 col-sm-6 text-left">
				    <div class="srt-rgt">
		                <select name="select-profession" id="select-profession" >
		                  <option value="" disabled="" selected="selected">Sort By</option>
		                  <option value="latest">Latest</option>
		                  <option value="asc">Price--Low to High</option>
		                  <option value="desc">Price--High to Low</option>
		                </select>
		            </div>
			    </div>
				<div class="col-md-6 col-sm-6 text-right">
				   {{ $links}}
			    </div>
			  
			 <div class="row"> 
				 @include('front.products.product-list', ['products' => $products])
			 </div>	 
			
				 <div class="col-md-12 col-sm-12 text-right">
				   {{ $links}}
			    </div>
			  </div>
		   </div>
		 </div>
	  </div>
	</div>

@endsection
