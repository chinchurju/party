@if(!$products->isEmpty())
	

			<table class="table table-condensed ">
              <thead>
                <tr>
                  <th align="left">Products</th>
                  <th align="center">Price</th>
                  <th align="center">Quantity</th>
                  <th align="center">Total</th>
                </tr>
              </thead>
              <tbody>
			  @foreach($cartItems as $cartItem)
                <tr>
                  <th align="left" scope="row" width="35%"> 
                    <div class="crt-sec">
                      <h4>{{ $cartItem->name }} </h4>
                      @if(isset($cartItem->options))
                        @foreach($cartItem->options as $key => $option)
                            <p >{{ $key }} : {{ $option }}</p>
                        @endforeach
                     @endif
                  </th>
                  <td width="25%" align="center"><h5>{{config('cart.currency')}} {{ number_format($cartItem->price, 2) }}</h5></td>
                  <td width="25%" align="center"><input type="number" class="qun" placeholder="1" value="{{ $cartItem->qty }}"></td>
                  <td width="25%" align="center"><h5 class="ylw-color">{{config('cart.currency')}} {{ ((float)$cartItem->price)*((int)$cartItem->qty) }}</h5></td>
                </tr>
			  @endforeach
                
              </tbody>
            </table>
			
			<div class="tpr">
            
            <ul>
              <li class="clearfix">
                <div class="tpr-lft">Price ({{count($cartItems) }} items)</div>
                <div class="tpr-rgt">{{config('cart.currency')}} {{ number_format($subtotal, 2, '.', ',') }}</div>
              </li>
              <li class="clearfix">
                <div class="tpr-lft" id="shipping_m_div">Delivery </div>
                <div class="tpr-rgt" id="shipping_m_div_val">FREE</div>
              </li>
              <li class="clearfix tpr-last">
                <div class="tpr-lft"><span>Total</span> </div>
                <div class="tpr-rgt"><span>{{config('cart.currency')}} <span id="total_span_s"> {{ number_format($total, 2, '.', ',') }}</span></span></div>
				<input type="hidden" id="total_span" value="{{$total}}" />
              </li>
            </ul>
          </div>

@endif
<script type="text/javascript">
    $(document).ready(function () {
        let courierRadioBtn = $('input[name="rate"]');
        courierRadioBtn.click(function () {
            $('#shippingFee').text($(this).data('fee'));
            let totalElement = $('span#grandTotal');
            let shippingFee = $(this).data('fee');
            let total = totalElement.data('total');
            let grandTotal = parseFloat(shippingFee) + parseFloat(total);
            totalElement.html(grandTotal.toFixed(2));
        });
    });
</script>