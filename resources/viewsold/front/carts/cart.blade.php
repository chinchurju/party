@extends('layouts.front.app')

@section('content')
<div class="abt-tp">
  <h2>My CArt</h2>
</div>
<div class="cart-sec">
  <div class="container">
    <div class="col-md-12 no-padding">
	 @if(!$cartItems->isEmpty())
      <div class="row">
        <div class="col-md-9">
          <div class="table-responsive">
			<div class="box-body">
              @include('layouts.errors-and-messages')
            </div>
            <table class="table table-condensed ">
              <thead>
                <tr>
                  <th align="left">Products</th>
                  <th align="center">Price</th>
                  <th align="center">Quantity</th>
                  <th align="center">Total</th>
                </tr>
              </thead>
              <tbody>
			   @foreach($cartItems as $cartItem)
                <tr>
                  <th align="left"scope="row" width="45%"> <div class="crt-img clearfix">
					@if(isset($cartItem->cover))
						<img src="{{ asset("storage/products/$cartItem->cover") }}" alt="{{ $cartItem->name }}" class="img-responsive">
					@else
						<img src="https://placehold.it/120x120" alt="" class="img-responsive img-thumbnail">
					@endif
						
				  </div>
                    <div class="crt-sec">
                      <h4>{{ $cartItem->name }}</h4>
                      <p>@if(isset($cartItem->options))
							@foreach($cartItem->options as $key => $option)
								<span class="label label-primary">{{ $key }} : {{ $option }}</span>
							@endforeach
						 @endif</p>
                      <!--<a href="#">MOVE TO WISHLIST</a>--><a href="javascript:void(0);" data-id="cart_del_frm_{{$cartItem->rowId}}" class="delete_btn">REMOVE</a> </div>
                  </th>
                  <td width="13%"  align="center"><h5>{{config('cart.currency')}} {{ number_format($cartItem->price, 2) }}</h5></td>
                  
				  <td  width="20%" align="center">
					 <form action="{{ route('cart.update', $cartItem->rowId) }}" class="form-inline" method="post" id="cart_update_frm_{{$cartItem->rowId}}">
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="put">
						<div class="input-group">
							<a class="minus update_qty" href="javascript:void(0)" data-id="cart_update_frm_{{$cartItem->rowId}}" > - </a>
								<input type="text" name="quantity" value="{{ $cartItem->qty }}" class="qun cart_update_frm_{{$cartItem->rowId}}"  />
							<a class="plus update_qty" href="javascript:void(0)" data-id="cart_update_frm_{{$cartItem->rowId}}"  > + </a>
						</div>
					 </form>
					 <form action="{{ route('cart.destroy', $cartItem->rowId) }}" method="post" style="display:none" id="cart_del_frm_{{$cartItem->rowId}}">
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="delete">
						<button onclick="return confirm('Are you sure?')" class="btn btn-danger"><i class="fa fa-times"></i></button>
					  </form>
				  </td>
                  
				  <td width="13%" align="center"><h5 class="ylw-color">{{config('cart.currency')}} {{ ((float)$cartItem->price)*((int)$cartItem->qty)  }} </h5></td>
                </tr>
                
				@endforeach
				
              </tbody>
            </table>
<div class="form-group">
                            <label for="image">Images</label>
                            <input type="file" name="image[]" id="image" class="form-control" multiple>
                            <small class="text-warning">You can use ctr (cmd) to select multiple images</small>
                        </div>
            <div class="ccs clearfix"> <a href="{{ route('home') }}">Continue With Shoping</a> <a href="{{ route('checkout.index') }}">Check Out</a></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="tpr">
            <h4>Summery</h4>
            <ul>
              <li class="clearfix">
                <div class="tpr-lft" style="    width: 57%;">Price ({{count($cartItems) }} items)</div>
                <div class="tpr-rgt" style="    width: 43%;">{{config('cart.currency')}} {{ number_format($subtotal, 2, '.', ',') }}</div>
              </li>
			 
              <li class="clearfix tpr-last">
                <div class="tpr-lft" style="    width: 50%;"><span>Total</span> </div>
                <div class="tpr-rgt" style="    width: 50%;"><span>{{config('cart.currency')}} {{ number_format($total, 2, '.', ',') }}</span></div>
              </li>
            </ul>
          </div>
          
        </div>
      </div>
	  @else
		<div class="row">
			<div class="col-md-12">
				<p class="alert alert-warning">No products in cart yet. <a href="{{ route('home') }}">Shop now!</a></p>
			</div>
		</div>
	  @endif
	  
    </div>
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script> 
<script>
$(document).ready(function(e){
	$('.update_qty').click(function(){
		if($(this).hasClass('minus')){
			$('.'+$(this).data('id')).val((parseInt(($('.'+$(this).data('id')).val())-1))?parseInt($('.'+$(this).data('id')).val())-1:1);
		}else{
			$('.'+$(this).data('id')).val((parseInt(($('.'+$(this).data('id')).val())+1))?parseInt($('.'+$(this).data('id')).val())+1:1);	
		}
		$('#'+$(this).data('id')).submit();
	});
	$('.delete_btn').click(function(e){
		
		e.preventDefault();
		var id = $(this).data('id');
		bootbox.confirm({
			message: "Are you sure, Do you want delete this item?",
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-success'
				},
				cancel: {
					label: 'No',
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				
				if(result){
					$('#'+id).submit();
				}
			}
		});
		
		
	});
});
</script>
@endsection



