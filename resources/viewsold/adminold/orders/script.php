<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC1TJP5byEqvvguoqDfOVihZ5yIa5rykg4&callback=initMap"></script>

<script type="text/javascript" src="http://localhost/services/resources/views/admin/js/markerwithlabel.js></script>
<script type="text/javascript">
function initMap() {
// for hide remove child button
// $("#removeButton").hide();
var latLng = new google.maps.LatLng(11.2522839, 76.1917714);
var image = 'http://zamoria.in/wp-content/themes/zamoria/images/locationpin.png';
// var isDraggable = $(document).width() > 480 ? true : false;
var map = new google.maps.Map(document.getElementById('map'), {
draggable: false,
scrollwheel: false,
zoom: 15,
center: latLng,
mapTypeId: google.maps.MapTypeId.ROADMAP
});
map.setOptions();
var marker2 = new MarkerWithLabel({
position: new google.maps.LatLng(11.2522839, 76.1917714),
raiseOnDrag: true,
icon: image,
map: map,
labelAnchor: new google.maps.Point(22, 0),
labelStyle: {opacity: 1.0}
});
var iw1 = new google.maps.InfoWindow({
//content: '<div style="height: auto;float: left;background-color: #8A4D35;"> <div height: auto;float: left;margin-bottom: 10px;"> <div style="width: auto;height: auto;float: left;color: #FFF;"><i class="fa fa-map-marker fa-2x" aria-hidden="true"></i></div> <div style="width: auto;height: auto;float: left;font-size: 17px;font-family: \'Myriad Pro\';color: #FFFFFF;margin-left: 20px;"> Zamoria Furniture<br> Ruby Building, 201<br> Tana, Vadapuram<br> Pin : 676542<br> Malappuram, Kearala </div> </div> <div style=" width:400px;height: auto;float: left;margin-bottom: 10px;"> <div style="width: auto;height: auto;float: left;color: #FFF;"><i class="fa fa-phone fa-lg" aria-hidden="true"></i></div> <div class="address_inn_right" style="width: auto;height: auto;float: left;font-size: 17px;font-family:\'Myriad Pro\';color: #FFFFFF;margin-left: 20px;">+91 9495964357 </div> </div> <div class="address_inn" style="width: 50%;height: auto;float: left;margin-bottom: 10px;"> <div style="width: auto;height: auto;float: left;color: #FFF;"><i class="fa fa-envelope fa-lg" aria-hidden="true"></i></div> <div style="width: auto;height: auto;float: left;font-size: 17px;font-family: \'Myriad Pro\';color: #FFFFFF;margin-left: 20px;">pkrubwood@zamoria.com</div> </div></div>'
content: "<div><h3> Zamoria Furniture</h3>Ruby Building, 201<br/>Tana, Vadapuram<br/>Pin : 676542<br/>Malappuram, Kearala<br/>+91 9495964357<br/>pkrubwood@zamoria.com</div>"
});
//iw1.open(map,marker2);
google.maps.event.addListener(marker2, "click", function (e) { iw1.open(map, this); });
}
initMap();
</script>