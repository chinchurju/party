@extends('layouts.admin.app')
<style>
    .chart-pie, .has-fixed-height {
        height: 400px;
    }
</style>
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{ $result['total_orders'] }}</h3>
                        <p>Total Request</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="{{ URL::to('admin/orders')}}" class="small-box-footer" data-toggle="tooltip" data-placement="bottom" title="View All Orders">View All Request <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
          <!--  <div class="col-lg-3 col-xs-6">

                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{ $result['outOfStock'] }} </h3>
                        <p>{{ trans('labels.outOfStock') }}</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="{{ URL::to('admin/productsStock')}}" class="small-box-footer" data-toggle="tooltip" data-placement="bottom" title="Out of Stock">{{ trans('labels.outOfStock') }} <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>-->
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{ $result['totalCustomers'] }}</h3>

                        <p>{{ trans('labels.customerRegistrations') }}</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="{{ URL::to('admin/customers')}}" class="small-box-footer" data-toggle="tooltip" data-placement="bottom" title="View All Customers">{{ trans('labels.viewAllCustomers') }}  <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{ $result['totalProducts'] }}</h3>

                        <p>Services</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="{{ URL::to('admin/products')}}" class="small-box-footer" data-toggle="tooltip" data-placement="bottom" title="Out of Products">View All Services <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>

     <!--   <div class="row">
            <div class="col-md-12">
                <h1>Orders</h1>
            </div>
        </div>
            <div class="row">
            <div class="col-md-12">
            <div class="has-fixed-height" id="simple" style="height:70%"></div>
            </div>
        </div>-->
        <!-- /.row -->
    </section>
    <!-- /.content -->

    <script type="text/javascript">
        /*$ = jQuery;
        $(document).ready(function () {
            alert('sssssssssssssssssss');
        });
        */
    </script>
@endsection