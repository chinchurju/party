-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 08, 2019 at 08:01 AM
-- Server version: 5.7.21
-- PHP Version: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `partyapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
CREATE TABLE IF NOT EXISTS `addresses` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `country_id` int(10) UNSIGNED DEFAULT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `phone` int(15) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `location_latitude` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_longitude` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `addresses_country_id_index` (`country_id`),
  KEY `addresses_customer_id_index` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `alias`, `address_1`, `address_2`, `zip`, `state_code`, `city`, `province_id`, `country_id`, `customer_id`, `status`, `phone`, `deleted_at`, `created_at`, `updated_at`, `location_latitude`, `location_longitude`, `is_default`) VALUES
(11, 'zx', 'test', 'test', '123456', '', 'fsdf', 5, 174, 15, 1, NULL, '2018-11-01 22:56:56', '2018-11-01 21:18:18', '2018-11-01 22:56:56', NULL, NULL, NULL),
(12, 'SANIL', 'test addr', 'test addr 2', '1256', NULL, NULL, NULL, 174, 15, 1, NULL, NULL, '2018-11-01 22:56:47', '2019-02-07 00:31:34', '10.850516', '76.271080', 0),
(13, 'Home', 'test addr', 'test', '123456', NULL, NULL, NULL, 174, 15, 1, NULL, NULL, '2018-11-13 00:59:59', '2019-02-07 00:31:34', NULL, NULL, 0),
(14, 'Home1', 'ssss', 'sss', '123456789', NULL, NULL, 7, 174, 15, 1, NULL, NULL, '2018-11-18 20:27:01', '2019-02-07 00:31:34', NULL, NULL, 0),
(19, 'Billing', 'dfgdgdgdfg', 'dfgdfg', NULL, NULL, NULL, 1, 174, 15, 1, NULL, '2018-11-26 02:35:57', '2018-11-19 01:41:36', '2018-11-26 02:35:57', NULL, NULL, NULL),
(20, 'Billing', 'asdasdsd', 'asdasd', NULL, NULL, NULL, 1, 174, 15, 1, NULL, NULL, '2018-11-19 01:42:08', '2019-02-07 00:31:34', NULL, NULL, 0),
(21, 'Billing', 'dfgdfgdg', 'dfgdfg', NULL, NULL, NULL, 1, 174, 15, 1, NULL, NULL, '2018-11-19 01:44:54', '2019-02-07 00:31:34', NULL, NULL, 0),
(22, 'Billing', 'dfffffffffg', 'dgdfgdfg', '123456', NULL, NULL, 4, 174, 15, 1, NULL, NULL, '2018-11-19 01:54:52', '2019-02-07 00:31:34', NULL, NULL, 0),
(23, 'Billing', 'sdfsdf', 'sdfsdf', '123456', NULL, NULL, 3, 174, 15, 1, NULL, NULL, '2018-11-19 01:57:02', '2019-02-07 00:31:34', NULL, NULL, 0),
(24, 'Billing', 'sdfsdfsdasdasdasdasd', 'sdfsdf', '123456', NULL, NULL, 3, 174, 15, 1, NULL, '2018-11-26 02:35:14', '2018-11-19 01:57:35', '2018-11-26 02:35:14', NULL, NULL, NULL),
(25, 'Billing', 'sdfsdfsdasdasdasdasdddasdasd', 'sdfsdf', '123456', NULL, NULL, 3, 174, 15, 1, NULL, NULL, '2018-11-19 01:57:58', '2019-02-07 00:31:34', NULL, NULL, 0),
(26, 'Billing', 'ghjghjgj', 'ghjghj', '1256', NULL, NULL, 5, 174, 15, 1, NULL, NULL, '2018-11-19 02:02:17', '2019-02-07 00:31:34', NULL, NULL, 0),
(27, 'Billing', 'fghfghfg', 'hfghfgh', '123456', NULL, NULL, 5, 174, 15, 1, NULL, '2018-11-26 02:37:04', '2018-11-19 02:05:19', '2018-11-26 02:37:04', NULL, NULL, NULL),
(28, 'test', 'xdvdcgd', 'fbcf', '123456', NULL, 'sdsd', 1, 174, 27, 1, 123456789, NULL, '2018-12-11 01:41:55', '2018-12-11 01:41:55', '10.850516', '76.271080', NULL),
(29, 'opl', 'oopl', 'oopl', NULL, NULL, NULL, 7, 174, 28, 1, NULL, NULL, '2018-12-14 05:26:57', '2018-12-14 05:26:57', NULL, NULL, NULL),
(30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-01-04 01:18:37', '2019-01-04 01:18:37', '9.9312', '76.2673', NULL),
(31, NULL, 'test address', NULL, NULL, NULL, NULL, NULL, NULL, 42, 1, NULL, NULL, '2019-02-14 23:45:21', '2019-02-14 23:45:21', '10.850516', '76.271080', NULL),
(32, NULL, 'test address', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, '2019-10-12 06:54:01', '2019-10-12 06:54:01', '10.850516', '76.271080', NULL),
(33, NULL, 'cr address', NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, NULL, NULL, '2019-10-21 09:28:05', '2019-10-21 09:28:05', '342234', '345346', NULL),
(34, NULL, 'testtt', NULL, NULL, NULL, NULL, NULL, NULL, 4, 1, NULL, NULL, '2019-11-08 03:43:41', '2019-11-08 03:43:41', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

DROP TABLE IF EXISTS `attributes`;
CREATE TABLE IF NOT EXISTS `attributes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_searchable` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_configurable` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `attributes_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `name`, `is_searchable`, `is_configurable`, `created_at`, `updated_at`) VALUES
(1, 'color', '1', '1', '2019-10-10 03:05:28', '2019-10-10 03:05:28'),
(2, 'model', '0', '0', '2019-10-18 07:07:21', '2019-10-18 07:07:21');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_values`
--

DROP TABLE IF EXISTS `attribute_values`;
CREATE TABLE IF NOT EXISTS `attribute_values` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `attribute_values_attribute_id_foreign` (`attribute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `attribute_values`
--

INSERT INTO `attribute_values` (`id`, `value`, `attribute_id`, `created_at`, `updated_at`) VALUES
(1, 'red', 1, '2019-10-10 03:05:39', '2019-10-10 03:05:39'),
(2, 'whyte', 1, '2019-10-10 03:05:48', '2019-10-10 03:05:48'),
(3, 'blue', 1, '2019-10-16 04:44:00', '2019-10-16 04:44:00');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_value_product_attribute`
--

DROP TABLE IF EXISTS `attribute_value_product_attribute`;
CREATE TABLE IF NOT EXISTS `attribute_value_product_attribute` (
  `attribute_value_id` int(10) UNSIGNED NOT NULL,
  `product_attribute_id` int(10) UNSIGNED NOT NULL,
  KEY `attribute_value_product_attribute_attribute_value_id_foreign` (`attribute_value_id`),
  KEY `attribute_value_product_attribute_product_attribute_id_foreign` (`product_attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `attribute_value_product_attribute`
--

INSERT INTO `attribute_value_product_attribute` (`attribute_value_id`, `product_attribute_id`) VALUES
(2, 1),
(1, 4),
(1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
CREATE TABLE IF NOT EXISTS `banners` (
  `banners_id` int(11) NOT NULL AUTO_INCREMENT,
  `banners_title` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banners_desc` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banners_link_text` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banners_link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banners_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banners_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `banners_group` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banners_html_text` mediumtext COLLATE utf8_unicode_ci,
  `expires_impressions` int(7) DEFAULT '0',
  `expires_date` datetime DEFAULT NULL,
  `date_scheduled` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `date_status_change` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `type` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'home1',
  PRIMARY KEY (`banners_id`),
  KEY `idx_banners_group` (`banners_group`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`banners_id`, `banners_title`, `banners_desc`, `banners_link_text`, `banners_link`, `banners_url`, `banners_image`, `banners_group`, `banners_html_text`, `expires_impressions`, `expires_date`, `date_scheduled`, `date_added`, `date_status_change`, `status`, `type`) VALUES
(1, 'banner1', 'One Platform to help you build, manage and grow your events', NULL, NULL, '', 'resources/assets/images/banner_images/1570604183.iPhone X-XS – 9.png', NULL, NULL, 0, '2021-08-31 00:00:00', NULL, '2019-10-09 09:56:23', NULL, 1, 'home1'),
(2, 'banner2', 'One Platform to help you build, manage and grow your events', NULL, NULL, '', 'resources/assets/images/banner_images/1570604340.iPhone X-XS – 8.png', NULL, NULL, 0, '2021-10-28 00:00:00', NULL, '2019-10-09 09:59:00', NULL, 1, 'home1'),
(3, 'banner3', 'One Platform to help you build, manage and grow your events', NULL, NULL, '', 'resources/assets/images/banner_images/1570604374.iPhone X-XS – 10.png', NULL, NULL, 0, '2021-10-31 00:00:00', NULL, '2019-10-09 09:59:34', NULL, 1, 'home1'),
(4, 'banner4', 'One Platform to help you build, manage and grow your events', NULL, NULL, '', 'resources/assets/images/banner_images/1570604423.iPhone X-XS – 11.png', NULL, NULL, 0, '2021-09-30 00:00:00', NULL, '2019-10-09 10:00:23', NULL, 1, 'home1');

-- --------------------------------------------------------

--
-- Table structure for table `banners_history`
--

DROP TABLE IF EXISTS `banners_history`;
CREATE TABLE IF NOT EXISTS `banners_history` (
  `banners_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `banners_id` int(11) NOT NULL,
  `banners_shown` int(5) NOT NULL DEFAULT '0',
  `banners_clicked` int(5) NOT NULL DEFAULT '0',
  `banners_history_date` datetime NOT NULL,
  PRIMARY KEY (`banners_history_id`),
  KEY `idx_banners_history_banners_id` (`banners_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blocks`
--

DROP TABLE IF EXISTS `blocks`;
CREATE TABLE IF NOT EXISTS `blocks` (
  `block_id` int(11) NOT NULL AUTO_INCREMENT,
  `block_title` varchar(250) DEFAULT NULL,
  `block_content` text,
  `display_order` int(11) DEFAULT '1',
  `status` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`block_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blocks`
--

INSERT INTO `blocks` (`block_id`, `block_title`, `block_content`, `display_order`, `status`) VALUES
(1, '<span> MADE THE HARD WAY </span><br> FEATURED CATEGORIES', '<div class=\"col-md-4 col-sm-4\">\r\n				 <figure><a href=\"#\"><img src=\"http://192.168.0.106/merwad/resources/views/admin/plugins//vendor/ckeditor/plugins/imageuploader/uploads/210fa2314.jpg\" class=\"img-responsive\"></a></figure>\r\n				 <figure><a href=\"#\"><img src=\"http://192.168.0.106/merwad/resources/views/admin/plugins//vendor/ckeditor/plugins/imageuploader/uploads/21198de44.jpg\" class=\"img-responsive\"></a></figure>\r\n			   </div>\r\n			   <div class=\"col-md-4 col-sm-4\">\r\n				 <figure><a href=\"#\"><img src=\"http://192.168.0.106/merwad/resources/views/admin/plugins//vendor/ckeditor/plugins/imageuploader/uploads/212184777.jpg\" class=\"img-responsive\"></a></figure>\r\n			   </div>\r\n			   <div class=\"col-md-4 col-sm-4\">\r\n				 <figure><a href=\"#\"><img src=\"http://192.168.0.106/merwad/resources/views/admin/plugins//vendor/ckeditor/plugins/imageuploader/uploads/213bd8ee5.jpg\" class=\"img-responsive\"></a></figure>\r\n				 <figure><a href=\"#\"><img src=\"http://192.168.0.106/merwad/resources/views/admin/plugins//vendor/ckeditor/plugins/imageuploader/uploads/21481914b.jpg\" class=\"img-responsive\"></a></figure>\r\n			   </div>', 1, '1'),
(2, 'ASASAS', '<img alt=\"\" src=\"http://localhost/merwad/resources/views/admin/plugins//vendor/ckeditor/plugins/imageuploader/uploads/209b98f55.jpg\" style=\"height:332px; width:370px\" />', 1, '0');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'One Time Payment', '2018-10-17 04:31:27', '2018-10-17 04:31:27'),
(2, 'After payment', '2018-11-15 05:08:02', '2018-11-15 05:08:02');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

DROP TABLE IF EXISTS `carts`;
CREATE TABLE IF NOT EXISTS `carts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_attr_id` int(11) DEFAULT NULL,
  `option` varchar(250) DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `sub_total` decimal(10,2) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `customer_id`, `product_id`, `product_attr_id`, `option`, `qty`, `price`, `sub_total`, `updated_at`, `created_at`) VALUES
(2, 2, 16, NULL, NULL, 2, '1500.00', '3000.00', '2019-10-24 15:03:02', '2019-10-21 15:03:07');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ar` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `description_ar` longtext COLLATE utf8mb4_unicode_ci,
  `cover` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `display_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `name_ar`, `slug`, `description`, `description_ar`, `cover`, `status`, `parent_id`, `created_at`, `updated_at`, `display_order`) VALUES
(1, 'Uncategorized', '', 'uncategorized', 'Doloremque sint praesentium et voluptatem. Quas fugit perferendis sit consectetur aliquam voluptatibus expedita vitae. Exercitationem veritatis omnis suscipit dolores quis qui.', NULL, 'categories/HHm4zs0P87hsWsI23luAqNLTDglea2xAtBvr3HQi.png', 1, 0, '2018-10-17 01:31:24', '2018-10-17 01:31:24', 0),
(2, 'Event Bookings', 'Event Bookings', 'event-bookings', NULL, NULL, 'categories/1570608447.Image 64.png', 1, 1, '2019-10-09 05:37:27', '2019-10-09 05:37:27', 0),
(3, 'Party Supplies', 'Party Supplies', 'party-supplies', NULL, NULL, 'categories/1570609401.p7fxgY3ujFXJz4Uy7TVsV8FaK.png', 1, 1, '2019-10-09 05:53:21', '2019-10-09 05:53:21', 0),
(4, 'Outdoor Set up', 'Outdoor Set up', 'outdoor-set-up', NULL, NULL, 'categories/1570609438.hiclipart.com-id_iihca.png', 1, 1, '2019-10-09 05:53:58', '2019-10-09 05:53:58', 0),
(5, 'Catering & Hospitality', 'Catering & Hospitality', 'catering-hospitality', NULL, NULL, 'categories/1570609484.4sZpzCZbcPBsCg98q9T2NyfvC.png', 1, 1, '2019-10-09 05:54:44', '2019-10-09 05:54:44', 0),
(6, 'Flowers & Chocolates', 'Flowers & Chocolates', 'flowers-chocolates', NULL, NULL, 'categories/1570621347.Group 218.png', 1, 1, '2019-10-09 05:55:24', '2019-10-09 09:12:27', 0),
(7, 'Table & Chairs', 'Table & Chairs', 'table-chairs', NULL, NULL, 'categories/1570609758.Mask Group 18.png', 1, 3, '2019-10-09 05:59:18', '2019-10-09 05:59:18', 0),
(8, 'Sound System', 'Sound System', 'sound-system', NULL, NULL, 'categories/1570609808.Mask Group 19.png', 1, 3, '2019-10-09 06:00:08', '2019-10-09 06:00:08', 0),
(9, 'Flowers&chocolates', 'Flowers&chocolates', 'flowerschocolates', NULL, NULL, 'categories/1570609941.Mask Group 20.png', 1, 3, '2019-10-09 06:02:21', '2019-10-09 06:02:46', 0),
(10, 'Dance Floor', 'Dance Floor', 'dance-floor', NULL, NULL, 'categories/1570610016.Mask Group 21.png', 1, 3, '2019-10-09 06:03:36', '2019-10-09 06:03:36', 0),
(11, 'Light', 'Light', 'light', NULL, NULL, 'categories/1570610048.Mask Group 22.png', 1, 3, '2019-10-09 06:04:08', '2019-10-09 06:04:08', 0),
(12, 'LED Alphabets', 'LED Alphabets', 'led-alphabets', NULL, NULL, 'categories/1570610080.Mask Group 23.png', 1, 3, '2019-10-09 06:04:40', '2019-10-09 06:04:40', 0),
(13, 'W Hotels', 'W Hotels', 'w-hotels', 'But I must explain to you how all this mistaken idea of denouncing.<br />\r\nStarting From 1000 qr', NULL, 'categories/1570621267.Image 148.png', 1, 5, '2019-10-09 09:11:07', '2019-10-09 09:11:07', 0),
(14, 'Hilton Doha', 'Hilton Doha', 'hilton-doha', NULL, NULL, 'categories/1570623782.Image 150.png', 1, 5, '2019-10-09 09:53:02', '2019-10-09 09:53:02', 0),
(15, 'Birthday', 'Birthday', 'birthday', NULL, NULL, NULL, 1, 2, '2019-10-10 09:47:41', '2019-10-10 09:47:41', 0),
(16, 'Marrage', 'Marrage', 'marrage', NULL, NULL, NULL, 1, 2, '2019-10-10 09:48:17', '2019-10-10 09:48:17', 0);

-- --------------------------------------------------------

--
-- Table structure for table `category_product`
--

DROP TABLE IF EXISTS `category_product`;
CREATE TABLE IF NOT EXISTS `category_product` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_product_category_id_index` (`category_id`),
  KEY `category_product_product_id_index` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `category_product`
--

INSERT INTO `category_product` (`id`, `category_id`, `product_id`) VALUES
(1, 3, 1),
(2, 7, 1),
(3, 3, 2),
(4, 7, 2),
(5, 3, 3),
(6, 8, 3),
(7, 3, 4),
(8, 8, 4),
(9, 3, 5),
(11, 3, 6),
(12, 9, 6),
(13, 10, 5),
(14, 3, 7),
(15, 11, 7),
(16, 3, 8),
(17, 12, 8),
(18, 3, 9),
(19, 9, 9),
(20, 3, 10),
(21, 10, 10),
(22, 2, 11),
(23, 2, 12),
(24, 4, 13),
(25, 4, 14),
(27, 13, 15),
(29, 13, 16),
(31, 14, 17),
(33, 14, 18),
(34, 15, 11),
(35, 16, 12);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
CREATE TABLE IF NOT EXISTS `cities` (
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province_id` int(10) UNSIGNED DEFAULT NULL,
  KEY `cities_province_id_foreign` (`province_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email_address` varchar(15) NOT NULL,
  `phone` int(15) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iso` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iso3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `numcode` int(11) DEFAULT NULL,
  `phonecode` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `countries_name_unique` (`name`),
  UNIQUE KEY `countries_iso_unique` (`iso`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `iso`, `iso3`, `numcode`, `phonecode`, `status`, `created_at`, `updated_at`) VALUES
(1, 'AFGHANISTAN', 'AF', 'AFG', 4, 93, 0, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(2, 'ALBANIA', 'AL', 'ALB', 8, 355, 0, '2018-10-17 04:31:25', '2019-01-30 03:10:23'),
(3, 'ALGERIA', 'DZ', 'DZA', 12, 213, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(4, 'AMERICAN SAMOA', 'AS', 'ASM', 16, 1684, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(5, 'ANDORRA', 'AD', 'AND', 20, 376, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(6, 'ANGOLA', 'AO', 'AGO', 24, 244, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(7, 'ANGUILLA', 'AI', 'AIA', 660, 1264, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(8, 'ANTARCTICA', 'AQ', NULL, NULL, 0, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(9, 'ANTIGUA AND BARBUDA', 'AG', 'ATG', 28, 1268, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(10, 'ARGENTINA', 'AR', 'ARG', 32, 54, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(11, 'ARMENIA', 'AM', 'ARM', 51, 374, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(12, 'ARUBA', 'AW', 'ABW', 533, 297, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(13, 'AUSTRALIA', 'AU', 'AUS', 36, 61, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(14, 'AUSTRIA', 'AT', 'AUT', 40, 43, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(15, 'AZERBAIJAN', 'AZ', 'AZE', 31, 994, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(16, 'BAHAMAS', 'BS', 'BHS', 44, 1242, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(17, 'BAHRAIN', 'BH', 'BHR', 48, 973, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(18, 'BANGLADESH', 'BD', 'BGD', 50, 880, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(19, 'BARBADOS', 'BB', 'BRB', 52, 1246, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(20, 'BELARUS', 'BY', 'BLR', 112, 375, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(21, 'BELGIUM', 'BE', 'BEL', 56, 32, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(22, 'BELIZE', 'BZ', 'BLZ', 84, 501, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(23, 'BENIN', 'BJ', 'BEN', 204, 229, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(24, 'BERMUDA', 'BM', 'BMU', 60, 1441, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(25, 'BHUTAN', 'BT', 'BTN', 64, 975, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(26, 'BOLIVIA', 'BO', 'BOL', 68, 591, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(27, 'BOSNIA AND HERZEGOVINA', 'BA', 'BIH', 70, 387, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(28, 'BOTSWANA', 'BW', 'BWA', 72, 267, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(29, 'BOUVET ISLAND', 'BV', NULL, NULL, 0, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(30, 'BRAZIL', 'BR', 'BRA', 76, 55, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(31, 'BRITISH INDIAN OCEAN TERRITORY', 'IO', NULL, NULL, 246, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(32, 'BRUNEI DARUSSALAM', 'BN', 'BRN', 96, 673, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(33, 'BULGARIA', 'BG', 'BGR', 100, 359, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(34, 'BURKINA FASO', 'BF', 'BFA', 854, 226, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(35, 'BURUNDI', 'BI', 'BDI', 108, 257, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(36, 'CAMBODIA', 'KH', 'KHM', 116, 855, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(37, 'CAMEROON', 'CM', 'CMR', 120, 237, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(38, 'CANADA', 'CA', 'CAN', 124, 1, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(39, 'CAPE VERDE', 'CV', 'CPV', 132, 238, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(40, 'CAYMAN ISLANDS', 'KY', 'CYM', 136, 1345, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(41, 'CENTRAL AFRICAN REPUBLIC', 'CF', 'CAF', 140, 236, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(42, 'CHAD', 'TD', 'TCD', 148, 235, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(43, 'CHILE', 'CL', 'CHL', 152, 56, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(44, 'CHINA', 'CN', 'CHN', 156, 86, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(45, 'CHRISTMAS ISLAND', 'CX', NULL, NULL, 61, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(46, 'COCOS (KEELING) ISLANDS', 'CC', NULL, NULL, 672, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(47, 'COLOMBIA', 'CO', 'COL', 170, 57, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(48, 'COMOROS', 'KM', 'COM', 174, 269, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(49, 'CONGO', 'CG', 'COG', 178, 242, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(50, 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'CD', 'COD', 180, 242, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(51, 'COOK ISLANDS', 'CK', 'COK', 184, 682, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(52, 'COSTA RICA', 'CR', 'CRI', 188, 506, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(53, 'COTE D\'IVOIRE', 'CI', 'CIV', 384, 225, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(54, 'CROATIA', 'HR', 'HRV', 191, 385, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(55, 'CUBA', 'CU', 'CUB', 192, 53, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(56, 'CYPRUS', 'CY', 'CYP', 196, 357, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(57, 'CZECH REPUBLIC', 'CZ', 'CZE', 203, 420, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(58, 'DENMARK', 'DK', 'DNK', 208, 45, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(59, 'DJIBOUTI', 'DJ', 'DJI', 262, 253, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(60, 'DOMINICA', 'DM', 'DMA', 212, 1767, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(61, 'DOMINICAN REPUBLIC', 'DO', 'DOM', 214, 1809, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(62, 'ECUADOR', 'EC', 'ECU', 218, 593, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(63, 'EGYPT', 'EG', 'EGY', 818, 20, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(64, 'EL SALVADOR', 'SV', 'SLV', 222, 503, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(65, 'EQUATORIAL GUINEA', 'GQ', 'GNQ', 226, 240, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(66, 'ERITREA', 'ER', 'ERI', 232, 291, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(67, 'ESTONIA', 'EE', 'EST', 233, 372, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(68, 'ETHIOPIA', 'ET', 'ETH', 231, 251, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(69, 'FALKLAND ISLANDS (MALVINAS)', 'FK', 'FLK', 238, 500, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(70, 'FAROE ISLANDS', 'FO', 'FRO', 234, 298, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(71, 'FIJI', 'FJ', 'FJI', 242, 679, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(72, 'FINLAND', 'FI', 'FIN', 246, 358, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(73, 'FRANCE', 'FR', 'FRA', 250, 33, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(74, 'FRENCH GUIANA', 'GF', 'GUF', 254, 594, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(75, 'FRENCH POLYNESIA', 'PF', 'PYF', 258, 689, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(76, 'FRENCH SOUTHERN TERRITORIES', 'TF', NULL, NULL, 0, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(77, 'GABON', 'GA', 'GAB', 266, 241, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(78, 'GAMBIA', 'GM', 'GMB', 270, 220, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(79, 'GEORGIA', 'GE', 'GEO', 268, 995, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(80, 'GERMANY', 'DE', 'DEU', 276, 49, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(81, 'GHANA', 'GH', 'GHA', 288, 233, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(82, 'GIBRALTAR', 'GI', 'GIB', 292, 350, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(83, 'GREECE', 'GR', 'GRC', 300, 30, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(84, 'GREENLAND', 'GL', 'GRL', 304, 299, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(85, 'GRENADA', 'GD', 'GRD', 308, 1473, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(86, 'GUADELOUPE', 'GP', 'GLP', 312, 590, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(87, 'GUAM', 'GU', 'GUM', 316, 1671, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(88, 'GUATEMALA', 'GT', 'GTM', 320, 502, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(89, 'GUINEA', 'GN', 'GIN', 324, 224, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(90, 'GUINEA-BISSAU', 'GW', 'GNB', 624, 245, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(91, 'GUYANA', 'GY', 'GUY', 328, 592, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(92, 'HAITI', 'HT', 'HTI', 332, 509, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(93, 'HEARD ISLAND AND MCDONALD ISLANDS', 'HM', NULL, NULL, 0, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(94, 'HOLY SEE (VATICAN CITY STATE)', 'VA', 'VAT', 336, 39, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(95, 'HONDURAS', 'HN', 'HND', 340, 504, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(96, 'HONG KONG', 'HK', 'HKG', 344, 852, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(97, 'HUNGARY', 'HU', 'HUN', 348, 36, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(98, 'ICELAND', 'IS', 'ISL', 352, 354, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(99, 'INDIA', 'IN', 'IND', 356, 91, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(100, 'INDONESIA', 'ID', 'IDN', 360, 62, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(101, 'IRAN, ISLAMIC REPUBLIC OF', 'IR', 'IRN', 364, 98, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(102, 'IRAQ', 'IQ', 'IRQ', 368, 964, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(103, 'IRELAND', 'IE', 'IRL', 372, 353, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(104, 'ISRAEL', 'IL', 'ISR', 376, 972, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(105, 'ITALY', 'IT', 'ITA', 380, 39, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(106, 'JAMAICA', 'JM', 'JAM', 388, 1876, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(107, 'JAPAN', 'JP', 'JPN', 392, 81, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(108, 'JORDAN', 'JO', 'JOR', 400, 962, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(109, 'KAZAKHSTAN', 'KZ', 'KAZ', 398, 7, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(110, 'KENYA', 'KE', 'KEN', 404, 254, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(111, 'KIRIBATI', 'KI', 'KIR', 296, 686, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(112, 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'KP', 'PRK', 408, 850, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(113, 'KOREA, REPUBLIC OF', 'KR', 'KOR', 410, 82, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(114, 'KUWAIT', 'KW', 'KWT', 414, 965, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(115, 'KYRGYZSTAN', 'KG', 'KGZ', 417, 996, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(116, 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'LA', 'LAO', 418, 856, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(117, 'LATVIA', 'LV', 'LVA', 428, 371, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(118, 'LEBANON', 'LB', 'LBN', 422, 961, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(119, 'LESOTHO', 'LS', 'LSO', 426, 266, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(120, 'LIBERIA', 'LR', 'LBR', 430, 231, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(121, 'LIBYAN ARAB JAMAHIRIYA', 'LY', 'LBY', 434, 218, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(122, 'LIECHTENSTEIN', 'LI', 'LIE', 438, 423, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(123, 'LITHUANIA', 'LT', 'LTU', 440, 370, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(124, 'LUXEMBOURG', 'LU', 'LUX', 442, 352, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(125, 'MACAO', 'MO', 'MAC', 446, 853, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(126, 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'MK', 'MKD', 807, 389, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(127, 'MADAGASCAR', 'MG', 'MDG', 450, 261, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(128, 'MALAWI', 'MW', 'MWI', 454, 265, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(129, 'MALAYSIA', 'MY', 'MYS', 458, 60, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(130, 'MALDIVES', 'MV', 'MDV', 462, 960, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(131, 'MALI', 'ML', 'MLI', 466, 223, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(132, 'MALTA', 'MT', 'MLT', 470, 356, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(133, 'MARSHALL ISLANDS', 'MH', 'MHL', 584, 692, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(134, 'MARTINIQUE', 'MQ', 'MTQ', 474, 596, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(135, 'MAURITANIA', 'MR', 'MRT', 478, 222, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(136, 'MAURITIUS', 'MU', 'MUS', 480, 230, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(137, 'MAYOTTE', 'YT', NULL, NULL, 269, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(138, 'MEXICO', 'MX', 'MEX', 484, 52, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(139, 'MICRONESIA, FEDERATED STATES OF', 'FM', 'FSM', 583, 691, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(140, 'MOLDOVA, REPUBLIC OF', 'MD', 'MDA', 498, 373, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(141, 'MONACO', 'MC', 'MCO', 492, 377, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(142, 'MONGOLIA', 'MN', 'MNG', 496, 976, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(143, 'MONTSERRAT', 'MS', 'MSR', 500, 1664, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(144, 'MOROCCO', 'MA', 'MAR', 504, 212, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(145, 'MOZAMBIQUE', 'MZ', 'MOZ', 508, 258, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(146, 'MYANMAR', 'MM', 'MMR', 104, 95, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(147, 'NAMIBIA', 'NA', 'NAM', 516, 264, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(148, 'NAURU', 'NR', 'NRU', 520, 674, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(149, 'NEPAL', 'NP', 'NPL', 524, 977, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(150, 'NETHERLANDS', 'NL', 'NLD', 528, 31, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(151, 'NETHERLANDS ANTILLES', 'AN', 'ANT', 530, 599, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(152, 'NEW CALEDONIA', 'NC', 'NCL', 540, 687, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(153, 'NEW ZEALAND', 'NZ', 'NZL', 554, 64, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(154, 'NICARAGUA', 'NI', 'NIC', 558, 505, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(155, 'NIGER', 'NE', 'NER', 562, 227, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(156, 'NIGERIA', 'NG', 'NGA', 566, 234, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(157, 'NIUE', 'NU', 'NIU', 570, 683, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(158, 'NORFOLK ISLAND', 'NF', 'NFK', 574, 672, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(159, 'NORTHERN MARIANA ISLANDS', 'MP', 'MNP', 580, 1670, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(160, 'NORWAY', 'NO', 'NOR', 578, 47, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(161, 'OMAN', 'OM', 'OMN', 512, 968, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(162, 'PAKISTAN', 'PK', 'PAK', 586, 92, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(163, 'PALAU', 'PW', 'PLW', 585, 680, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(164, 'PALESTINIAN TERRITORY, OCCUPIED', 'PS', NULL, NULL, 970, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(165, 'PANAMA', 'PA', 'PAN', 591, 507, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(166, 'PAPUA NEW GUINEA', 'PG', 'PNG', 598, 675, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(167, 'PARAGUAY', 'PY', 'PRY', 600, 595, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(168, 'PERU', 'PE', 'PER', 604, 51, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(169, 'PHILIPPINES', 'PH', 'PHL', 608, 63, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(170, 'PITCAIRN', 'PN', 'PCN', 612, 0, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(171, 'POLAND', 'PL', 'POL', 616, 48, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(172, 'PORTUGAL', 'PT', 'PRT', 620, 351, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(173, 'PUERTO RICO', 'PR', 'PRI', 630, 1787, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(174, 'QATAR', 'QA', 'QAT', 634, 974, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(175, 'REUNION', 'RE', 'REU', 638, 262, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(176, 'ROMANIA', 'RO', 'ROM', 642, 40, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(177, 'RUSSIAN FEDERATION', 'RU', 'RUS', 643, 70, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(178, 'RWANDA', 'RW', 'RWA', 646, 250, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(179, 'SAINT HELENA', 'SH', 'SHN', 654, 290, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(180, 'SAINT KITTS AND NEVIS', 'KN', 'KNA', 659, 1869, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(181, 'SAINT LUCIA', 'LC', 'LCA', 662, 1758, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(182, 'SAINT PIERRE AND MIQUELON', 'PM', 'SPM', 666, 508, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(183, 'SAINT VINCENT AND THE GRENADINES', 'VC', 'VCT', 670, 1784, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(184, 'SAMOA', 'WS', 'WSM', 882, 684, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(185, 'SAN MARINO', 'SM', 'SMR', 674, 378, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(186, 'SAO TOME AND PRINCIPE', 'ST', 'STP', 678, 239, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(187, 'SAUDI ARABIA', 'SA', 'SAU', 682, 966, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(188, 'SENEGAL', 'SN', 'SEN', 686, 221, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(189, 'SERBIA AND MONTENEGRO', 'CS', NULL, NULL, 381, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(190, 'SEYCHELLES', 'SC', 'SYC', 690, 248, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(191, 'SIERRA LEONE', 'SL', 'SLE', 694, 232, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(192, 'SINGAPORE', 'SG', 'SGP', 702, 65, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(193, 'SLOVAKIA', 'SK', 'SVK', 703, 421, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(194, 'SLOVENIA', 'SI', 'SVN', 705, 386, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(195, 'SOLOMON ISLANDS', 'SB', 'SLB', 90, 677, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(196, 'SOMALIA', 'SO', 'SOM', 706, 252, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(197, 'SOUTH AFRICA', 'ZA', 'ZAF', 710, 27, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(198, 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'GS', NULL, NULL, 0, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(199, 'SPAIN', 'ES', 'ESP', 724, 34, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(200, 'SRI LANKA', 'LK', 'LKA', 144, 94, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(201, 'SUDAN', 'SD', 'SDN', 736, 249, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(202, 'SURINAME', 'SR', 'SUR', 740, 597, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(203, 'SVALBARD AND JAN MAYEN', 'SJ', 'SJM', 744, 47, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(204, 'SWAZILAND', 'SZ', 'SWZ', 748, 268, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(205, 'SWEDEN', 'SE', 'SWE', 752, 46, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(206, 'SWITZERLAND', 'CH', 'CHE', 756, 41, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(207, 'SYRIAN ARAB REPUBLIC', 'SY', 'SYR', 760, 963, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(208, 'TAIWAN, PROVINCE OF CHINA', 'TW', 'TWN', 158, 886, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(209, 'TAJIKISTAN', 'TJ', 'TJK', 762, 992, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(210, 'TANZANIA, UNITED REPUBLIC OF', 'TZ', 'TZA', 834, 255, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(211, 'THAILAND', 'TH', 'THA', 764, 66, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(212, 'TIMOR-LESTE', 'TL', NULL, NULL, 670, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(213, 'TOGO', 'TG', 'TGO', 768, 228, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(214, 'TOKELAU', 'TK', 'TKL', 772, 690, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(215, 'TONGA', 'TO', 'TON', 776, 676, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(216, 'TRINIDAD AND TOBAGO', 'TT', 'TTO', 780, 1868, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(217, 'TUNISIA', 'TN', 'TUN', 788, 216, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(218, 'TURKEY', 'TR', 'TUR', 792, 90, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(219, 'TURKMENISTAN', 'TM', 'TKM', 795, 7370, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(220, 'TURKS AND CAICOS ISLANDS', 'TC', 'TCA', 796, 1649, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(221, 'TUVALU', 'TV', 'TUV', 798, 688, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(222, 'UGANDA', 'UG', 'UGA', 800, 256, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(223, 'UKRAINE', 'UA', 'UKR', 804, 380, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(224, 'UNITED ARAB EMIRATES', 'AE', 'ARE', 784, 971, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(225, 'UNITED KINGDOM', 'GB', 'GBR', 826, 44, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(226, 'UNITED STATES OF AMERICA', 'US', 'USA', 840, 1, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(227, 'UNITED STATES MINOR OUTLYING ISLANDS', 'UM', NULL, NULL, 1, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(228, 'URUGUAY', 'UY', 'URY', 858, 598, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(229, 'UZBEKISTAN', 'UZ', 'UZB', 860, 998, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(230, 'VANUATU', 'VU', 'VUT', 548, 678, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(231, 'VENEZUELA', 'VE', 'VEN', 862, 58, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(232, 'VIET NAM', 'VN', 'VNM', 704, 84, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(233, 'VIRGIN ISLANDS, BRITISH', 'VG', 'VGB', 92, 1284, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(234, 'VIRGIN ISLANDS, U.S.', 'VI', 'VIR', 850, 1340, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(235, 'WALLIS AND FUTUNA', 'WF', 'WLF', 876, 681, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(236, 'WESTERN SAHARA', 'EH', 'ESH', 732, 212, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(237, 'YEMEN', 'YE', 'YEM', 887, 967, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(238, 'ZAMBIA', 'ZM', 'ZMB', 894, 260, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(239, 'ZIMBABWE', 'ZW', 'ZWE', 716, 263, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25');

-- --------------------------------------------------------

--
-- Table structure for table `country_product`
--

DROP TABLE IF EXISTS `country_product`;
CREATE TABLE IF NOT EXISTS `country_product` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `country_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `country_product_country_id_index` (`country_id`),
  KEY `country_product_product_id_index` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

DROP TABLE IF EXISTS `coupon`;
CREATE TABLE IF NOT EXISTS `coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(300) DEFAULT NULL,
  `catid` varchar(300) DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `code` varchar(500) DEFAULT NULL,
  `amount` varchar(250) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  `valid_from` datetime DEFAULT NULL,
  `expires_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupon`
--

INSERT INTO `coupon` (`id`, `userid`, `catid`, `name`, `code`, `amount`, `status`, `valid_from`, `expires_date`) VALUES
(1, '1', '13', 'test', 'test', '20', '1', '2019-10-14 00:00:00', '2019-11-16 00:00:00'),
(2, '1', '10', 'testr', 'testr', '10', '1', '2019-10-10 00:00:00', '2019-11-30 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `couriers`
--

DROP TABLE IF EXISTS `couriers`;
CREATE TABLE IF NOT EXISTS `couriers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_free` int(11) NOT NULL,
  `cost` decimal(8,2) DEFAULT '0.00',
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_pic` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customertype` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fb_id` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gmail_id` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(10) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `stripe_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login_token` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customers_email_unique` (`email`),
  UNIQUE KEY `token_unique` (`login_token`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `lname`, `profile_pic`, `email`, `password`, `phone`, `customertype`, `fb_id`, `gmail_id`, `country_id`, `status`, `stripe_id`, `card_brand`, `card_last_four`, `trial_ends_at`, `deleted_at`, `remember_token`, `login_token`, `created_at`, `updated_at`) VALUES
(1, 'testt', NULL, 'profile/vnvNWRMyeyj19FAVIMHcjqPHHGuRIfvQu1loT4rL.png', 'testerwhytecreations@gmail.com', '$2y$10$0t12TTY2KC5.XTeXfoJX7.Dzm7mkpaLIvX/Qfy070PYdf8sKkeWf6', NULL, 'Individual', NULL, NULL, 174, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'af92c9efbfc9fb11a89e81efd80d87d7ac5bd96334a91baccad78961eaf1223c', '2019-10-10 07:04:35', '2019-10-25 02:12:58'),
(2, 'crr', NULL, 'profile/1dsKTHqjztEUCFxTbSemS0dfJYaX7NheL7wH5vgg.png', 'cr@gmail.com', '$2y$10$vDYjDbAG9To/MuhcWGPOKeUZ9GyFsf.T4sa66IgPStjNyAKvtwama', '2589631470', NULL, NULL, NULL, 174, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'b17e4078bdd38909aa0af36af6725e77da3aa49c393fc22aa78cad928c54424a', '2019-10-21 09:11:47', '2019-10-21 09:16:09'),
(3, 'ramya', NULL, NULL, 'ramyaissac123@gmail.com', NULL, '9874563210', NULL, NULL, '108878835266056248843', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'eyJhbGciOiJSUzI1NiIsImtpZCI6ImE0MzEzZTdmZDFlOWUyYTRkZWQzYjI5MmQyYTdmNGU1MTk1NzQzMDgiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiIxMDk5MDAyMzE0OTI4LTRvaGtsY2puazczcGNnNnYzbmNsMjhwaGZtc3FqcDZzLmFwcHMuZ29vZ2xldXNlcmNvbnR', '2019-10-25 02:10:46', '2019-10-25 02:10:46'),
(4, 'demo test', NULL, 'profile/BwnAQLD71V8HoLTrFpEdQi8CJCaBa8VI9xoR3dYV.jpeg', 'test@gmail.com', '$2y$10$HDjbfNfhy//AJ1yrmVloz.B3ZQRdSvsiKbiT6YAu.vi4Xsx9RctvG', '123456789', NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'a7d93b38c53d80e4a7f7a161f03eedc62e08fa1ea767d67cea3dd1ae6a8895fd', '2019-11-08 03:10:33', '2019-11-08 03:38:51');

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
CREATE TABLE IF NOT EXISTS `devices` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `device_id` text COLLATE utf8_unicode_ci NOT NULL,
  `customers_id` int(100) NOT NULL DEFAULT '0',
  `device_type` text COLLATE utf8_unicode_ci NOT NULL,
  `register_date` int(100) NOT NULL DEFAULT '0',
  `update_date` int(100) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `isDesktop` tinyint(1) NOT NULL DEFAULT '0',
  `oneSignalId` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isEnableMobile` tinyint(1) NOT NULL DEFAULT '1',
  `isEnableDesktop` tinyint(1) NOT NULL DEFAULT '1',
  `ram` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `processor` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `device_os` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `device_model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manufacturer` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_notify` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`id`, `device_id`, `customers_id`, `device_type`, `register_date`, `update_date`, `status`, `isDesktop`, `oneSignalId`, `isEnableMobile`, `isEnableDesktop`, `ram`, `processor`, `device_os`, `location`, `device_model`, `manufacturer`, `is_notify`) VALUES
(2, 'd_nJaG61GD0:APA91bEjPnXBoL8D3NM24sbAP7QpGblMbRlsO2XNo5x3dI6I9X4aDTGIDbHwMwEOWwzeDVahd_a3mODYhyPycX5SgeCFqOM4NqYCaP9khA0q0nsk5CbpLJj5lbyOfM0acmNIRJlXZYaQ', 1, '3', 1571649611, 1571649611, 1, 0, NULL, 1, 1, NULL, NULL, 'ios', NULL, NULL, NULL, 1),
(3, 'cLdDWXDQdpI:APA91bF2KE-TO7hiVAJLXEc5PptfX03z7jaQ8ki3_8-WMUJAiOSJOV0lu2IR3iOe3hrWHDyOX6LjjBQdToOyqMx2mKRxjxI4H-2I9rX_il3mj7i4EferF1IZfl3T6DLZkG1ZOaGp_Rjq', 4, '3', 1573196135, 1573196135, 1, 0, NULL, 1, 1, NULL, NULL, 'iOS', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employees_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `email`, `password`, `status`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Marcel', 'john@doe.com', '$2y$10$5ZrA4DfB5j1pybCFryKQyugMfls.Ql6acYHNoopX4W60hJfegExJW', 1, NULL, 'hJ1WcgFyXm', '2018-10-17 04:31:24', '2018-10-17 04:31:24'),
(2, 'PartApp', 'admin@party.com', '$2y$10$FJY2HtqWLK3Mwnz1ZNC2DOND71XV.5mNBC1kXwi4WHG2ePwbQ4aGm', 1, NULL, 'RIp4ZcxS1M', '2018-10-17 04:31:24', '2019-10-09 03:56:30'),
(3, 'Rex', 'clerk@doe.com', '$2y$10$5ZrA4DfB5j1pybCFryKQyugMfls.Ql6acYHNoopX4W60hJfegExJW', 1, NULL, 'hr1OoDSHKB', '2018-10-17 04:31:24', '2018-10-17 04:31:24');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_customers_table', 1),
(2, '2014_10_12_000010_create_employees_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2016_05_26_020731_create_country_table', 1),
(5, '2016_05_26_035202_create_provinces_table', 1),
(6, '2016_05_26_051502_create_cities_table', 1),
(7, '2017_06_10_225235_create_products_table', 1),
(8, '2017_06_11_015526_create_categories_table', 1),
(9, '2017_06_11_033553_create_category_product_table', 1),
(10, '2017_06_11_073305_create_address_table', 1),
(11, '2017_06_12_225546_create_order_status_table', 1),
(12, '2017_06_13_044714_create_couriers_table', 1),
(13, '2017_06_13_053346_create_orders_table', 1),
(14, '2017_06_13_091740_create_order_products_table', 1),
(15, '2017_06_17_011245_create_shoppingcart_table', 1),
(16, '2018_01_18_163143_create_product_images_table', 1),
(17, '2018_02_19_151228_create_cost_column', 1),
(18, '2018_03_10_024148_laratrust_setup_tables', 1),
(19, '2018_03_10_110530_create_attributes_table', 1),
(20, '2018_03_10_150920_create_attribute_values_table', 1),
(21, '2018_03_11_014046_create_product_attributes_table', 1),
(22, '2018_03_11_090249_create_attribute_value_product_attribute_table', 1),
(23, '2018_03_15_232344_create_customer_subscription_table', 1),
(24, '2018_06_16_000410_add_fields_on_order_product_table', 1),
(25, '2018_06_16_102641_create_brands_table', 1),
(26, '2018_06_17_175657_add_brand_id_in_products_table', 1),
(27, '2018_06_18_135142_add_columns_in_product_attributes_table', 1),
(28, '2018_06_30_041523_add_product_attributes', 1),
(29, '2018_07_03_023925_create_states_table', 1),
(30, '2018_07_16_184224_add_phone_number_in_address_table', 1),
(31, '2018_07_16_190024_add_tracking_number_and_label_url_to_orders_table', 1),
(32, '2018_07_17_184437_add_sale_price_in_products_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
CREATE TABLE IF NOT EXISTS `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(5) NOT NULL,
  `order_id` int(10) NOT NULL,
  `title` varchar(250) NOT NULL,
  `message` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `reference` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `address_id` int(10) UNSIGNED DEFAULT NULL,
  `coupon_code` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_address_id` int(11) DEFAULT NULL,
  `order_status_id` int(10) UNSIGNED NOT NULL,
  `payment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discounts` decimal(8,2) NOT NULL DEFAULT '0.00',
  `total_products` decimal(8,2) DEFAULT NULL,
  `shipping_charge` decimal(10,2) DEFAULT NULL,
  `tax` decimal(8,2) NOT NULL DEFAULT '0.00',
  `total` decimal(8,2) DEFAULT NULL,
  `total_paid` decimal(8,2) NOT NULL DEFAULT '0.00',
  `sale_price` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tracking_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `servicedate` date DEFAULT NULL,
  `servicetime` time(6) DEFAULT NULL,
  `summary` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(900) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uploadfile` varchar(900) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_latitude` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_longitude` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `orders_reference_unique` (`reference`),
  KEY `orders_customer_id_index` (`customer_id`),
  KEY `orders_address_id_index` (`address_id`),
  KEY `orders_order_status_id_index` (`order_status_id`),
  KEY `shipping_address_id` (`shipping_address_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `reference`, `order_id`, `customer_id`, `address_id`, `coupon_code`, `amount`, `shipping_address_id`, `order_status_id`, `payment`, `discounts`, `total_products`, `shipping_charge`, `tax`, `total`, `total_paid`, `sale_price`, `invoice`, `label_url`, `tracking_number`, `created_at`, `updated_at`, `servicedate`, `servicetime`, `summary`, `description`, `uploadfile`, `location_latitude`, `location_longitude`, `address`, `country_id`) VALUES
(1, 'ORD0000002023', NULL, 1, 32, 'test', '20', NULL, 5, 'cash_on_delivery', '0.00', '15.00', NULL, '0.00', '15.00', '0.00', NULL, NULL, NULL, NULL, '2019-02-15 00:04:22', '2019-02-15 00:52:26', '2019-02-14', '838:59:59.000000', NULL, NULL, NULL, '10.850516', '76.271080', 'aaa bbb', 187),
(2, 'ORD0000002002', NULL, 1, NULL, NULL, NULL, NULL, 3, 'cash after service', '0.00', '3480.00', NULL, '0.00', '3480.00', '0.00', NULL, NULL, NULL, NULL, '2019-10-18 09:22:29', '2019-11-08 02:14:22', '2019-10-26', '02:16:00.000000', NULL, NULL, NULL, '10.850516', '76.271080', 'test address', 174);

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

DROP TABLE IF EXISTS `order_product`;
CREATE TABLE IF NOT EXISTS `order_product` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_type` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_description` text COLLATE utf8mb4_unicode_ci,
  `product_price` decimal(8,2) DEFAULT NULL,
  `productAttribute` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_product_order_id_index` (`order_id`),
  KEY `order_product_product_id_index` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `order_product`
--

INSERT INTO `order_product` (`id`, `order_id`, `product_id`, `quantity`, `product_name`, `product_sku`, `product_type`, `product_description`, `product_price`, `productAttribute`) VALUES
(1, 2, 18, 3, 'Menu4', NULL, NULL, '<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the</p>', '1160.00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_statuses`
--

DROP TABLE IF EXISTS `order_statuses`;
CREATE TABLE IF NOT EXISTS `order_statuses` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `order_statuses`
--

INSERT INTO `order_statuses` (`id`, `name`, `color`, `created_at`, `updated_at`) VALUES
(1, 'Received', 'green', '2018-10-17 04:31:27', '2018-10-17 04:31:27'),
(3, 'Cancelled', 'red', '2018-10-17 04:31:27', '2018-10-22 04:41:38'),
(5, 'Requested', 'violet', '2018-10-17 04:31:27', '2018-10-17 04:31:27'),
(6, 'Completed', 'Green', '2018-11-15 00:32:16', '2018-11-15 00:32:16');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `page_id` int(100) NOT NULL AUTO_INCREMENT,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`page_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `slug`, `status`) VALUES
(1, 'about-us', 1),
(2, 'terms', 1),
(3, 'privacypolicy', 1),
(4, 'contact', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages_description`
--

DROP TABLE IF EXISTS `pages_description`;
CREATE TABLE IF NOT EXISTS `pages_description` (
  `page_description_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_ar` varchar(600) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_ar` longtext COLLATE utf8_unicode_ci,
  `language_id` int(100) NOT NULL,
  `page_id` int(100) NOT NULL,
  PRIMARY KEY (`page_description_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages_description`
--

INSERT INTO `pages_description` (`page_description_id`, `name`, `name_ar`, `slug`, `description`, `description_ar`, `language_id`, `page_id`) VALUES
(1, 'About Us', 'About Us', 'about-us', '<h4>About part App</h4>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\\\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', NULL, 1, 1),
(2, 'Terms and Conditions', 'Terms and Conditions', 'terms', '<h4>Terms and Conditions</h4>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\\\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', NULL, 1, 2),
(3, 'Privacy Policy', 'Privacy Policy', 'privacypolicy', '<h4>Privacy Policy</h4>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\\\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', NULL, 1, 3),
(4, 'Contact Us', 'Contact Us', 'contact', '<h2>We’d Love to Hear <br>From You.</h2>\r\n\r\n\r\n<h4>Party App events planning &amp; Trading</h4>\r\n<p>P. O. Box: 2757,<br>\r\n26, Al Nathra Street, Dafna<br>\r\nDoha - Qatar<br>\r\nEmail : info@partyapp.qa<br>\r\nContact:  +974 3333 7386</p>', '<h2>We’d Love to Hear <br>From You.</h2>\r\n\r\n\r\n<h4>Party App events planning &amp; Trading</h4>\r\n<p>P. O. Box: 2757,<br>\r\n26, Al Nathra Street, Dafna<br>\r\nDoha - Qatar<br>\r\nEmail : info@partyapp.qa<br>\r\nContact:  +974 3333 7386</p>', 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('test@gmail.com', '$2y$10$nKgadMnI0X58QJFoECNuhOfOM8ztA1tcMQeGnUB90KcJtB5e7kzfi', '2019-01-04 03:46:45'),
('testerwhytecreations@gmail.com', '$2y$10$HhEGsHFtz50Il61qzYL3Qe01g5uQrIYj5Mq8YFgf4LcmFmeN/XOgW', '2019-11-08 04:20:09');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'create-product', 'Create product', '', '2018-10-17 04:31:23', '2018-10-17 04:31:23'),
(2, 'view-product', 'View product', '', '2018-10-17 04:31:23', '2018-10-17 04:31:23'),
(3, 'update-product', 'Update product', '', '2018-10-17 04:31:23', '2018-10-17 04:31:23'),
(4, 'delete-product', 'Delete product', '', '2018-10-17 04:31:23', '2018-10-17 04:31:23'),
(5, 'update-order', 'Update order', '', '2018-10-17 04:31:23', '2018-10-17 04:31:23');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(1, 2),
(2, 2),
(3, 2),
(4, 2),
(5, 2),
(1, 3),
(2, 3),
(3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

DROP TABLE IF EXISTS `permission_user`;
CREATE TABLE IF NOT EXISTS `permission_user` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`permission_id`,`user_type`),
  KEY `permission_user_permission_id_foreign` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `brand_id` int(10) UNSIGNED DEFAULT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ar` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `description_ar` longtext COLLATE utf8mb4_unicode_ci,
  `short_description` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_description_ar` mediumtext COLLATE utf8mb4_unicode_ci,
  `duration` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT '0',
  `payment` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(8,2) NOT NULL,
  `sale_price` decimal(8,2) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `length` decimal(8,2) DEFAULT NULL,
  `width` decimal(8,2) DEFAULT NULL,
  `height` decimal(8,2) DEFAULT NULL,
  `distance_unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` decimal(8,2) DEFAULT '0.00',
  `mass_unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `new_product_flag` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `products_brand_id_foreign` (`brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `brand_id`, `sku`, `name`, `name_ar`, `slug`, `type`, `description`, `description_ar`, `short_description`, `short_description_ar`, `duration`, `cover`, `quantity`, `payment`, `price`, `sale_price`, `status`, `length`, `width`, `height`, `distance_unit`, `weight`, `mass_unit`, `created_at`, `updated_at`, `new_product_flag`) VALUES
(1, NULL, NULL, 'Brown wooden tables  chairs', 'Brown wooden tables  chairs', 'brown-wooden-tables-chairs', NULL, NULL, NULL, NULL, NULL, NULL, '1570613747.Mask Group 24.png', 0, NULL, '2425.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-10-09 07:05:47', '2019-10-16 04:59:44', '0'),
(2, NULL, NULL, 'Brown wooden tables  chairs1', 'Brown wooden tables  chairs1', 'brown-wooden-tables-chairs1', NULL, NULL, NULL, NULL, NULL, NULL, '1570618894.Mask Group 24.png', 0, NULL, '2343.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-10-09 08:31:34', '2019-10-09 08:31:34', '0'),
(3, NULL, NULL, 'Brown wooden tables  chairs2', 'Brown wooden tables  chairs2', 'brown-wooden-tables-chairs2', NULL, NULL, NULL, NULL, NULL, NULL, '1570618951.Mask Group 24.png', 0, NULL, '2123.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-10-09 08:32:31', '2019-10-09 08:32:31', '0'),
(4, NULL, NULL, 'Brown wooden tables  chairs3', 'Brown wooden tables  chairs3', 'brown-wooden-tables-chairs3', NULL, NULL, NULL, NULL, NULL, NULL, '1570619044.Mask Group 24.png', 0, NULL, '2321.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-10-09 08:34:04', '2019-10-09 08:34:11', '0'),
(5, NULL, NULL, 'Brown wooden tables  chairs4', 'Brown wooden tables  chairs4', 'brown-wooden-tables-chairs4', NULL, NULL, NULL, NULL, NULL, NULL, '1570619086.Mask Group 24.png', 0, NULL, '4321.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-10-09 08:34:46', '2019-10-09 08:35:40', '0'),
(6, NULL, NULL, 'Brown wooden tables  chairs5', 'Brown wooden tables  chairs5', 'brown-wooden-tables-chairs5', NULL, NULL, NULL, NULL, NULL, NULL, '1570619105.Mask Group 24.png', 0, NULL, '2121.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-10-09 08:35:05', '2019-10-09 08:35:11', '0'),
(7, NULL, NULL, 'Brown wooden tables  chairs6', 'Brown wooden tables  chairs6', 'brown-wooden-tables-chairs6', NULL, NULL, NULL, NULL, NULL, NULL, '1570619174.Mask Group 24.png', 0, NULL, '2221.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-10-09 08:36:14', '2019-10-09 08:36:14', '0'),
(8, NULL, NULL, 'Brown wooden tables  chairs7', 'Brown wooden tables  chairs7', 'brown-wooden-tables-chairs7', NULL, NULL, NULL, NULL, NULL, NULL, '1570619194.Mask Group 24.png', 0, NULL, '2122.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-10-09 08:36:34', '2019-10-21 03:02:54', '0'),
(9, NULL, NULL, 'Brown wooden tables  chairs8', 'Brown wooden tables  chairs8', 'brown-wooden-tables-chairs8', NULL, NULL, NULL, NULL, NULL, NULL, '1570619226.Mask Group 24.png', 0, NULL, '2122.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-10-09 08:37:06', '2019-10-09 08:37:06', '0'),
(10, NULL, NULL, 'Brown wooden tables  chairs9', 'Brown wooden tables  chairs9', 'brown-wooden-tables-chairs9', NULL, NULL, NULL, NULL, NULL, NULL, '1570619341.Mask Group 24.png', 0, NULL, '2322.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-10-09 08:39:01', '2019-10-09 08:39:01', '0'),
(11, NULL, NULL, 'option1', 'option1', 'option1', NULL, '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', NULL, NULL, NULL, NULL, NULL, 1, NULL, '233.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-10-09 08:45:22', '2019-10-10 09:50:23', '0'),
(12, NULL, NULL, 'option2', 'option2', 'option2', NULL, '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', NULL, NULL, NULL, NULL, '1570620816.Mask Group 19.png', 0, NULL, '2222.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-10-09 09:03:36', '2019-10-10 09:50:41', '0'),
(13, NULL, NULL, 'option3', 'option3', 'option3', NULL, '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', NULL, NULL, NULL, NULL, '1570620918.Mask Group 18.png', 0, NULL, '3232.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-10-09 09:05:18', '2019-10-09 09:05:18', '0'),
(14, NULL, NULL, 'option4', 'option4', 'option4', NULL, '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', NULL, NULL, NULL, NULL, '1570620959.Mask Group 19.png', 0, NULL, '3123.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-10-09 09:05:59', '2019-10-09 09:05:59', '0'),
(15, NULL, NULL, 'Menu1', 'Menu1', 'menu1', NULL, '<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the</p>', NULL, NULL, NULL, NULL, '1570684931.Mask Group 14.png', 0, NULL, '1600.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-10-10 02:52:11', '2019-10-16 04:07:18', '0'),
(16, NULL, NULL, 'Menu2', 'Menu2', 'menu2', NULL, '<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the</p>', NULL, NULL, NULL, NULL, '1570685006.Mask Group 15.png', 0, NULL, '1500.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-10-10 02:53:26', '2019-10-16 04:07:56', '0'),
(17, NULL, NULL, 'Menu3', 'Menu3', 'menu3', NULL, '<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the</p>', NULL, NULL, NULL, NULL, '1570685115.Mask Group 16.png', 0, NULL, '1200.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-10-10 02:55:15', '2019-10-16 04:08:22', '0'),
(18, NULL, NULL, 'Menu4', 'Menu4', 'menu4', NULL, '<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the</p>', NULL, NULL, NULL, NULL, '1572928744.alArtboard 1.png', 47, NULL, '1160.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-10-10 02:56:05', '2019-11-05 02:09:05', '0');

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes`
--

DROP TABLE IF EXISTS `product_attributes`;
CREATE TABLE IF NOT EXISTS `product_attributes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL,
  `price` decimal(8,2) DEFAULT NULL,
  `sale_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default` tinyint(4) NOT NULL DEFAULT '0',
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_attributes_product_id_foreign` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `product_attributes`
--

INSERT INTO `product_attributes` (`id`, `quantity`, `price`, `sale_price`, `cover`, `default`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 23, '21.00', NULL, NULL, 1, 18, '2019-10-10 03:15:04', '2019-10-10 03:15:04'),
(4, 24, '22.00', NULL, NULL, 0, 18, '2019-10-10 07:17:23', '2019-10-10 07:17:23'),
(5, 1, '2425.00', NULL, NULL, 1, 1, '2019-10-16 05:00:52', '2019-10-16 05:00:52');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

DROP TABLE IF EXISTS `product_images`;
CREATE TABLE IF NOT EXISTS `product_images` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` int(10) UNSIGNED NOT NULL,
  `src` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_images_product_id_index` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `src`) VALUES
(1, 11, '1570619722.Mask Group 30.png'),
(2, 11, '1570619722.Mask Group 31.png'),
(3, 11, '1570619777.Mask Group 20.png'),
(4, 11, '1570619777.Mask Group 21.png'),
(5, 12, '1570620816.Mask Group 18.png'),
(6, 12, '1570620816.Mask Group 20.png'),
(7, 12, '1570620816.Mask Group 21.png'),
(8, 12, '1570620816.Mask Group 30.png'),
(9, 12, '1570620816.Mask Group 31.png');

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

DROP TABLE IF EXISTS `provinces`;
CREATE TABLE IF NOT EXISTS `provinces` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `provinces_country_id_index` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `related_products`
--

DROP TABLE IF EXISTS `related_products`;
CREATE TABLE IF NOT EXISTS `related_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `related_product_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `request_attachment`
--

DROP TABLE IF EXISTS `request_attachment`;
CREATE TABLE IF NOT EXISTS `request_attachment` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `order_id` int(15) DEFAULT NULL,
  `images` varchar(900) DEFAULT NULL,
  `type` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
CREATE TABLE IF NOT EXISTS `reviews` (
  `reviews_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `customers_id` int(11) DEFAULT NULL,
  `customers_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reviews_rating` int(1) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `reviews_status` tinyint(1) NOT NULL DEFAULT '0',
  `reviews_read` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`reviews_id`),
  KEY `idx_reviews_products_id` (`products_id`),
  KEY `idx_reviews_customers_id` (`customers_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reviews_description`
--

DROP TABLE IF EXISTS `reviews_description`;
CREATE TABLE IF NOT EXISTS `reviews_description` (
  `reviews_id` int(11) NOT NULL,
  `languages_id` int(11) NOT NULL,
  `reviews_text` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`reviews_id`,`languages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'Super Admin', '', '2018-10-17 04:31:24', '2018-10-17 04:31:24'),
(2, 'admin', 'Admin', '', '2018-10-17 04:31:24', '2018-10-17 04:31:24'),
(3, 'clerk', 'Clerk', '', '2018-10-17 04:31:24', '2018-10-17 04:31:24');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
CREATE TABLE IF NOT EXISTS `role_user` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  KEY `role_user_role_id_foreign` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`, `user_type`) VALUES
(1, 1, 'App\\Shop\\Employees\\Employee'),
(2, 2, 'App\\Shop\\Employees\\Employee'),
(3, 3, 'App\\Shop\\Employees\\Employee');

-- --------------------------------------------------------

--
-- Table structure for table `service_countries`
--

DROP TABLE IF EXISTS `service_countries`;
CREATE TABLE IF NOT EXISTS `service_countries` (
  `id` int(56) NOT NULL AUTO_INCREMENT,
  `product_id` int(56) NOT NULL,
  `country_id` int(57) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcart`
--

DROP TABLE IF EXISTS `shoppingcart`;
CREATE TABLE IF NOT EXISTS `shoppingcart` (
  `identifier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`identifier`,`instance`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
CREATE TABLE IF NOT EXISTS `states` (
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  KEY `states_country_id_foreign` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`state`, `state_code`, `country_id`) VALUES
('Alaska', 'AK', 226),
('Alabama', 'AL', 226),
('Arkansas', 'AR', 226),
('Arizona', 'AZ', 226),
('California', 'CA', 226),
('Colorado', 'CO', 226),
('Connecticut', 'CT', 226),
('District of Columbia', 'DC', 226),
('Delaware', 'DE', 226),
('Florida', 'FL', 226),
('Georgia', 'GA', 226),
('Hawaii', 'HI', 226),
('Iowa', 'IA', 226),
('Idaho', 'ID', 226),
('Illinois', 'IL', 226),
('Indiana', 'IN', 226),
('Kansas', 'KS', 226),
('Kentucky', 'KY', 226),
('Louisiana', 'LA', 226),
('Massachusetts', 'MA', 226),
('Maryland', 'MD', 226),
('Maine', 'ME', 226),
('Michigan', 'MI', 226),
('Minnesota', 'MN', 226),
('Missouri', 'MO', 226),
('Mississippi', 'MS', 226),
('Montana', 'MT', 226),
('North Carolina', 'NC', 226),
('North Dakota', 'ND', 226),
('Nebraska', 'NE', 226),
('New Hampshire', 'NH', 226),
('New Jersey', 'NJ', 226),
('New Mexico', 'NM', 226),
('Nevada', 'NV', 226),
('New York', 'NY', 226),
('Ohio', 'OH', 226),
('Oklahoma', 'OK', 226),
('Oregon', 'OR', 226),
('Pennsylvania', 'PA', 226),
('Rhode Island', 'RI', 226),
('South Carolina', 'SC', 226),
('South Dakota', 'SD', 226),
('Tennessee', 'TN', 226),
('Texas', 'TX', 226),
('Utah', 'UT', 226),
('Virginia', 'VA', 226),
('Vermont', 'VT', 226),
('Washington', 'WA', 226),
('Wisconsin', 'WI', 226),
('West Virginia', 'WV', 226),
('Wyoming', 'WY', 226);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

DROP TABLE IF EXISTS `subscribers`;
CREATE TABLE IF NOT EXISTS `subscribers` (
  `subscriber_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_address` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`subscriber_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
CREATE TABLE IF NOT EXISTS `subscriptions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_plan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `tax_class`
--

DROP TABLE IF EXISTS `tax_class`;
CREATE TABLE IF NOT EXISTS `tax_class` (
  `tax_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_class_title` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `tax_class_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`tax_class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tax_class`
--

INSERT INTO `tax_class` (`tax_class_id`, `tax_class_title`, `tax_class_description`, `last_modified`, `date_added`) VALUES
(1, 'Handling Charge', 'This charge is applied on products related to Qatar areas.', '2018-10-18 07:06:34', '2017-08-07 07:06:53');

-- --------------------------------------------------------

--
-- Table structure for table `tax_rates`
--

DROP TABLE IF EXISTS `tax_rates`;
CREATE TABLE IF NOT EXISTS `tax_rates` (
  `tax_rates_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_zone_id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `tax_priority` int(5) DEFAULT '1',
  `tax_rate` decimal(7,2) NOT NULL,
  `tax_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`tax_rates_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tax_rates`
--

INSERT INTO `tax_rates` (`tax_rates_id`, `tax_zone_id`, `tax_class_id`, `tax_priority`, `tax_rate`, `tax_description`, `last_modified`, `date_added`) VALUES
(1, 1, 1, 1, '0.00', '', '2018-10-03 07:22:00', '2017-08-07 07:07:45'),
(2, 2, 1, 1, '0.00', '', NULL, '2018-10-17 17:38:55');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

DROP TABLE IF EXISTS `wishlist`;
CREATE TABLE IF NOT EXISTS `wishlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq` (`product_id`,`customer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zones`
--

DROP TABLE IF EXISTS `zones`;
CREATE TABLE IF NOT EXISTS `zones` (
  `zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_country_id` int(11) UNSIGNED NOT NULL,
  `zone_code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `zone_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`zone_id`),
  KEY `idx_zones_country_id` (`zone_country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `zones`
--

INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES
(1, 174, 'QA-DA', 'Al Daayen'),
(2, 174, 'QA-DH', 'Doha'),
(3, 174, 'QA-KH', 'Al Khor'),
(4, 174, 'QA-RA', 'Al Rayyan'),
(5, 174, 'QA-SH', 'Shahaniya'),
(6, 174, 'QA-SM', 'Shamal'),
(7, 174, 'QA-US', 'Um Salal'),
(8, 174, 'QA-WA', 'Al Wakrah');

-- --------------------------------------------------------

--
-- Table structure for table `zones_to_geo_zones`
--

DROP TABLE IF EXISTS `zones_to_geo_zones`;
CREATE TABLE IF NOT EXISTS `zones_to_geo_zones` (
  `association_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_country_id` int(11) NOT NULL,
  `zone_id` int(11) DEFAULT NULL,
  `geo_zone_id` int(11) DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`association_id`),
  KEY `idx_zones_to_geo_zones_country_id` (`zone_country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zone_courier_mapping`
--

DROP TABLE IF EXISTS `zone_courier_mapping`;
CREATE TABLE IF NOT EXISTS `zone_courier_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_id` int(11) UNSIGNED NOT NULL,
  `courier_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zone_id` (`zone_id`) USING BTREE,
  KEY `courier_id` (`courier_id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zone_courier_mapping`
--

INSERT INTO `zone_courier_mapping` (`id`, `zone_id`, `courier_id`) VALUES
(9, 3, 1),
(8, 2, 1),
(7, 1, 1),
(10, 4, 1),
(11, 5, 1),
(12, 6, 1),
(13, 7, 1),
(14, 8, 1),
(26, 5, 2),
(25, 3, 2),
(24, 1, 2);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  ADD CONSTRAINT `addresses_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`);

--
-- Constraints for table `attribute_values`
--
ALTER TABLE `attribute_values`
  ADD CONSTRAINT `attribute_values_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attribute_value_product_attribute`
--
ALTER TABLE `attribute_value_product_attribute`
  ADD CONSTRAINT `attribute_value_product_attribute_attribute_value_id_foreign` FOREIGN KEY (`attribute_value_id`) REFERENCES `attribute_values` (`id`),
  ADD CONSTRAINT `attribute_value_product_attribute_product_attribute_id_foreign` FOREIGN KEY (`product_attribute_id`) REFERENCES `product_attributes` (`id`);

--
-- Constraints for table `category_product`
--
ALTER TABLE `category_product`
  ADD CONSTRAINT `category_product_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `category_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_province_id_foreign` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_address_id_foreign` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`),
  ADD CONSTRAINT `orders_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `orders_order_status_id_foreign` FOREIGN KEY (`order_status_id`) REFERENCES `order_statuses` (`id`);

--
-- Constraints for table `order_product`
--
ALTER TABLE `order_product`
  ADD CONSTRAINT `order_product_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `order_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`);

--
-- Constraints for table `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD CONSTRAINT `product_attributes_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `provinces`
--
ALTER TABLE `provinces`
  ADD CONSTRAINT `provinces_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`);

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `states`
--
ALTER TABLE `states`
  ADD CONSTRAINT `states_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`);

--
-- Constraints for table `zones`
--
ALTER TABLE `zones`
  ADD CONSTRAINT `fk_country` FOREIGN KEY (`zone_country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
